import { cardSubtitle } from "assets/jss/material-kit-react";
import React, { useState,useEffect } from "react";
import styled from "styled-components";
import axios from 'axios'
import JobDetailCardWorker from "./JobDetailCardWorker";
import WorkerFooter from "../components/WorkerNavBarFooter/WorkerFooter"
import AdminNavBar from '../components/Navbars/AdminNavbar'
import Cookies from "js-cookie";
// import Cookies from "js-cookie";
import { withRouter } from "react-router-dom"
import {
  Row,
  Col
} from "reactstrap";
 const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
//const URL ="http://localhost:8080"
const InfoRow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
`;

const InfoContent = styled.div``;

const InfoDetail = styled.div`
  margin: 2% 0;
`;

const ContentWrapper = styled.div`
  display: flex;
  margin: 5% 0;
`;

const ContentText = styled.p`
  margin-left: 8px;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
`;

const handleColorType = (bgcolor) => {
  switch (bgcolor) {
    case "เสร็จแล้ว":
      return "#09c676";
    case "ยังไม่เสร็จ":
      return "#e34849";
    default:
      return "#000";
  }
};

const WorkTodo = styled.div`
  width: 45%;
  height: auto;
  border-radius: 4px;
  background-color: ${({ bgcolor }) => handleColorType(bgcolor)};
  color: white;
  text-align: center;
`;

const WorkTodoText = styled.p`
  color: white;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
  font-weight: bold;
  margin-top: 4px;
`;

const workAPI = {
  id: 1,
  status: "ยังไม่ได้ทำ",
  name: "ตรวจดูสถานที่อบไข่",
  ponds: [2, 5, 7],
  startTime: "10.00",
  endTime: "13.00",
  insectarium: {
      number: 1
  },
  workers: [
    {
      id: 1,
      firstName: "ลิซ่า"
    },
    {
      id: 2,
      firstName: "เจนนี่"
    }
]
};

const insectariumAPI = [
  {
    id: 1,
    puddle: "บ่อที่1",
  },
  { id: 2, puddle: "บ่อที่2" },
  { id: 3, puddle: "บ่อที่3" },
];

const percentage1 = 25;
const percentage2 = 12;
const percentage3 = 35;

const Jobdetail = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [active, setActive] = useState(null);
  const [id,setid]=useState("")
  const [jobapi, setjob]=useState([]);
  const [actname, setname]=useState("");
  const [poundseq, setpoundlist] = useState([]);
  const [boxseq, setboxlist] = useState([]);
  const [Workerseq, setWorkerlist] = useState([]);
  const [Workername, setWorkername] = useState([]);
  const [detail, setdetail] = useState([]);
  const [success, setfin] = useState("none")
  const [both, setboth]= useState("none")
  const [nameandval ,settasknameandval] = useState([])
  const [wid, setWid] = useState(null)
  const [date,setdate] =useState("")

  const toggle = (id) => {
    setIsOpen(!isOpen);
    setActive(id)
  }

  const dashboard = () => { 
    // - you can now access this.props.history for navigation
    props.history.push({pathname: "/admin/jobtodo", search: '?worker_id=' + wid});
  };


  

  const onejob = async (params) => {
    
    const onejob = await axios.get(`${URL}/oneact/${params}`)
    // console.log('onejob', onejob.data);
        setjob(onejob.data[0]);
        setpoundlist(onejob.data[0].assigned_pound);
        setboxlist(onejob.data[0].box_id)
        setWorkerlist(onejob.data[0].assigned_worker);

        var datearray = onejob.data[0].assigned_date.split("-");
        var newdate = datearray[1] + '-' + datearray[0] + '-' + datearray[2];
        setdate(newdate);

        if(onejob.data[0].assigned_insectarium === "0"){
          setboth(`ห้องอบไข่ ${jobapi.egg_id} กล่อง ${renderPonds(boxseq)}`)
        }
        else if(onejob.data[0].egg_id === "0"){
          setboth(`โรงเลี้ยง ${jobapi.assigned_insectarium} บ่อ ${renderPonds(poundseq)}`)
        }
        else if(onejob.data[0].activity_id === "30"){
          setboth(`โรงเลี้ยง ${jobapi.assigned_insectarium} บ่อ ${renderPonds(jobapi.assigned_pound)} ไป ห้องอบไข่ ${jobapi.egg_id} กล่อง ${renderPonds(jobapi.box_id)} `)
        }
        else{
          setboth(`ห้องอบไข่ ${jobapi.egg_id} กล่อง ${renderPonds(boxseq)} ไป โรงเลี้ยง ${jobapi.assigned_insectarium} บ่อ ${renderPonds(poundseq)}`)
        }
  
        const activityname = await axios.get(`${URL}/allact23/${onejob.data[0].activity_id}`)
        // console.log('activityname', activityname.data);
            setname(activityname.data[0]);
  
        const workername = await axios.get(`${URL}/workername/{${onejob.data[0].assigned_worker}}`)
        // console.log('activityname', workername.data);
          setWorkername(workername.data);
        
        const managertasknameandval = await axios.get(`${URL}/managertasknameandval/${onejob.data[0].assigned_id}`)
          settasknameandval(managertasknameandval.data)
          
        
    }
  
    const ponddetail = async (params) => {
      try {
    const ponddetail = await axios.get(`${URL}/subwstatus/${params}`)
        // console.log(ponddetail.data)
        setdetail(ponddetail.data);

      } catch (err) {
        // console.error('ERROR ',err.message);
      }
      
    };

    const Alltasksuccess = async (params) => {
      try {
        if(jobapi.assigned_status != "เสร็จแล้ว"){
    const Alltasksuccess = await axios.get(`${URL}/subwstatusnotfin/${params}`)
        //  console.log(Alltasksuccess.data)
        if(Alltasksuccess.data.length == 0){
          setfin("block");
        }
        else{
          setfin("none");
        }
        }
        else{
          setfin("none");
        }
      } catch (err) {
        // console.error('ERROR ',err.message);
      }
      
    };
  
    
  const renderPonds = (poundseq) => {
    if (!poundseq) { return }
    return poundseq.map((pound) => (
        pound + " "
    ))
}
const renderWorker = (Workername) => {
  if (!Workername) { return }
  return Workername.map((name) => (
      name.worker_name + " "
  ))
}


  const renderNameandVal = (nameandval) => {
    if (!nameandval) { return }
    return nameandval.map((task,index) => (
      <InfoDetail>
    
      <b style={{fontFamily: 'IBM Plex Sans Thai' ,fontSize: 17}}>{task.managertask_name}</b>
      <br></br>
      <b style={{fontFamily: 'IBM Plex Sans Thai'}}>- {task.mantaskval_val}</b>
      </InfoDetail>
    ))
  }
  const renderJobCards = (cards) => {
      return cards.map((item, index) => (
        <JobDetailCardWorker
          disable={true}
          isOpen={isOpen}
          toggle={toggle}
          active={active}
          item={item}
          index={index}
          id={id}
          detail={jobapi}
        />
        )
    )
  }

  const handleSubmit = () =>{
    
      axios({
        method: 'post',
        url: `${URL}/updatewassignedstatus/${id}`
    })
  }
 
  useEffect(() => {
    var query = new URLSearchParams(props.location.search);
    var params = query.get('card_id');
    const wid = query.get('worker_id')
    setWid(wid)
    setid(params)
    onejob(params)
    ponddetail(params)
    Alltasksuccess(params)
    
    // console.log(success)
    
  },[success,jobapi,nameandval])

  




  if (!jobapi) { return } 
  return (
  
    <div className="content" style={{ marginTop: "15vh" }}>
      <button
          className="btn btn-primary d-flex mb-5"
          onClick={dashboard}
          >
            <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
            <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>กลับหน้างานที่ต้องทำ</p>
        </button>
      <InfoRow>
        <InfoContent>
          <WorkTodo bgcolor={jobapi.assigned_status}>
            <WorkTodoText>{jobapi.assigned_status}</WorkTodoText>
          </WorkTodo>
          <h2 style={{fontFamily: 'IBM Plex Sans Thai'}}>{actname.activity_name}</h2>
          <ContentWrapper>
            <i
              style={{
                fontWeight: "bold",
                fontSize: "18px",
                color: "#0dc5ca",
                
              }}
              className={`nc-icon nc-pin-3`}
            />
            <ContentText>{both}</ContentText>
           
          </ContentWrapper>
          <ContentWrapper>
            <i
              style={{
                fontWeight: "bold",
                fontSize: "18px",
                color: "#0dc5ca",
              }}
              className={`nc-icon nc-single-02`}
            />
            <ContentText>{renderWorker(Workername)}</ContentText>
          </ContentWrapper>
          <ContentWrapper>
            <i
              style={{
                fontWeight: "bold",
                fontSize: "18px",
                color: "#0dc5ca",
              }}
              className={`nc-icon nc-calendar-60`}
            />
            <ContentText>{date}</ContentText>
            </ContentWrapper>
          <ContentWrapper>
            <i
              style={{
                fontWeight: "bold",
                fontSize: "18px",
                color: "#0dc5ca",
              }}
              className={`nc-icon nc-time-alarm`}
            />
            <ContentText>{jobapi.assigned_timestart}-{jobapi.assigned_timefin} น.</ContentText>
          </ContentWrapper>
        </InfoContent>
      </InfoRow>
      <InfoRow>
        <InfoDetail>
          <h3 style={{fontFamily: 'IBM Plex Sans Thai'}}>รายละเอียดงาน</h3>
          {renderNameandVal(nameandval)}
          <br></br>
          <b style={{fontFamily: 'IBM Plex Sans Thai'}}>{jobapi.assigned_note}</b>
          
        </InfoDetail>
      </InfoRow>
      <h2 style={{fontFamily: 'IBM Plex Sans Thai'}}>งานที่ต้องทำ</h2>
      {renderJobCards(detail)}
      <Row className='mb-3' style={{marginTop: '10px'}}>
                        <Col md="12" xs="12">
                          <div 
                          className='border' 
                          style={{
                            backgroundColor: '#09c676', 
                            borderRadius: '8px', 
                            height: '40px',
                            display: success }}
                            onClick ={()=>handleSubmit()}
                          >
                               <a href={`/admin/jobtodo?worker_id=${Cookies.get('workerId')}`}>
                                  <p className="text-center" style={{color:"white", fontSize:"18px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', lineHeight: '40px'}}>ยืนยันว่างานทั้งหมดเสร็จแล้ว</p>
                              </a>
                              
                          </div>
                        </Col>
                    </Row>
      <AdminNavBar pageName="งาน"/>
      <WorkerFooter active={true}/>
    </div>
  );
};

export default withRouter(Jobdetail);
