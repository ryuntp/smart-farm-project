import React from "react";
import axios from 'axios';
import BigCalendar from "react-big-calendar";
import Calendars from 'react-calendar';
import AdminFooter from "../components/AdminNavbarFooter/AdminFooter"
import '../assets/css/calendar.css';
import moment from "moment";
import SweetAlert from "react-bootstrap-sweetalert";
import Select from "react-select";
import { Card, CardBody, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Button, FormGroup, Input, Label} from "reactstrap";
import Cookies from 'js-cookie'

import { events } from "variables/general.jsx";
import TaskListCard from "./taskListCard"
import AdminNavBar from '../components/Navbars/AdminNavbar'
import { withRouter } from "react-router-dom";

const localizer = BigCalendar.momentLocalizer(moment);
const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
class Calendar extends React.Component {
  constructor(props) {
    const currentDate = new Date()
    super(props);
    this.state = {
      events: events,
      alert: null,
      date: currentDate,
      taskLists: [],
      filteredTaskLists: [],
      pound:[],
      modal: false,
      isOpen: false,
      jobNameOptions: [{
        value: "",
        label: "กรุณาเลือกงาน",
        }],
      workerOptions: [{
        value: "",
        label: "กรุณาเลือกพนักงาน",
        }],
      worker: null,
      selectedJobName: null

    };
    this.sendtime = this.sendtime.bind(this);
  }
  componentDidMount(){
    const farmId = Cookies.get('farmId')
    const currentDate = new Date()
    const data = ('0'+(currentDate.getMonth()+1)).slice(-2)+"-"+('0'+currentDate.getDate()).slice(-2)+"-"+currentDate.getFullYear()
    axios.get(`${URL}/allactv2/${data}/${farmId}`,{params: data})
      .then(res => {
        this.setState({
          taskLists:res.data
        })
      })
    
   

    axios.get(`${URL}/allactivityv2/${farmId}`)
    .then(res => {
        this.setState({jobNames: res.data})
        const jobNames = res.data
        if (jobNames) {
          jobNames.map((jn) => (
              this.setState({ jobNameOptions: [...this.state.jobNameOptions, {value: jn.activity_id, label: jn.activity_name }] })
          ))
        }
    })

    axios.get(`${URL}/allworkername/${farmId}`)
      .then(res => {
          const workers = res.data
          if (workers) {
            workers.map((worker) => (
                this.setState({ workerOptions: [...this.state.workerOptions, {value: worker.worker_id, label: worker.worker_name }] })
            ))
          }
    })
    // if (this.state.taskLists[0]){
    //    axios.get(`${URL}/poundlistass/${this.state.taskLists[0].assigned_id}`,{params: data})
    //   .then(res => {
    //      console.log(res.data)
    //      this.setState({
    //       pound :res.data
    //     })
    //     console.log(this.state.pound)
    //   })
    // }
  }

  sendtime = async (e) =>{
    const {date, onChange} = this.state
    const farmId = Cookies.get('farmId')
    await this.setState({date: e})

    const data = ('0'+(date.getMonth()+1)).slice(-2)+"-"+('0'+date.getDate()).slice(-2)+"-"+date.getFullYear()
    await axios.get(`${URL}/allactv2/${data}/${farmId}`,{params: data})
    .then(res => {
       this.setState({taskLists: []})
       this.setState({
        taskLists:res.data
      })

    })
    
  }

  selectedEvent = event => {
    alert(event.title);
  };
  addNewEventAlert = slotInfo => {
    this.setState({
      alert: (
        <SweetAlert
          input
          showCancel
          style={{ display: "block", marginTop: "-100px" }}
          title="Input something"
          onConfirm={e => this.addNewEvent(e, slotInfo)}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
          cancelBtnBsStyle="danger"
        />
      )
    });
  };
  addNewEvent = (e, slotInfo) => {
    var newEvents = this.state.events;
    newEvents.push({
      title: e,
      start: slotInfo.start,
      end: slotInfo.end
    });
    this.setState({
      alert: null,
      events: newEvents
    });
  };
  hideAlert = () => {
    this.setState({
      alert: null
    });
  };
  eventColors = (event, start, end, isSelected) => {
    var backgroundColor = "event-";
    event.color
      ? (backgroundColor = backgroundColor + event.color)
      : (backgroundColor = backgroundColor + "default");
    return {
      className: backgroundColor
    };
  };

  renderPonds = (ponds) => {
    if (!ponds) { return }
    return ponds.map((pond, index) => (
        pond + " "
    ))
}
  renderWorkLists = (taskLists) => {
    return taskLists.map((task, index) => (
      <div>
        <TaskListCard 
        taskList={task}
        index={index}

        />
      </div>
    ))
  }

  handleWorkerOnChage = (value) => {
    // const workerLength = value.length - 1
    this.setState({ worker: value })
  }

  handleOnSelectJobName = (value) => {
    this.setState({selectedJobName: value})
  }

  onSubmit = () => {
    const { selectedJobName, worker, modal, date } = this.state
    const formattedDate = ('0'+(date.getMonth()+1)).slice(-2)+"-"+('0'+date.getDate()).slice(-2)+"-"+date.getFullYear()
    this.setState({modal: !modal})
    console.log('selectedJobName', selectedJobName);
    console.log('worker', worker);
    axios.get(`${URL}/filtercanlendar/`, {
      params: {
        aid: selectedJobName ? selectedJobName.value : null, 
        wid: worker ? worker.value : null, 
        date: formattedDate ,
        fid : Cookies.get('farmId')
      }
    }).then(res => {
          console.log('resres', res);
          this.setState({filteredTaskLists: res.data})
          if (res.data.length < 1) {
            this.setState({taskLists: []})
          }
    })
    
  }

  handleClearFilter = () => {
    const { modal } = this.state
    this.setState({modal: !modal, filteredTaskLists: [],  selectedJobName: null, worker: null})
    if (this.state.taskLists.length < 1) {
      this.sendtime(this.state.date)
    }
  }

  dashboard = () => { 
    // - you can now access this.props.history for navigation
    this.props.history.push({pathname: "/admin/homedash", state: this.state});
  };
  
  render() {
    const {date, onChange, toggle, modal, filteredTaskLists} = this.state

    console.log('filteredTaskLists', filteredTaskLists);
    const year = date.getFullYear() + 543
    const month = []
    month[0] = "มกราคม";
    month[1] = "กุมภาพันธ์";
    month[2] = "มีนาคม";
    month[3] = "เมษายน";
    month[4] = "พฤษภาคม";
    month[5] = "มิถุนายน";
    month[6] = "กรกฏาคม";
    month[7] = "สิงหาคม";
    month[8] = "กันยายน";
    month[9] = "ตุลาคม";
    month[10] = "พฤศจิกายน";
    month[11] = "ธันวาคม";
    let thaiMonth = month[date.getMonth()]

    return (
      <>
        <div className="content">
          {this.state.alert}
          <button
            className="btn btn-primary d-flex"
            onClick={this.dashboard}
            >
              <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
              <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>กลับหน้าแดชบอร์ด</p>
          </button>
          <Row>
            <Col className="ml-auto mr-auto" md="10">
              <Card className="card-calendar">
                <CardBody>
                  <p style={{color:"#0dc5ca", fontSize:"20px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginBottom: '-4px'}}>{year} {thaiMonth}</p>
                  <Calendars
                    locale={'th'}
                    className={'border-0'}
                    onChange={async (e) =>{ await this.setState({date: e}); await this.setState({filteredTaskLists: []}); await this.sendtime(e)}}
                    value={date}
                  />
                 
                </CardBody>
              </Card>
              <div style={{width: '100%', display: 'flex'}} className="justify-content-between">
                <p style={{fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px'}}>รายการงาน</p>
                <i style={{fontWeight: 'bold', fontSize: '20px', color: '#0dc5ca'}} className={`nc-icon nc-icon nc-bullet-list-67`} onClick={() => this.setState({modal: !modal})}/>
              </div>
              {this.renderWorkLists((this.state.filteredTaskLists.length > 0) ? this.state.filteredTaskLists : this.state.taskLists)}
              {/* {this.state.filteredTaskLists ? this.renderWorkLists(this.state.filteredTaskLists) : this.renderWorkLists(this.state.taskLists)} */}
              <div className="float-right" style={{position: 'fixed', bottom: '10%', right:'10%'}}>
              <a href="/admin/job-assignment">
                <button class="button button-round"><i style={{fontWeight: 'bold', fontSize: '20px', color: 'white'}} className={`nc-icon nc-icon nc-simple-add mt-1`} /></button>
              </a>
              </div>
            </Col>
          </Row>
          <AdminNavBar pageName="ปฏิทินมอบหมายงาน"/>
          <AdminFooter active={true}/>
          <Modal isOpen={modal} className="custom-modal-style">
            <ModalHeader className="">
              <div className="d-flex justify-content-center">
                <h5 className="text-center" style={{color:"black", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}> ตัวกรองงาน </h5>
                <i style={{position: "absolute", right: '5%'}} className="far fa-times-circle fa-2x text-danger" onClick={() => this.setState({modal: !modal})}></i>
              </div>
            </ModalHeader>
                <ModalBody>
                    <Row>
                      <div style={{width: '100%', display: 'flex'}} className="justify-content-between mt-3">
                          <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '12px'}}>เลือกงานที่ต้องการ</p>
                      </div>
                      <Col lg="5" md="6" sm="3">
                        <Select
                        isSearchable={false}
                        className="react-select primary mb-3 bg-white"
                        classNamePrefix="react-select"
                        name="jobName"
                        value={this.state.selectedJobName}
                        onChange={value =>
                            this.handleOnSelectJobName( value )
                        }
                        options={this.state.jobNameOptions}
                        placeholder="กรุณาเลือกงานที่ต้องการ"
                        />
                      </Col>
                      <div style={{width: '100%', display: 'flex'}} className="ml-0 mb-3">
                          <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '16px'}}>พนักงาน</p>
                      </div>
                      <Col lg="5" md="6" sm="3">
                          <Select
                          isSearchable={false}
                          className="react-select primary mb-3 bg-white"
                          classNamePrefix="react-select"
                          name="worker"
                          value={this.state.worker}
                          onChange={(value, index) =>
                              this.handleWorkerOnChage(value)
                          }
                          options={this.state.workerOptions}
                          placeholder="กรุณาเลือกพนักงาน"
                          />
                      </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                <Button color="secondary" onClick={() => this.handleClearFilter()}>ล้างตัวกรอง</Button>
                <Button className="mr-3" color="primary" onClick={() => this.onSubmit()}>กรองงาน</Button>{' '}
            </ModalFooter>
        </Modal>
        </div>
      </>
    );
  }
}

export default withRouter(Calendar);
