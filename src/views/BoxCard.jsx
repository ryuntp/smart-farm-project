import axios from 'axios';
import React, { useState,useEffect } from "react";
import Select from "react-select";
// react plugin used to create charts
import { Line, Bar, Doughnut } from "react-chartjs-2";
// react plugin for creating vector maps
import { VectorMap } from "react-jvectormap";
import {  Pie } from "react-chartjs-2";
import AdminFooter from "../components/AdminNavbarFooter/AdminFooter"
// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Label,
  Form,
  FormGroup, 
  Input,
  Table,
  Row,
  Col,
  Collapse,
  UncontrolledTooltip,
  Modal, ModalHeader, ModalBody, ModalFooter
} from "reactstrap";
import Steps, {Step} from "rc-steps"
import { CircularProgressbar, buildStyles, CircularProgressbarWithChildren } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import { readConfigFile } from 'typescript';
import Cookies from 'js-cookie'
const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"

const BoxCard = (props) => {
    const { allbox, index } = props
    const [modal, setModal] = useState(false)
    const [isOpen, setIsOpen] = useState(false)
    const [id,setid]=useState(allbox.box_id)
    const [status, setstatus] = useState(allbox.box_status)
    const [enemy, setenemies] = useState(allbox.box_enemies)
    const [enemyid, setenemiesid] = useState(allbox.enemies_id)
    const [bac, setbac] = useState(allbox.box_bac)
    const [coconut, setcoconut] = useState(allbox.box_coconut)
    const [wb, setwb] = useState(allbox.box_whitebac)
    const [gb, setgb] = useState(allbox.box_greenbac)
    const [checkwb,setcheckw] = useState(false)
    const [checkgb,setcheckg] = useState(false)
    const [toggleEnemyReport, setToggleEnemyReport] = useState(false)
    const [toggleBacteriaReport, setToggleBacteriaReport] = useState(false)
    const [boxdetail,setboxdetail] = useState([])
    const [start,setstart] = useState(null)
    const [end,setend] = useState(null)

    const toggle = () => setModal(!modal);
    const [species, setSpecies] = useState([
        {id: 1, name: "สะดิ้ง"},
        {id: 2, name: "ทองดำ"},
        {id: 3, name: "ทองแดง"},
    ])
    const handleSubmit2 = (e) => {
        setenemiesid(e.target.value)
    }

    const Boxdetail = () => {
        axios.get(`${URL}/showboxdetail/${allbox.cricket_id}`)
        .then(res => {
            if(res.data.duration){
            setboxdetail(res.data[0]); 
            const start = new Date(res.data[0].cricket_initstamp).toLocaleDateString('en-GB');
            const end = new Date(res.data[0].outboxdate).toLocaleDateString('en-GB');
            setstart(start)
            setend(end)
            }
           })
        
      
    }

    const togglecheckw = () => {
        
        if (wb == "1"){
            setwb("0")
        }
        if (wb == "0"){
            setwb("1")
        }
    };
    const togglecheckg = () => {
        if (gb == "1"){
            setgb("0")
           
        }
        if (gb == "0"){
            setgb("1")
           
        }
    };

    const togglecoconut = () => {
        if (coconut == "ปกติ"){
            setcoconut("ไม่ปกติ")
           
        }
        if (coconut == "ไม่ปกติ"){
            setcoconut("ปกติ")
           
        }
    };


    const handleNormalBacteriaReport = () => {
         setbac("ปกติ")
         setToggleBacteriaReport(false)
         checkstatus();
    }

    const handleAbNormalBacteriaReport = () => {
         setbac("ไม่ปกติ")
         setToggleBacteriaReport(!toggleBacteriaReport)
         checkstatus();
    }
    // const Suinormal = () => {
    //      setcoconut("ปกติ")
    //      checkstatus();
    // }
    // const Suiabnormal = () => {
    //      setcoconut("ไม่ปกติ")
    //      checkstatus();
    // }

    const renderPonds = (ponds) => {
        if (!ponds) { return }
        return  ponds.map((pond) => (
            pond + " "
        ))
    }

    const checkstatus = async () => {
        if(enemy =='ปกติ'){
        
            setToggleEnemyReport(false)
            setenemiesid("0")
        }
        else{
            setToggleEnemyReport(true)
        }

        if(bac=='ปกติ'){
            setToggleBacteriaReport(false)    
        }
        else{
            setToggleBacteriaReport(true)
        }

        
        
        if(enemy =='ปกติ' && bac == 'ปกติ' ){
            await setstatus("ปกติ")
          const data = {
            status:"ปกติ",
            bac:bac,
            coconut:coconut,
            enemy:enemy,
            green:gb,
            white:wb,
            enemyid:enemyid,
            workerType: Cookies.get('workerType'),
            workerId:Cookies.get('workerId')
        }
          await axios({
          method: 'post',
          url: `${URL}/updateboxstatus/${id}/${Cookies.get('workerId')}`,
          data: data
        })
        // .then(res => {
        //     console.log(res)})

    }
    else{
        await setstatus("ไม่ปกติ")
        const data = {
            status:"ไม่ปกติ",
            bac:bac,
            coconut:coconut,
            enemy:enemy,
            green:gb,
            white:wb,
            enemyid:enemyid,
            workerType: Cookies.get('workerType'),
            workerId:Cookies.get('workerId')
        }
         await axios({
         method: 'post', 
         url: `${URL}/updateboxstatus/${id}/${Cookies.get('workerId')}`,
         data: data
         })
       
     }

    }

    const getEnemyLabel = (id) => {
        console.log('idid', id);
        switch (id) {
          case "2":
            return "ไรขาว"
          case "3":
            return "ไรแดง"
          case "4":
            return "มด"
          case "5":
            return "จิ้งจก"
          case "6":
            return "แมลงสาบ"
          case "7":
            return "แมงมุม"
          case "8":
            return "นก"
          case "9":
            return "อื่นๆ"
          default:
            return "ระบุศัตรูจิ้งหรีด"
        }
      }

    useEffect(()=>{
        checkstatus();
        Boxdetail()
        
      }, [enemy,toggleBacteriaReport,enemyid,bac,coconut,wb,gb,toggleEnemyReport,checkwb,checkgb]);

      useEffect(()=>{
        if (wb == 1){
            setcheckw(true)
        }
        if (gb == 1){
            setcheckg(true)
        }
        if (wb == 0){
            setcheckw(false)
        }
        if (gb == 0){
            setcheckg(false)
        }
      
        
      }, []);
      
 

    //   console.log(enemy)
    return (
        <Col lg="3" md="6" sm="6" className="mt-2" key={index}>
              <Card className="card-stats mt-2">
                <CardBody>
                  <Row onClick={()=> setIsOpen(!isOpen)}>
                    <Col xs="12">
                        <div className="w-100" style={{display: 'flex', marginBottom: '-18px'}}>
                          <p className="ml-3" style={{fontColor: "white", fontSize:"16px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>กล่อง {allbox.box_name}</p>
                          <div className="border" style={{height: '25px', borderRadius: '4px', backgroundColor: status === "ปกติ" ? '#09c676' : '#e34849', color:'white', textAlign: 'center', marginLeft: '8px', width: '50px', margin: "-2px 0 0 auto"}}>
                            <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px'}}>{status}</p>
                          </div>
                          <div className="mt-1 w-50 ml-3">
                            <i className="nc-icon nc-minimal-down" style={{color:'black', fontWeight: 'bold', float: 'right'}} />
                          </div>
                        </div>
                        <hr />
                    </Col>
                  </Row>
                  <Collapse isOpen={isOpen}>
                    {/* <Row className="d-flex justify-content-center">
                        <div className="mb-3">
                        <Steps current={3}>
                            <Step icon={<i className={`nc-icon nc-check-2`} />}/>
                            <Step icon={<i className={`nc-icon nc-check-2`} />}/>
                            <Step icon={<i className={`nc-icon nc-check-2`} />}/>
                            <Step/>
                            <Step/>
                        </Steps>
                        </div>
                    </Row> */}
                    <Row>
                    <div className="mb-3 w-25 p-3 text-center" style={{backgroundColor: '#e7e7e7', borderRadius:"20px", margin: "auto", textAlignLast: "center"}}>
                    <p style={{fontColor:"#000000", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>{boxdetail.duration} วัน</p>
                    </div>
                    </Row>
                    <Row>
                        <div className="col-4" style={{borderRight: "1px solid #d3d3d3"}}>
                            <p className="text-center w-100" style={{fontSize:"10px", fontFamily: 'IBM Plex Sans Thai', marginBottom: '4px'}}>วันที่นำไข่มาลง</p>
                            <p className="text-center w-100" style={{fontSize:"10px", fontFamily: 'IBM Plex Sans Thai'}}>{start}</p>
                        </div>
                        <div className="col-4" style={{borderRight: "1px solid #d3d3d3"}}>
                            <p className="text-center w-100" style={{fontSize:"10px", fontFamily: 'IBM Plex Sans Thai', marginBottom: '4px'}}>เอาตัวอ่อนออก</p>
                            <p className="text-center w-100" style={{color:"red", fontSize:"10px", fontFamily: 'IBM Plex Sans Thai'}}>{end}</p>
                        </div>
                        <div className="col-4">
                            <p className="text-center w-100" style={{fontSize:"10px", fontFamily: 'IBM Plex Sans Thai', marginBottom: '4px'}}>บ่อที่นำไข่มา</p>
                            <p className="text-center w-100" style={{fontSize:"10px", fontFamily: 'IBM Plex Sans Thai'}}>{boxdetail.cricket_frompound}</p>
                            {/* <p className="text-center w-100" style={{fontSize:"10xpx", fontFamily: 'IBM Plex Sans Thai'}}>2,3,4</p> */}
                        </div>
                    </Row>
                    <Row className="mt-4">
                        <div className="row mt-3 ml-4" style={{position: "relative"}}>
                            <p className='mt-2' style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '14px'}}>ศัตรูจิ้งหรีด</p>
                        </div>
                    </Row>
                    <Row className="mt-4 mb-2 w-75" style={{marginLeft: '50px'}}>
                        <div className="row" style={{width: '100%', height: '40px'}}>
                            <div className="col" style={{backgroundColor: enemy === "ปกติ" ? "#08d1c0" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '8px', borderBottomLeftRadius: '8px'}} onClick={()=> setenemies("ปกติ")}>
                                <p style={{color: enemy === "ปกติ" ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่พบศัตรู</p>
                            </div>
                            <div className="col" style={{backgroundColor: enemy === "ปกติ" ? "white" : "#f1b825", padding: '0px 0px 0px 0px', display: 'relative', border:'solid 1px lightgray', borderLeft: 'none', borderTopRightRadius: '8px', borderBottomRightRadius: '8px'}} onClick={() => setenemies("ไม่ปกติ")}>
                                <p style={{color: enemy === "ปกติ" ? "gray" : "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>พบศัตรู</p>
                            </div>
                        </div>
                    </Row>
                    <Collapse isOpen={toggleEnemyReport}>
                        <Row className="mt-3">
                            <div className="row ml-4" style={{position: "relative"}}>
                            <p className='' style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '14px'}}>ชนิดศัตรู</p>
                            </div>
                        </Row>
                        <Row >
            <div className="w-100">
              <Form className="mt-6 ml-4  ">
                <FormGroup style={{display: "flex"}}>
                  
                <Select size="3"
                 isSearchable={false}
                  className="mr-3 w-100"
                  classNamePrefix="react-select"
                  name="enemy"
                  value={((enemyid && enemyid !== "0") ? {value: enemyid, label: getEnemyLabel(enemyid)} : null)}
                  onChange={value =>
                    setenemiesid(value.value)
                  }
                  options={[
                      {
                      value: "",
                      label: "ศัตรูจิ้งหรีด",
                      isDisabled: true
                      },
                      { value: "2", label: "ไรขาว" },
                      { value: "3", label: "ไรแดง" },
                      { value: "4", label: "มด" },
                      { value: "5", label: "จิ้งจก" },
                      { value: "6", label: "แมลงสาบ" },
                      { value: "7", label: "แมงมุม" },
                      { value: "8", label: "นก" },
                      { value: "9", label: "อื่นๆ" }
                  ]}
                  placeholder="ระบุศัตรูจิ้งหรีด"
                  />
                {/* <a href={'/admin/hatchery?hatchery_id=' + allbox.egg_id}>
                <Button className="btn-round mt-0 ml-4" color="info" >
                ยืนยัน
                </Button>
                </a> */}
                
                </FormGroup>
                    </Form>
                  </div>
                </Row>
                </Collapse>
                    <Row className="mt-4">
                        <div className="row mt-3 ml-4" style={{position: "relative"}}>
                            <p className='mt-2' style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '14px'}}>เชื้อรา</p>
                        </div>
                    </Row>
                    <Row className="mt-4 mb-4 w-75" style={{marginLeft: '50px'}}>
                        <div className="row" style={{width: '100%', height: '40px'}}>
                            <div className="col" style={{backgroundColor: bac === "ปกติ" ? "#08d1c0" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '8px', borderBottomLeftRadius: '8px'}} onClick={()=> setbac("ปกติ")}>
                                <p style={{color: bac === "ปกติ" ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่พบเชื้อรา</p>
                            </div>
                            <div className="col" style={{backgroundColor: bac === "ปกติ" ? "white" : "#f1b825", padding: '0px 0px 0px 0px', display: 'relative', border:'solid 1px lightgray', borderLeft: 'none', borderTopRightRadius: '8px', borderBottomRightRadius: '8px'}} onClick={() => setbac("ไม่ปกติ")}>
                                <p style={{color: bac === "ปกติ" ? "gray" : "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>พบเชื้อรา</p>
                            </div>
                        </div>
                    </Row>
                    <Collapse isOpen={toggleBacteriaReport}>
                        <Col sm="10" className="mb-2">
                            <FormGroup check inline>
                                <Label check>
                                    <Input type="checkbox" onClick={()=> togglecheckw()} defaultChecked={ wb === "1" ? true : false}/>
                                    <span className="form-check-sign" />เชื้อราสีขาว
                                </Label>
                            </FormGroup>{" "}
                            <FormGroup check inline>
                                <Label check>
                                    <Input type="checkbox" onClick={()=> togglecheckg()} defaultChecked={ gb === "1" ? true : false}/>
                                    <span className="form-check-sign" />เชื้อราสีเขียว
                                </Label>
                            </FormGroup>{" "}
                        </Col>
                    </Collapse>
                    <Row className="mt-3">
                        <div className="row ml-4" style={{position: "relative"}}>
                        <p className='' style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '14px'}}>ซุยขุยมะพร้าว</p>
                        </div>
                    </Row>
                    
                    <Row className="mt-4" style={{marginLeft: '50px'}}>
                        {/* <div className="row" style={{width: '100%', height: '40px'}}>
                            <div className="col" style={{backgroundColor: coconut === "ปกติ" ? "#08d1c0" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '8px', borderBottomLeftRadius: '8px'}} >
                                <p style={{color: coconut === "ปกติ" ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่พบการซุย</p>
                            </div>
                            <div className="col" style={{backgroundColor: coconut === "ปกติ" ? "white" : "#f1b825", padding: '0px 0px 0px 0px', display: 'relative', border:'solid 1px lightgray', borderLeft: 'none', borderTopRightRadius: '8px', borderBottomRightRadius: '8px'}} onClick={()=> setcoconut("ไม่ปกติ")}>
                                <p style={{color: coconut === "ปกติ" ? "gray" : "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>พบการซุย</p>
                            </div>
                        </div> */}
                    </Row>
                    <Col sm="10" className="mb-2">
                            <FormGroup check inline>
                                <Label check>
                                    <Input type="checkbox" onClick={()=> togglecoconut()} defaultChecked={ coconut === "ไม่ปกติ" ? true : false}/>
                                    <span className="form-check-sign" />พบการซุย
                                </Label>
                            </FormGroup>{" "}
                            
                    </Col>
                    
                  
                  </Collapse>
                </CardBody>
              </Card>
            </Col>
    )
}   

export default BoxCard;
