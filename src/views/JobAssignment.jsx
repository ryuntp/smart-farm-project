import React from "react";
import axios from "axios"
// react plugin used to create datetimepicker
import ReactDatetime from "react-datetime";
import moment from "moment";
import { Moment } from "moment";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select from "react-select";
import AdminFooter from "../components/AdminNavbarFooter/AdminFooter"
// react plugin used to create switch buttons
import Switch from "react-bootstrap-switch";
// plugin that creates slider
import Slider from "nouislider";
import AdminNavBar from '../components/Navbars/AdminNavbar'
import { Redirect } from 'react-router-dom';
import Cookies from 'js-cookie'
import _ from "lodash"
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  FormGroup,
  Progress,
  Row,
  Col,
  Label,
  Input
} from "reactstrap";
import '../assets/css/style.css'
import { withRouter } from "react-router-dom";

// core components
import ImageUpload from "components/CustomUpload/ImageUpload.jsx";
const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
class JobAssignment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedJobName: null,
      worker: null,
      insectarium: null,
      pond: [],
      singleSelect2: null,
      singleSelect3: null,
      multipleSelect: null,
      jobDate: null,
      startTime: "07:00",
      endTime: "08:00",
      jobFrequency: 4,
      boxNumber: null,
      note: null,
      jobNameOptions: [{
        value: "",
        label: "Single Option",
        isDisabled: true
        }],
       managerTasks: null,
       allInsectariums: null,
       insectariumOptions: [],
       selectedInsectarium: null,
       selectedBreed: null,
       InsectariumPounds: null,
       poundOptions: [{ label : "ทั้งหมด", value: "all" }],
       workerOptions: [],
       workers: [],
       roomOptions: [],
       selectedRoom: null,
       boxOptions: [{ label : "ทั้งหมด", value: "all" }],
       selectedBoxes: [],
       lotNumber: null,
       dateToggle: false,
       blurDate: false,
       blurStart: false,
       blurEnd: false,
       key: Math.random(),
       workerLoading: false,
       newPound: null,
       newBox: null,
       newPoundError: false,
       newBoxError: false,
       timeErr: false,
       submitLoading: false

    };
  }
  async componentDidMount() {
    const farmId = Cookies.get('farmId')
    await axios.get(`${URL}/allactivityv2/${farmId}`)
      .then(res => {
          this.setState({jobNames: res.data})
          const jobNames = res.data
          if (jobNames) {
            jobNames.map((jn) => (
                this.setState({ jobNameOptions: [...this.state.jobNameOptions, {value: jn.activity_id, label: jn.activity_name }] })
            ))
          }
    })

    await axios.get(`${URL}/allinsectarium/${farmId}`)
      .then(res => {
          const insectariums = res.data
          if (insectariums) {
            insectariums.map((is) => (
                this.setState({ insectariumOptions: [...this.state.insectariumOptions, {value: is.insectarium_name, label: is.insectarium_name }] })
            ))
          }
    })

    // await axios.get(`${URL}/worker/${farmId}`)
    //   .then(res => {
    //       const workers = res.data
    //       if (workers) {
    //         workers.map((worker) => (
    //             this.setState({ workerOptions: [...this.state.workerOptions, {value: worker.worker_id, label: worker.worker_name }] })
    //         ))
    //       }
    // })

    await axios.get(`${URL}/allegg/${farmId}`)
      .then(res => {
          const rooms = res.data
          if (rooms) {
            rooms.map((room) => (
                this.setState({ roomOptions: [...this.state.roomOptions, {value: room.egg_name, label: room.egg_name }] })
            ))
          }
    })
  }

  componentDidUpdate = (prevProps, prevState) => {
      const farmId = Cookies.get('farmId')
      const {jobDate, startTime, endTime, blurEnd, blurDate, blurStart, newPoundError, newBoxError, timeErr} = this.state
    if ((prevState.jobDate !== jobDate) || (prevState.startTime !== startTime) || (prevState.endTime !== endTime) || (prevState.blurEnd !== blurEnd)) {
        if (jobDate && startTime && endTime && blurEnd && blurStart) {
            console.log("SUCCESS")
            this.setState({workerOptions: [], workerLoading: true})
            axios.get(`${URL}/worker/${farmId}`)
            .then(res => {
                const workers = res.data
                console.log('workers', workers);
                if (workers) {
                    workers.map((worker) => (
                        this.setState({ workerOptions: [...this.state.workerOptions, {value: worker.worker_id, label: worker.worker_name }] })
                    ))
                }
                this.setState({workerLoading: false})
            })
        }
    }
  }

  handleTimeChange = (startTime, type) => {
    const hour = startTime.getHours()
    const minuteLength = String(startTime.getMinutes()).length
    const minute = (minuteLength === 1) ? `0` + startTime.getMinutes() : startTime.getMinutes()
    const combined = hour + ":" + minute
    if (type === "start"){
        this.setState({startTime: combined})
    } else {
        this.setState({endTime: combined})
    }
  }

  handleOnSelectJobName = (value) => {
      this.setState({selectedJobName: value})
      axios.get(`${URL}/addmtask/${value.value}`)
      .then(res => {
          this.setState({managerTasks: res.data})
      })
    }

  handleOnSelectInsectarium = (value) => {
    const farmId = Cookies.get('farmId')
      this.setState({selectedInsectarium: value, poundOptions: [{ label : "ทั้งหมด", value: "all" }]})
      this.setState({key: Math.random(), pond: []})
      axios.get(`${URL}/allpoundv2/${farmId}/${value.value}`)
      .then(res => {
          const pounds = res.data
          if (pounds) {
            this.setState({pounds: pounds})
            }
          this.setState({ pondCount: pounds.length })
          if (pounds) {
            pounds.map((pond) => (
                this.setState({ poundOptions: [...this.state.poundOptions, {value: pond.pound_name, label: pond.pound_name }] })
            ))
          }
      })
    }

    handleOnSelectRoom = (value) => {
        this.setState({selectedRoom: value, boxOptions: [{ label : "ทั้งหมด", value: "all" }]})
        axios.get(`${URL}/allbox/${value.value}`)
        .then(res => {
            const boxes = res.data
            if (boxes) {
                this.setState({boxes: boxes})
            }
            this.setState({boxCount: boxes.length})
            if (boxes) {
                boxes.map((box) => (
                  this.setState({ boxOptions: [...this.state.boxOptions, {value: box.box_name, label: box.box_name }] })
              ))
            }
        })
      }

  renderManagerTasks = (tasks) => {
      if (!tasks) { return }
      return (
          tasks.map((task, index) => (
            <Col md ="12" className="mt-2" id={`m_task_${index}`} key={`m_task_${index}`}>
                <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>{task.managertask_name}</small>
                <FormGroup className="mt-3">
                    <Input placeholder="" id={`m_task_${index}`} type="input" onChange={e => this.onChangeManInput(e)}/>
                </FormGroup>
            </Col>
          ))
      )
  }

  renderPoundSelect = () => {
      return (
        <Col className="col-5 ml-4">
            <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>บ่อเลี้ยง</small>
            <FormGroup row className="">
                <Input placeholder="เช่น 1-10" type="input" onChange={e => {this.setState({newPound: e.target.value})}}/>
            </FormGroup>
            {
                this.state.pondCount ?
                <div className="w-100">
                    <small className="mb-2" style={{fontSize:"12px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', color: "red"}}>บ่อที่เลือกได้: 1-{this.state.pondCount}</small>
                </div> :
                null
            }
        </Col>
      )
  }

  renderBoxSelect = () => {
      return (
        <Col className="col-5 ml-4">
            <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>กล่อง</small>
            <FormGroup row className="">
                <Input placeholder="เช่น 1-10" type="input" onChange={e => {this.setState({newBox: e.target.value})}}/>
            </FormGroup>
            {
                this.state.boxCount ?
                <div className="w-100">
                    <small className="mb-2" style={{fontSize:"12px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', color: "red"}}>กล่องที่เลือกได้: 1-{this.state.boxCount}</small>
                </div> :
                null
            }
        </Col>
      )
  }
  onChangeManInput = (e) => {
    let obj = {};
    obj[e.target.id] = e.target.value
    this.setState(obj);
  }
 // box, edit page
  handleNewPoundList = (newPound) => {
    let newPoundList = [];
    let firstVal;
    let lastVal;

    if (!newPound) return

    const regexPound = /^[0-9|{,}| -]*$/gi
    if (!regexPound.test(newPound.trim())) {
        return this.setState({ newPoundError: true })
    }
    if (newPound.includes(",")) {
        newPound.split(",").forEach(pound => {
            if(pound.split("-").length === 1) {
                this.setState({ newPoundError: false })
                newPoundList.push(pound)
            }
            else if(pound.includes("-")){
                this.setState({ newPoundError: false })
                firstVal = parseInt(pound.split("-")[0])
                lastVal = parseInt(pound.split("-")[1])
                if (!firstVal || !lastVal) {
                    return this.setState({ newPoundError: true })
                }
                if (firstVal >= lastVal) {
                    return this.setState({ newPoundError: true })
                }
                for(firstVal; firstVal <= lastVal; firstVal++) {
                    newPoundList.push(firstVal.toString())
                }
            }
        })
    }
    else {
        if(newPound.split("-").length === 1) {
            this.setState({ newPoundError: false })
            newPoundList.push(newPound)
        }
        else if(newPound.includes("-")){
            this.setState({ newPoundError: false })
            firstVal = parseInt(newPound.split("-")[0])
            lastVal = parseInt(newPound.split("-")[1])
            if (!firstVal || !lastVal) {
                return this.setState({ newPoundError: true })
            }
            if (firstVal >= lastVal) {
                return this.setState({ newPoundError: true })
            }
            for(firstVal; firstVal <= lastVal; firstVal++) {
                newPoundList.push(firstVal.toString())
            }
        }
    }
    console.log("newPoundList => ", newPoundList);
    return newPoundList
  }
  handleNewBoxList = (newBox) => {
    let newBoxList = [];
    let firstVal;
    let lastVal;

    if (!newBox) return

    const regexPound = /^[0-9|{,}| -]*$/gi
    if (!regexPound.test(newBox.trim())) {
        return this.setState({ newBoxError: true })
    }
    if (newBox.includes(",")) {
        newBox.split(",").forEach(box => {
            if(box.split("-").length === 1) {
                this.setState({ newBoxError: false })
                newBoxList.push(box)
            }
            else if(box.includes("-")){
                this.setState({ newBoxError: false })
                firstVal = parseInt(box.split("-")[0])
                lastVal =  parseInt(box.split("-")[1])
                if (!firstVal || !lastVal) {
                    return this.setState({ newBoxError: true })
                }
                if (firstVal >= lastVal) {
                    return this.setState({ newBoxError: true })
                }
                for(let i = firstVal; i <= lastVal; i++) {
                    newBoxList.push(i.toString())
                }
            }
        })
    }
    else {
        if(newBox.split("-").length === 1) {
            this.setState({ newBoxError: false })
            newBoxList.push(newBox)
        }
        else if(newBox.includes("-")){
            this.setState({ newBoxError: false })
            firstVal = parseInt(newBox.split("-")[0])
            lastVal = parseInt(newBox.split("-")[1])
            if (!firstVal || !lastVal) {
                return this.setState({ newBoxError: true })
            }
            if (firstVal >= lastVal) {
                return this.setState({ newBoxError: true })
            }
            for(firstVal; firstVal <= lastVal; firstVal++) {
                newBoxList.push(firstVal.toString())
            }
        }
    }
    console.log("newBoxList => ", newBoxList);
    return newBoxList
  }

  handleTimeFormat = (start, end) => {
    if (((start.split(":")[0].length === 2) && (start.split(":")[1].length === 2)) && ((end.split(":")[0].length === 2) && (end.split(":")[1].length === 2)) && ((start[2] === ":") && (end[2] === ":"))) {
        return this.setState({ timeErr: false })
    }
    else { 
         return this.setState({ timeErr: true }) 
    }
  }

  checkValidPound = (finalPoundList) => {
    if (finalPoundList && (finalPoundList.length > this.state.pondCount)) {
        return this.setState({ newPoundError: true })
    }
   
    let pondOptions = []
    if(this.state.pounds) {
        this.state.pounds.forEach(pound => {
            pondOptions.push(pound.pound_name)
        })
    }

    console.log("this.state.pounds ", this.state.pounds);
    console.log("finalPoundList ", finalPoundList);
    console.log("pondOptions ", pondOptions);
    if (finalPoundList) {
        finalPoundList.forEach(pound => {
            if (!_.includes(pondOptions, pound)) {
                return this.setState({ newPoundError: true })
            }
        })
        return finalPoundList
    }
    else return "err"
  }

  checkValidBox = (finalBoxList) => {
    if (finalBoxList && (finalBoxList.length > this.state.boxCount)) {
        return this.setState({ newBoxError: true })
    }
   
    let pondOptions = []
    if(this.state.boxes) {
        this.state.boxes.forEach(box => {
            pondOptions.push(box.box_name)
        })
    }
    if (finalBoxList) {
        finalBoxList.forEach(box => {
            if (!_.includes(pondOptions, box)) {
                return this.setState({ newBoxError: true })
            }
        })
        return finalBoxList
    }
    else return "err"
  }

  onSubmit = async() => {
    const farmId = Cookies.get('farmId')

    const { newBoxError, timeErr, newPoundError, lotNumber, selectedRoom, selectedBoxes, workers, worker, managerTasks, selectedJobName, jobDate, startTime, endTime, jobFrequency, note, insectarium, pond, selectedInsectarium, selectedBreed, newPound, newBox } = this.state
    let adminWorks = []
    if (managerTasks) {
        for(let i=0; i < managerTasks.length; i++){
            adminWorks.push(this.state[`m_task_${i}`])
        }
    }

    this.handleTimeFormat(startTime, endTime)

    let finalPoundList = newPound ? this.handleNewPoundList(newPound) : "eiei"
    let finalBoxList = newBox ? this.handleNewBoxList(newBox) : "eiei"

    let validPounds = finalPoundList!== "eiei" ? this.checkValidPound(finalPoundList) : null
    let validBoxes =finalBoxList!=="eiei" ? this.checkValidBox(finalBoxList) : null
    console.log("validPounds ", validPounds);
    console.log("validBoxes ", validBoxes);

    let isValid = true;
    let isValidDate = true;
    if (((startTime.split(":")[0].length === 2) && (startTime.split(":")[1].length === 2)) && ((endTime.split(":")[0].length === 2) && (endTime.split(":")[1].length === 2)) && ((startTime[2] === ":") && (endTime[2] === ":"))) {
        console.log("eieieei");
    } else { isValidDate = false }
    let selectedIds = managerTasks ? managerTasks.map(item => 
        {
        return (
            item.managertask_id
        )
        }) : []

    let pondOptions = []
    if(this.state.pounds) {
        this.state.pounds.forEach(pound => {
            pondOptions.push(pound.pound_name)
        })
    }
    if (finalPoundList && Array.isArray(finalPoundList)) {
        finalPoundList.forEach(pound => {
            if (!_.includes(pondOptions, pound)) {
                isValid = false
                this.setState({ newPoundError: true })
            }
        })
    }

    let boxOptions = []
    if(this.state.boxes) {
        this.state.boxes.forEach(pound => {
            boxOptions.push(pound.box_name)
        })
    }
    if (finalBoxList && Array.isArray(finalBoxList)) {
        finalBoxList.forEach(pound => {
            if (!_.includes(boxOptions, pound)) {
                isValid = false
                this.setState({ newBoxError: true })
            }
        })
    }

    const submittedData = {
        farmId: farmId,
        selectedJobName: selectedJobName ? selectedJobName.value : null,
        jobDate: jobDate,
        startTime: startTime,
        endTime: endTime,
        jobFrequency: jobFrequency,
        note: note,
        adminWorks: adminWorks,
        insectarium: _.get(selectedInsectarium, 'value', null),
        pond: finalPoundList && finalPoundList !== "eiei" ? _.uniq(finalPoundList) : [],
        worker: workers,
        adminWorkIds: selectedIds,
        selectedRoom: _.get(selectedRoom, 'value', null),
        selectedBoxes: finalBoxList && finalBoxList !== "eiei" ? _.uniq(finalBoxList) : [],
        lotNumber: lotNumber,
        selectedBreed:  _.get(selectedBreed, 'value', null)
    }

    console.log('submittedData', submittedData)

    if( isValidDate !== false && isValid !== false && validPounds !== "err"  && validPounds !== undefined && validBoxes !== "err"  && validBoxes !== undefined) {
        this.setState({ submitLoading: true })
        console.log("true")
        await axios({
            method: 'post',
            url: 'https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com/updateassignv2',
            data: submittedData
        }).then(
            res => {
                if (res.status === 200){
                    this.setState({statusResponse: 200, submitLoading: false})
                    console.log('resresres', res);
                }
                else {
                    return
                }
            }
        )
    }  else { return console.log("wronggg"); }
    }

    handlePoundOnChage = (value, e) => {
        if(e && (_.get(e, 'action', null) === "remove-value")) {
            let duplicatedPond = [...this.state.pond]
            _.remove(duplicatedPond, function(n) {
                if (n ===  _.get(e, "removedValue.value", null)) {
                    return n
                }
            })

            return this.setState({pond: duplicatedPond})
        }
        else if (e && (_.get(e, 'action', null) === "clear")) { return }
        
        const pondLength = value.length - 1
        this.setState({ pond: this.state.pond.concat(value[pondLength].value) })
    }

    handleBoxOnChage = (value, e) => {
        if(e && (_.get(e, 'action', null) === "remove-value")) {
            let duplicatedSelectedBoxes = [...this.state.selectedBoxes]
            _.remove(duplicatedSelectedBoxes, function(n) {
                if (n ===  _.get(e, "removedValue.value", null)) {
                    return n
                }
            })

            return this.setState({selectedBoxes: duplicatedSelectedBoxes})
        }
        const boxLength = value.length - 1
        this.setState({ selectedBoxes: this.state.selectedBoxes.concat(value[boxLength].value) })
    }

    handleWorkerOnChage = (value, e) => {
        if(e && (_.get(e, 'action', null) === "remove-value")) {
            let duplicatedWorkers = [...this.state.workers]
            _.remove(duplicatedWorkers, function(n) {
                if (n ===  _.get(e, "removedValue.value", null)) {
                    return n
                }
            })

            return this.setState({workers: duplicatedWorkers})
        }
        const workerLength = value.length - 1
        this.setState({ workers: this.state.workers.concat(value[workerLength].value) })
    }

    renderPondValue = (ponds) => {
        let o = []
        if (!ponds) { return }
        ponds.map((pond) => (
            o.push(pond + " ")
        ))
    }

    renderWorkerValue = (workers) => {
        let o = []
        if (!workers) { return }
        workers.map((worker) => (
            o.push(worker + " ")
        ))
    }

    calendar = () => { 
        // - you can now access this.props.history for navigation
        this.props.history.push({pathname: "/admin/calendar", state: this.state});
      };

  render() {
      const { dateToggle, lotNumber, boxOptions, selectedBoxes, roomOptions, selectedRoom, pond, workers, worker, selectedJobName, jobNameOptions, managerTasks, insectariumOptions, selectedInsectarium, poundOptions, workerOptions, selectedBreed } = this.state
      if (this.state.statusResponse === 200) {
        return (
            <Redirect to="/admin/calendar" />
        )
    }
    return (
      <>
        <div className="content">
            <button
                className="btn btn-primary d-flex"
                onClick={this.calendar}
                >
                <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
                <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>กลับหน้าจัดการงาน</p>
            </button>
          <Row>
            <div style={{width: '100%', display: 'flex'}} className="justify-content-between mt-3">
                <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '12px'}}>เลือกงานที่ต้องการ</p>
                <a href={'/admin/newjobtype'}>
                    <i style={{fontWeight: 'bold', fontSize: '18px', color: '#0dc5ca'}} className={`nc-icon nc-simple-add mt-1 mr-2`} />
                </a>
            </div>
            <Col lg="5" md="6" sm="3">
                <Select
                className="react-select primary mb-3 bg-white"
                classNamePrefix="react-select"
                name="jobName"
                isSearchable={ false }
                value={selectedJobName}
                onChange={value =>
                    this.handleOnSelectJobName( value )
                }
                options={jobNameOptions}
                placeholder="กรุณาเลือกงานที่ต้องการ"
                />
            </Col>
            <Row className="w-100 ml-0 mt-2">
                <Col className="col-12 mt-2">
                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>วันที่ / เวลา</small>
                    <FormGroup className="mt-3">
                        <ReactDatetime
                        dateFormat="DD-MM-YYYY"
                        open={dateToggle}
                        onBlur={()=> this.setState({dateToggle: false})}
                        inputProps={{
                            className: "form-control date-assign",
                            placeholder: "กรุณาใส่วันที่",
                            backgroundColor: 'white',
                            readOnly: true,
                            onClick: () => this.setState({dateToggle: true}),
                        }}
                        timeFormat={false}
                        onChange={value =>
                            (this.setState({ jobDate: moment(value.toDate()).format('MM-DD-YYYY'), dateToggle: false}))
                        }
                        />
                  </FormGroup>
                </Col>
                <Col className="col-12 mt-2">
                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>เวลาเริ่ม</small>
                    <FormGroup className="mt-3">
                        <ReactDatetime
                        onBlur={()=> this.setState({blurStart: true})}
                        onFocus={()=> this.setState({blurStart: false})}
                        defaultValue="07:00"
                        // pattern="[0-9]*"
                        inputProps={{
                            className: "form-control bg-white",
                            placeholder: "กรุณาใส่เวลา",
                            pattern: "[0-9]*"
                            // readOnly: true,
                        }}
                        timeFormat={"HH:mm"}
                        dateFormat={false}
                        onChange={async value => { 
                                   // this.handleTimeChange(value.toDate(), 'start')
                               let timeFormat = await value._d ? `${value._d.toString().split(" ")[4].split(":")[0]}:${value._d.toString().split(" ")[4].split(":")[1]}` : value
                                await this.setState({startTime: timeFormat })
                            }
                        }
                        />
                  </FormGroup>
                </Col>
                <Col className="col-12 mt-2">
                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>เวลาจบ</small>
                    <FormGroup className="mt-3">
                        <ReactDatetime
                        onBlur={()=> this.setState({blurEnd: true})}
                        onFocus={()=> this.setState({blurEnd: false})}
                        defaultValue="08:00"
                        inputProps={{
                            className: "form-control  bg-white",
                            placeholder: "กรุณาใส่เวลา",
                            pattern: "[0-9]*"
                            // readOnly: true,
                        }}
                        timeFormat={"HH:mm"}
                        dateFormat={false}
                        onChange={async value => { 
                            // this.handleTimeChange(value.toDate(), 'start')
                            let timeFormat = await value._d ? `${value._d.toString().split(" ")[4].split(":")[0]}:${value._d.toString().split(" ")[4].split(":")[1]}` : value
                            await this.setState({endTime: timeFormat })
                            }
                        }
                        />
                  </FormGroup>
                </Col>
            </Row>
            <Col className="mt-2">
                <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>ทำซ้ำ</small>
            </Col>
            <Col lg="3" sm="6" className="mt-4">
             <div className="form-check-radio d-flex">
                    <Label check>
                    <Input
                        defaultChecked
                        defaultValue={4}
                        id="exampleRadios2"
                        name="exampleRadios"
                        type="radio"
                        onChange={value => this.setState({jobFrequency: 4})}
                    />
                    <span className="form-check-sign" />
                    </Label>
                    <small className="ml-2" style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai'}}>ทำครั้งเดียว</small>
                </div>
                <div className="form-check-radio d-flex">
                    <Label check>
                    <Input
                        defaultValue={1}
                        id="exampleRadios1"
                        name="exampleRadios"
                        type="radio"
                        onChange={value => this.setState({jobFrequency: 1})}
                    />
                    <span className="form-check-sign" />
                    </Label>
                    <small className="ml-2" style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai'}}>ทำซ้ำรายวัน</small>
                </div>
                <div className="form-check-radio d-flex">
                    <Label check>
                    <Input
                        defaultValue={2}
                        id="exampleRadios2"
                        name="exampleRadios"
                        type="radio"
                        onChange={value => this.setState({jobFrequency: 2})}
                    />
                    <span className="form-check-sign" />
                    </Label>
                    <small className="ml-2" style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai'}}>ทำซ้ำรายสัปดาห์</small>
                </div>
                <div className="form-check-radio d-flex">
                    <Label check>
                    <Input
                        defaultValue={3}
                        id="exampleRadios3"
                        name="exampleRadios"
                        type="radio"
                        onChange={value => this.setState({jobFrequency: 3})}
                    />
                   <span className="form-check-sign" />
                    </Label>
                    <small className="ml-2" style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai'}}>ทำซ้ำรายเดือน</small>
                </div>
            </Col>
            <div style={{width: '100%', display: 'flex'}} className="ml-0 mb-3">
                <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '16px'}}>พนักงาน</p>
            </div>
            <Col lg="5" md="6" sm="3">
                <Select
                isMulti
                isLoading={ this.state.workerLoading }
                isSearchable={ false }
                className="react-select primary mb-3 bg-white"
                classNamePrefix="react-select"
                name="worker"
                value={this.renderWorkerValue(workers)}
                onChange={(value, e) =>
                    this.handleWorkerOnChage(value, e)
                }
                options={workerOptions}
                placeholder="กรุณาเลือกพนักงาน"
                />
            </Col>
            {this.renderManagerTasks(managerTasks)}
            {((_.get(selectedJobName,'value', null) === '21') || (_.get(selectedJobName,'value', null) === '26') || (_.get(selectedJobName,'value', null) === '36') || (_.get(selectedJobName,'value', null) === '37') ) ? 
                <div className="w-100 ml-4 mt-3">
                    <Row>
                        <Col className="col-5">
                            <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>ห้องอบไข่</small>
                            <Select
                                isSearchable={ false }
                                className="react-select primary mb-3 bg-white"
                                classNamePrefix="react-select"
                                name="singleSelect"
                                value={selectedRoom}
                                onChange={value =>
                                    this.handleOnSelectRoom(value)
                                }
                                options={roomOptions}
                                placeholder=""
                            />
                        </Col>
                        {/* <Col className="col-5 ml-4">
                            <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>กล่อง</small>
                            <Select
                                isMulti
                                isSearchable={ false }
                                className="react-select primary mb-3 bg-white"
                                classNamePrefix="react-select"
                                name="singleSelect2"
                                value={this.renderPondValue(pond)}
                                onChange={(value, e) =>
                                    this.handleBoxOnChage(value, e)
                                }
                                options={boxOptions}
                                placeholder=""
                            />
                        </Col> */}
                        {this.renderBoxSelect()}
                    </Row>
                </div> 
                : (_.get(selectedJobName,'value', null) === '25') ? 
                    <div className="w-100">
                        <div className="w-100 ml-4 mt-3">
                            <Row>
                                <Col className="col-5">
                                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>ห้องอบไข่</small>
                                    <Select
                                        isSearchable={ false }
                                        className="react-select primary mb-3 bg-white"
                                        classNamePrefix="react-select"
                                        name="singleSelect"
                                        value={selectedRoom}
                                        onChange={value =>
                                            this.handleOnSelectRoom(value)
                                        }
                                        options={roomOptions}
                                        placeholder=""
                                    />
                                </Col>
                                {this.renderBoxSelect()}
                            </Row>
                        </div>
                        <p className="text-center mt-1" style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}> ไปที่ </p>
                        <div className="w-100 ml-4 mt-3">
                            <Row>
                                <Col className="col-5">
                                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>โรงเลี้ยง</small>
                                    <Select
                                        isSearchable={ false }
                                        className="react-select primary mb-3 bg-white"
                                        classNamePrefix="react-select"
                                        name="singleSelect"
                                        value={selectedInsectarium}
                                        onChange={value =>
                                            this.handleOnSelectInsectarium(value)
                                        }
                                        options={insectariumOptions}
                                        placeholder=""
                                    />
                                </Col>
                                {this.renderPoundSelect()}
                            </Row>
                        </div>
                    </div> 
                : 
                (_.get(selectedJobName,'value', null) === '30') ?
                <div className="w-100">
                        <div className="w-100 ml-4 mt-3">
                            <Row>
                                <Col className="col-5">
                                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>โรงเลี้ยง</small>
                                    <Select
                                        isSearchable={ false }
                                        className="react-select primary mb-3 bg-white"
                                        classNamePrefix="react-select"
                                        name="singleSelect"
                                        value={selectedInsectarium}
                                        onChange={value =>
                                            this.handleOnSelectInsectarium(value)
                                        }
                                        options={insectariumOptions}
                                        placeholder=""
                                    />
                                </Col>
                                {this.renderPoundSelect()}
                            </Row>
                        </div>
                        <p className="text-center mt-1" style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}> ไปที่ </p>
                        <div className="w-100 ml-4 mt-3">
                            <Row>
                                <Col className="col-5">
                                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>ห้องอบไข่</small>
                                    <Select
                                        isSearchable={ false }
                                        className="react-select primary mb-3 bg-white"
                                        classNamePrefix="react-select"
                                        name="singleSelect"
                                        value={selectedRoom}
                                        onChange={value =>
                                            this.handleOnSelectRoom(value)
                                        }
                                        options={roomOptions}
                                        placeholder=""
                                    />
                                </Col>
                                {this.renderBoxSelect()}
                            </Row>
                        </div>
                    </div> 
                :
                (_.get(selectedJobName,'value', null) === '31') ?
                    <div className="w-100 ml-4 mt-3">
                        <Row>
                            <Col className="col-5">
                                <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>โรงเลี้ยง</small>
                                <Select
                                    className="react-select primary mb-3 bg-white"
                                    classNamePrefix="react-select"
                                    name="singleSelect"
                                    isSearchable={ false }
                                    value={selectedInsectarium}
                                    onChange={value =>
                                        this.handleOnSelectInsectarium(value)
                                    }
                                    options={insectariumOptions}
                                    placeholder=""
                                />
                            </Col>
                            {this.renderPoundSelect()}
                        </Row>
                    </div>
                
                :
                (_.get(selectedJobName,'value', null) === '56') ?
                    <div className="w-100 ml-4 mt-3">
                        <div>
                            <Row>
                                <Col className="col-10">
                                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>สายพันธ์ุ</small>
                                    <Select
                                        className="react-select primary mt-3 mb-3 bg-white"
                                        classNamePrefix="react-select"
                                        name="breed"
                                        value={selectedBreed}
                                        isSearchable={ false }
                                        value={selectedBreed}
                                        onChange={value =>
                                        this.setState({ selectedBreed: value })
                                        }
                                        options={[
                                        { value: "1", label: "สะดิ้ง" },
                                        { value: "2", label: "ทองดำ" },
                                        { value: "3", label: "ทองแดง" }
                                        ]}
                                        placeholder="เลือกสายพันธุ์"
                                    />
                                </Col>
                            </Row>
                        </div>
                        <div className="w-100 mt-3">
                            <Row>
                                <Col className="col-6">
                                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>โรงเลี้ยง</small>
                                    <Select
                                        isSearchable={ false }
                                        className="react-select primary mb-3 bg-white"
                                        classNamePrefix="react-select"
                                        name="singleSelect"
                                        value={selectedInsectarium}
                                        onChange={value =>
                                            this.handleOnSelectInsectarium(value)
                                        }
                                        options={insectariumOptions}
                                        placeholder=""
                                    />
                                </Col>
                                {this.renderPoundSelect()}
                            </Row>
                        </div>
                    </div>
                
                :
                 <div className="w-100 ml-4 mt-3">
                    <Row>
                        <Col className="col-5">
                            <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>โรงเลี้ยง</small>
                            <Select
                                className="react-select primary mb-3 bg-white"
                                classNamePrefix="react-select"
                                name="singleSelect"
                                isSearchable={ false }
                                value={selectedInsectarium}
                                onChange={value =>
                                    this.handleOnSelectInsectarium(value)
                                }
                                options={insectariumOptions}
                                placeholder=""
                            />
                        </Col>
                        {this.renderPoundSelect()}
                    </Row>
                </div>
            }
            <Col className="mt-2">
                <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>หมายเหตุ</small>
                <FormGroup className="mt-3">
                    <Input placeholder="กรอกหมายเหตุที่ต้องการแจ้งพนักงาน" type="textarea" onChange={e => {this.setState({note: e.target.value})}}/>
                </FormGroup>
            </Col>
            <div className="w-100 mt-3">
                {
                    this.state.newPoundError ?
                    <div className="w-100 ml-4">
                        <small className="mb-2" style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', color: "red"}}>กรอกบ่อเลี้ยงไม่ถูกต้อง</small>
                    </div> :
                    null
                }
                {
                    this.state.newBoxError ?
                    <div className="w-100 ml-4">
                        <small className="mb-2" style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', color: "red"}}>กรอกกล่องไม่ถูกต้อง</small>
                    </div> :
                    null
                }
                {
                    this.state.timeErr ?
                    <div className="w-100 ml-4">
                        <small className="mb-2" style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', color: "red"}}>เวลาเริ่ม/จบไม่ถูกต้อง</small>
                    </div>
                     :
                    null
                }
                <Row className='mb-3 justify-content-center'>
                    <Col xs="5">
                        <a href="/admin/calendar">
                            <div className='' style={{borderColor: '#0dc5ca', borderRadius: '8px', height: '5', border:'1px solid #0dc5ca'}}>
                                <p className="text-center" style={{color:"#0dc5ca", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', lineHeight: '40px'}}>ยกเลิก</p>
                            </div>
                        </a>
                    </Col>
                    <Col xs="5">
                        <div className='border' style={{backgroundColor: 'black', borderRadius: '8px', height: '40px'}} onClick={() => this.onSubmit()}>
                            <p className="text-center" style={{color:"white", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', lineHeight: '40px'}}>ยืนยัน</p>
                        </div>
                    </Col>
                </Row>
            </div>
          </Row>
          {this.state.submitLoading ? (
              <button class="btn btn-primary" style={{height: "20%", position: "fixed", top: "50%", left: "50%", width: "50%", transform: "translate(-50%, -50%)" }} type="button" disabled>
                <span class="spinner-border spinner-border-sm" style={{fontSize: "160px"}} role="status" aria-hidden="true"></span>
                <span class="sr-only">Loading...</span>
            </button>
          ) : null}
          <AdminNavBar pageName="มอบหมายงาน"/>
          <AdminFooter active={true}/>
        </div>
      </>
    );
  }
}

export default withRouter(JobAssignment);
