import React from "react";
import axios from "axios"
import ReactDatetime from "react-datetime";
import TagsInput from "react-tagsinput";
import Select from "react-select";
import AdminFooter from "../components/AdminNavbarFooter/AdminFooter"
import { Redirect } from 'react-router-dom';
import AdminNavBar from '../components/Navbars/AdminNavbar'
import { withRouter } from "react-router-dom";
import moment from "moment";
import _ from "lodash"
import Cookies from 'js-cookie'
import {
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Row,
    Col,
    Input,
    Collapse
  } from "reactstrap";

import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
  const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
  class Export extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          workerOptions:[],
          isOpen1: false,
          isOpen2: false,
          isOpen3: false,
          isOpen4: false,
          isOpen5: false,
          isOpen6: false,
          isOpen7: false,
          isGap1: null,
          worker1: null,
          text1: null,
          date1: null,
          time1: null,
          isGap2: null,
          worker2: null,
          text2: null,
          date2: null,
          time2: null,
          isGap3: null,
          worker3: null,
          text3: null,
          date3: null,
          time3: null,
          isGap4: null,
          worker4: null,
          text4: null,
          date4: null,
          time4: null,
          isGap5: null,
          worker5: null,
          text5: null,
          date5: null,
          time5: null,
          abnormalData: null,
          assignedData: null,
          havestData: null,
          FWData:null,
          jobDate:null,
          jobDate2:null,
          dateToggle: false,
          dateToggle2: false,
          
      };
    }
    componentDidMount() {
        const farmId = Cookies.get('farmId')
            axios.get(`${URL}/worker/${farmId}`)
            .then(res => {
                const workers = res.data
                if (workers) {
                workers.map((worker) => (
                this.setState({ workerOptions: [...this.state.workerOptions, {value: worker.worker_id, label: worker.worker_name }] })
            ))
          }
    })
    }

    onSubmit = () => {
        const { isGap1, worker1, text1, date1, time1, note1,
            isGap2, worker2, text2, date2, time2, note2,
            isGap3, worker3, text3, date3, time3, note3,
            isGap4, worker4, text4, date4, time4, note4,
            isGap5, worker5, text5, date5, time5, note5,
            isGap6, worker6, text6, date6, time6, note6,
            isGap7, worker7, text7, date7, time7, note7,
            isGap8, worker8, text8, date8, time8, note8,
            isGap9, worker9, text9, date9, time9, note9,
            isGap10, worker10, text10, date10, time10, note10,
            isGap11, worker11, text11, date11, time11, note11,
            isGap12, worker12, text12, date12, time12, note12,
            isGap13, worker13, text13, date13, time13, note13,
            isGap14, worker14, text14, date14, time14, note14,
            isGap15, worker15, text15, date15, time15, note15,
            isGap16, worker16, text16, date16, time16, note16,
            isGap17, worker17, text17, date17, time17, note17,
            isGap18, worker18, text18, date18, time18, note18,
            isGap19, worker19, text19, date19, time19, note19,
            isGap20, worker20, text20, date20, time20, note20,
            isGap21, worker21, text21, date21, time21, note21,
            isGap22, worker22, text22, date22, time22, note22,
            isGap23, worker23, text23, date23, time23, note23
         } = this.state

        const submittedData = [
            {
            SubTopic1:"1. ฟาร์มตั้งอยู่ในแหล่งที่ไม่มีน้ำท่วมขัง หรือมีการป้องกัน",
            isGap1: isGap1,
            worker1: _.get(worker1, "value", null),
            text1: text1,
            date1: date1,
            time1: time1,
            note1: note1,
            },
            {
            SubTopic2:"2. ตั้งอยูในพื้นที่ที่ไม่ส่งผลกระทบต่อชุมชนโดยรอบ เว้นแต่มีมาตรการป้องกันผลกระทบต่อชุมชน",
            isGap2: isGap2,
            worker2: _.get(worker2, "value", null),
            text2: text2,
            date2: date2,
            time2: time2,
            note2: note2,
            },
            {
            SubTopic3:"3. ตั้งอยู่ในสภาพแวดล้อมที่ไม่เสี่ยงต่อหารปนเปื้อนจากอันตรายทางกายภาพ เคมีและชีวภาพ",
            isGap3: isGap3,
            worker3: _.get(worker3, "value", null),
            text3: text3,
            date3: date3,
            time3: time3,
            note3: note3,
            },
            {
            SubTopic4:"4. สิ่งแวดล้อมในฟาร์มเช่น อุณหภูมิ ความชื้น ฝุ่น ละออง กลิ่น อยู่ในเกณฑ์ที่เหมาะสม ไม่ก่อให้เกิดอันตรายต่อจิ้งหรีดและผู้ปฎิบัติงาน",    
            isGap4: isGap4,
            worker4: _.get(worker4, "value", null),
            text4: text4,
            date4: date4,
            time4: time4,
            note4: note4,
            },
            {
            SubTopic5:"5. การคมนาคมขนส่งเข้า-ออก ฟาร์ม สะดวกต่อการรับ-ส่ง วัสดุ อุปกรณ์ อาหาร และผลิตผล",
            isGap5: isGap5,
            worker5: _.get(worker5, "value", null),
            text5: text5,
            date5: date5,
            time5: time5,
            note5: note5,
            },
            {
            SubTopic6:"1. พื้นที่ฟาร์มมีฃนาดแหมาะสม กล่องเลี้ยงมีขนาดและจำนวนเหมาะสม แต่ละบ่อควรมีระยะห่างจากกันเพื่อป้องกันการปนเปื้อน การติดเชื้อข้าม และเพื่อความสะดวกในการปฎิบัติงาน หรือมีวิธีการปฎิบัติงานในแต่ละบ่อ",
            isGap6: isGap6,
            worker6: _.get(worker6, "value", null),
            text6: text6,
            date6: date6,
            time6: time6,
            note6: note6,
            },
            {
            SubTopic7:"2. ออกแบบพื้นที่การเพาะเลี้ยงเป็นระบบปิด เพื่อป้องกันการหลุดรอดของจิ้งหรีดออกภายนอกกล่องเลี้ยงหรือโรงเพาะเลี้ยง และป้องกันจิ้งหรีดจากธรรมชาติปะปน ป้องกันสัตว์เลี้ยง สัตว์พาหะ ศัตรูจิ้งหรีด รวมทั้งการเข้า-ออกของผู้ไม่เกี่ยวข้อง",
            isGap7: isGap7,
            worker7: _.get(worker7, "value", null),
            text7: text7,
            date7: date7,
            time7: time7,
            note7: note7,
            },
            {
            SubTopic8:"3. ติดตั้งระบบควบคุมอุณหภูมิ ความชื้น และการหมุนเวียนอากาศและตั้งค่าที่เหมาะสมกับกับการเจริญเติบโตของจิ้งหรีด",
            isGap8: isGap8,
            worker8: _.get(worker8, "value", null),
            text8: text8,
            date8: date8,
            time8: time8,
            note8: note8,
            },
            {
            SubTopic9:"4. มีการวางผังฟาร์มที่ดี จัดแยกส่วนพื้นที่เป็นสัดส่วนอย่างชัดเจน เหมาะสมตามวัตถุประสงค์ เช่นบริเวณเลี้ยงจิ้งหรีด เก็บอาหาร เก็บน้ำ เก็บอุปกรณ์เวชภัณฑ์และสารเคมี บริเวณชำระล้างวัสดุอุปกรณ์ บริเวณรวบรวมจิ้งหรีดหลังการเก็บเกี่ยว รวบรวมมูลจิ้งหรีด ขยะและสิ่งปฎิกูล",
            isGap9: isGap9,
            worker9: _.get(worker9, "value", null),
            text9: text9,
            date9: date9,
            time9: time9,
            note9: note9,
            },
            {
            SubTopic10:"5. พื้นที่เลี้ยงแยกส่วนชัดเจนกับที่อยู่อาศัย มีการป้องกัน สิ่งรบกวนจากบริเวณที่อยู่อาศัยหรือชุมชนโดยรอบอย่างมีประสิทธิภาพ เช่น ควันพิษ ฝุ่นละออง เสียง และป้องกันสิ่งรบกวนจากการเลี้ยงจิ้งหรีดต่อที่อยู่อาศัยหรือชุมชน เช่นกลิ่น เสียง ฝุ่นละออง เป็นต้น",
            isGap10: isGap10,
            worker10: _.get(worker10, "value", null),
            text10: text10,
            date10: date10,
            time10: time10,
            note10: note10,
            },
            {
            SubTopic11:"1. โรงเพาะเลี้ยงทำด้วยวัสดุแข็งแรงคงทน ไม่ดูดซับน้ำ และง่ายต่อการทำความสะอาดง่ายและบำรุงรักษา",
            isGap11: isGap11,
            worker11: _.get(worker11, "value", null),
            text11: text11,
            date11: date11,
            time11: time11,
            note11: note11,
            },
            {
            SubTopic12:"2. พื้นโรงเพาะเลี้ยงต้องไม่ลื่นในระหว่างปฏิบัติงาน ไม่มีน้ำขัง",
            isGap12: isGap12,
            worker12: _.get(worker12, "value", null),
            text12: text12,
            date12: date12,
            time12: time12,
            note12: note12,
            },
            {
            SubTopic13:"3. หน้าต่าง(ถ้ามี) ลาดเอียงลงอย่างเหมาะสม ผนังส่วนที่เป็นกระจกแก้วมีการป้องกันการแตกกระจายของเศษแก้ว",
            isGap13: isGap13,
            worker13: _.get(worker13, "value", null),
            text13: text13,
            date13: date13,
            time13: time13,
            note13: note13,
            },
            {
            SubTopic14:"4. โรงเพาะเลี้ยงสามารถกันแดด กันฝน มีระบบการกรองอากาศและระบบควบคุมการหมุนเวียนอากาศที่มีประสิทธิภาพ สามารถควบคุมการระบายอากาศ กลิ่น อุณหภูมิและความชื้น",    
            isGap14: isGap14,
            worker14: _.get(worker14, "value", null),
            text14: text14,
            date14: date14,
            time14: time14,
            note14: note14,
            },
            {
            SubTopic15:"5. ห้อง ประตู หน้าต่างของโรงเพาะเลี้ยงสามารถป้องกันการเข้าสู่ไม่ให้ศัตรูและสัตว์พาหะเข้าสู่โรงเพาะเลี้ยงได้",
            isGap15: isGap15,
            worker15: _.get(worker15, "value", null),
            text15: text15,
            date15: date15,
            time15: time15,
            note15: note15,
            },
            {
            SubTopic16:"1. มีวิธีการบำบัด การปรับปรุงคุณภาพน้ำเสียหรือกากของเสีย ไม่ก่อให้เกิดแหล่งสะสมของสัตว์พาหะ",
            isGap16: isGap16,
            worker16: _.get(worker16, "value", null),
            text16: text16,
            date16: date16,
            time16: time16,
            note16: note16,
            },
            {
            SubTopic17:"1.1 ติดตั้งอุปกรณ์สำหรับการบำบัดน้ำเสียจากฟาร์ม",
            isGap17: isGap17,
            worker17: _.get(worker17, "value", null),
            text17: text17,
            date17: date17,
            time17: time17,
            note17: note17,
            },
            { 
            SubTopic18:"1.1.1 ตะแกรงดักจับสิ่งแปลกปลอมเช่น ขุยมะพร้าว เศษแผงไข่",
            isGap18: isGap18,
            worker18: _.get(worker18, "value", null),
            text18: text18,
            date18: date18,
            time18: time18,
            note18: note18,
            },
            {
            SubTopic19:"1.1.2. ถังดักไขมัน ก่อนปล่อยสู่ท่อระบายน้ำ",
            isGap19: isGap19,
            worker19: _.get(worker19, "value", null),
            text19: text19,
            date19: date19,
            time19: time19,
            note19: note19,
            },
            {
            SubTopic20:"2. จัดให้มีที่ทิ้งหรือรองรับขยะมูลฝอยและสิ่งเปรอะเปื้อนที่เพียงพอและถูกสุขลักษณะ ไม่เป็นแหล่งอาศัยของสัตว์พาหะและใส่ในภาชนะที่มีฝาปิดสนิท",
            isGap20: isGap20,
            worker20: _.get(worker20, "value", null),
            text20: text20,
            date20: date20,
            time20: time20,
            note20: note20,
            },
            {
            SubTopic21:"3. มีวิธีการกำจัดขยะมูลฝอยหรือของเสียด้วยวิธีการที่เหมาะสมและถูกสุขลักษณะ โดยมีการแยกขยะติดเชื้อและขยะไม่ติดเชื้อ",
            isGap21: isGap21,
            worker21: _.get(worker21, "value", null),
            text21: text21,
            date21: date21,
            time21: time21,
            note21: note21,
            },
            {
            SubTopic22:"1. ความเข้มแสงในบริเวณทั่วไปมีอย่างน้อย 220 ลักซ์ (หลอดไฟบ้านทั่วไป)",
            isGap22: isGap22,
            worker22: _.get(worker22, "value", null),
            text22: text22,
            date22: date22,
            time22: time22,
            note22: note22,
            },
            {
            SubTopic23:"2. มีฝาครอบหลอดไฟในโรงเพาะเลี้ยงและมีการรักษาของฝาครอบอย่างสม่ำเสมอ",
            isGap23: isGap23,
            worker23: _.get(worker23, "value", null),
            text23: text23,
            date23: date23,
            time23: time23,
            note23: note23,
            }
            
        ]

        console.log('submittedData', submittedData);

        axios({
            method: 'post',
            url: 'https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.comaddcl02',
            data: submittedData
          }).then(
              res => {
                  if (res.status === 200){
                      this.setState({statusResponse: 200})
                      console.log(this.state.statusResponse)
                  }
                  else {
                      this.setState({statusResponse: "error"}) 
                      console.log(this.state.statusResponse)
                      return
                  }
              }
        )
        console.log(this.state.statusResponse)
    }

    handleTopics = (topic, number) => {
        return (
            <div>
                <Row className="ml-1 mr-1">
                    <p className="ml-3 mt-4" style={{color: "gray", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>{topic}</p>
                </Row>
                <div style={{textAlign: "-webkit-center"}}>
                    <Row className="mt-2 mb-2 w-100" style={{height: "40px"}}>
                        <div className="col" 
                            onClick={() => this.setState({[`isGap${number}`]: true})}
                            style={{backgroundColor: this.state[`isGap${number}`] === true ? "#08d1c0" : "white", 
                            padding: '0px 0px 0px 0px', 
                            display: 'relative', 
                            border:'solid 1px lightgray',
                            borderTopLeftRadius: '8px', 
                            borderBottomLeftRadius: '8px'}} >
                                <p style={{color: this.state[`isGap${number}`] === true ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '13px', position: 'relative', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>สอดคล้องกับ GAP</p>
                        </div>
                        <div className="col" 
                            onClick={() => this.setState({[`isGap${number}`]: false})}
                            style={{backgroundColor: this.state[`isGap${number}`] === false ? "#08d1c0" : "white", 
                            padding: '0px 0px 0px 0px', display: 'relative', 
                            border:'solid 1px lightgray', borderLeft: 'none', 
                            borderTopRightRadius: '8px', 
                            borderBottomRightRadius: '8px'}}>
                            <p style={{color: this.state[`isGap${number}`] === false ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '13px', position: 'relative', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่สอดคล้องกับ GAP</p>
                        </div>
                    </Row>
                </div>
                <Collapse isOpen={this.state[`isGap${number}`] === false ? true : false}>
                    <Row>
                        <p className="ml-4 mt-0 mb-2" style={{color: "gray", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>ผู้รับผิดชอบ</p>
                    </Row>
                    <Col lg="5" md="6" sm="3">
                        <Select
                        isSearchable={ false }
                        className="react-select primary mb-3 bg-white"
                        classNamePrefix="react-select"
                        name="worker"
                        onChange={(value, e) =>
                            this.setState({[`worker${number}`]: value})
                        }
                        options={this.state.workerOptions}
                        placeholder="กรุณาเลือกพนักงาน"
                        />
                    </Col>
                    <Row>
                        <p className="ml-4 mt-0" style={{color: "gray", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>การดำเนินการแก้ไข</p>
                    </Row>
                    <Col className="mt-2">
                        <FormGroup className="mt-3">
                            <Input placeholder="กรอกวิธีการดำเนินแก้ไข" type="textarea"
                            onChange={e => {this.setState({[`text${number}`]: e.target.value})}}
                            />
                        </FormGroup>
                    </Col>
                    <Row>
                        <p className="ml-4 mt-0" style={{color: "gray", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>วันที่กำหนดแล้วเสร็จ</p>
                    </Row>
                    <Row className="w-100 ml-0 mt-2" >
                        <Col className="col-12">
                            <FormGroup className="">
                                <ReactDatetime
                                dateFormat="DD-MM-YYYY"
                                // open={dateToggle}
                                // onBlur={()=> this.setState({dateToggle: false})}
                                inputProps={{
                                    className: "form-control date-assign",
                                    placeholder: "กรุณาใส่วันที่",
                                    backgroundColor: 'white',
                                    readOnly: true,
                                    // onClick: () => this.setState({dateToggle: true}),
                                }}
                                timeFormat={false}
                                onChange={value =>
                                    (this.setState({ [`date${number}`]: moment(value.toDate()).format('MM-DD-YYYY'), dateToggle: false}))
                                }
                                />
                        </FormGroup>
                        </Col>
                        <Row>
                            <p className="ml-4 mt-0" style={{color: "gray", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>เวลากำหนดแล้วเสร็จ</p>
                        </Row>
                        <Col className="col-12 mt-2">
                            <FormGroup className="">
                                <ReactDatetime
                                // onBlur={()=> this.setState({blurStart: true})}
                                // onFocus={()=> this.setState({blurStart: false})}
                                inputProps={{
                                    className: "form-control bg-white",
                                    key: `${number}`,
                                    placeholder: "กรุณาใส่เวลา",
                                    readOnly: true,
                                }}
                                timeFormat={"HH:mm"}
                                dateFormat={false}
                                onChange={value =>
                                    this.handleTimeChange(value.toDate(), `${number}`)
                                }
                                />
                            </FormGroup>    
                        </Col>    
                    </Row>
                    <Row>
                        <p className="ml-4 mt-0" style={{color: "gray", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>หมายเหตุ</p>
                    </Row>
                    <Col className="mt-2" style={{borderBottom: "1px solid #d3d3d3"}}>
                        <FormGroup className="mt-3">
                            <Input placeholder="ระบุหมายเหตุ" type="textarea"
                            onChange={e => {this.setState({[`note${number}`]: e.target.value})}}
                            />
                        </FormGroup>
                    </Col>
                </Collapse>
            </div>
        )
    }

    jobAssignment = () => { 
        // - you can now access this.props.history for navigation
        this.props.history.push({pathname: "/admin/calendar", state: this.state});
      };

      handleTimeChange = (startTime, no) => {
        const hour = startTime.getHours()
        const minuteLength = String(startTime.getMinutes()).length
        const minute = (minuteLength === 1) ? `0` + startTime.getMinutes() : startTime.getMinutes()
        const combined = hour + ":" + minute

        if (no === "1") {
            this.setState({time1: combined})
        } 
        else if (no === "2") {
            this.setState({time2: combined})
        }
        else if (no === "3") {
            this.setState({time3: combined})
        }
        else if (no === "4") {
            this.setState({time4: combined})
        }
        else if (no === "5") {
            this.setState({time5: combined})
        }
        else if (no === "6") {
            this.setState({time6: combined})
        }
        else if (no === "7") {
            this.setState({time7: combined})
        }
        else if (no === "8") {
            this.setState({time8: combined})
        }
        else if (no === "9") {
            this.setState({time9: combined})
        }
        else if (no === "10") {
            this.setState({time10: combined})
        }
        else if (no === "11") {
            this.setState({time11: combined})
        }
        else if (no === "12") {
            this.setState({time12: combined})
        }
        else if (no === "13") {
            this.setState({time13: combined})
        }
        else if (no === "14") {
            this.setState({time14: combined})
        }
        else if (no === "15") {
            this.setState({time15: combined})
        }
        else if (no === "16") {
            this.setState({time16: combined})
        }
        else if (no === "17") {
            this.setState({time17: combined})
        }
        else if (no === "18") {
            this.setState({time18: combined})
        }
        else if (no === "19") {
            this.setState({time19: combined})
        }
        else if (no === "20") {
            this.setState({time20: combined})
        }
        else if (no === "21") {
            this.setState({time21: combined})
        }
        else if (no === "22") {
            this.setState({time22: combined})
        }
        else if (no === "23") {
            this.setState({time23: combined})
        }
        
      }

      handleExportAb = async () => {
          console.log(this.state.jobDate)
          console.log(this.state.jobDate2)

          axios.get(`${URL}/ablog`, {
            params: {
              farmId: Cookies.get('farmId'),
              DateStart: this.state.jobDate,
              DateEnd: this.state.jobDate2
            }
          }).then(res => {
             
            this.setState({
                abnormalData:res.data
             })
             console.log(this.state.abnormalData)
            })

         await this.setState({abnormalData: null})
      }

      handleExportHarvest = async () => {
        console.log(this.state.jobDate)
        console.log(this.state.jobDate2)

        axios.get(`${URL}/havestlog`, {
            params: {
              farmId: Cookies.get('farmId'),
              DateStart: this.state.jobDate,
              DateEnd: this.state.jobDate2
            }
          }).then(res => {
             
            this.setState({
                havestData:res.data
             })

            })
        await this.setState({havestData: null})
      }
      handleExportAssigned = async () => {
        console.log(this.state.jobDate)
        console.log(this.state.jobDate2)

        axios.get(`${URL}/assignedlog`, {
            params: {
              farmId: Cookies.get('farmId'),
              DateStart: this.state.jobDate,
              DateEnd: this.state.jobDate2
            }
        }).then(res => {
            
            this.setState({
                assignedData:res.data
            })
            console.log(this.state.assignedData)
            })

        await this.setState({assignedData: null})
      }

      handleExportFW = async () => {
        console.log(this.state.jobDate)
        console.log(this.state.jobDate2)

        axios.get(`${URL}/fwlog`, {
            params: {
              farmId: Cookies.get('farmId'),
              DateStart: this.state.jobDate,
              DateEnd: this.state.jobDate2
            }
        }).then(res => {
            
            this.setState({
                FWData:res.data
            })
            console.log(this.state.FWData)
            })

        await this.setState({FWData: null})
      }

    render() {
        const { abnormalData, havestData,assignedData,FWData, dateToggle,dateToggle2} = this.state
      return (
        <>
           <div className="content">
            <button
                className="btn btn-primary d-flex"
                onClick={this.jobAssignment}
                >
                <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
                <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>กลับหน้าปฏิทินมอบหมายงาน</p>
            </button>


            <Row>
                
            <Row className="w-100 ml-0 mt-2">
                <Col className="col-12 mt-2">
                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>วันที่เริ่มต้น</small>
                    <FormGroup className="mt-3">
                        <ReactDatetime
                        dateFormat="DD-MM-YYYY"
                        open={dateToggle}
                        onBlur={()=> this.setState({dateToggle: false})}
                        inputProps={{
                            className: "form-control date-assign",
                            placeholder: "กรุณาใส่วันที่เริ่มต้น",
                            backgroundColor: 'white',
                            style:{fontFamily: 'IBM Plex Sans Thai'},  
                            readOnly: true,
                            onClick: () => this.setState({dateToggle: true}),
                        }}
                        timeFormat={false}
                        onChange={value =>
                            (this.setState({ jobDate: moment(value.toDate()).format('DD/MM/YYYY'), dateToggle: false}))
                        }
                        />
                  </FormGroup>
                </Col>
            </Row>
            <Row className="w-100 ml-0 mt-2">
                <Col className="col-12 mt-2">
                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>วันที่สิ้นสุด</small>
                    <FormGroup className="mt-3">
                        <ReactDatetime
                        dateFormat="DD-MM-YYYY"
                        open={dateToggle2}
                        onBlur={()=> this.setState({dateToggle2: false})}
                        inputProps={{
                            className: "form-control date-assign",
                            placeholder: "กรุณาใส่วันที่สิ้นสุด",
                            backgroundColor: 'white',
                            style:{fontFamily: 'IBM Plex Sans Thai'},  
                            readOnly: true,
                            onClick: () => this.setState({dateToggle2: true}),
                        }}
                        timeFormat={false}
                        onChange={value =>
                            (this.setState({ jobDate2: moment(value.toDate()).format('DD/MM/YYYY'), dateToggle2: false}))
                        }
                        />
                  </FormGroup>
                </Col>
            </Row>
        
        </Row> 

        <Row>
          <div className="row mt-3 ml-4" style={{position: "relative"}}>
            <p className='mt-2' style={{width: '100%', textAlign: 'left', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '20px'}}>1. เอกสาร สรุปสิ่งผิดปกติ</p>
            <button
                className="btn btn-success"
                onClick={this.handleExportAb}
                >
                <p style={{fontColor: "white", fontSize:"16px", fontFamily: 'IBM Plex Sans Thai'}}>Export</p>
            </button>
          </div>
        </Row>
        <Row>
          <div className="row mt-3 ml-4" style={{position: "relative"}}>
            <p className='mt-2' style={{width: '100%', textAlign: 'left', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '20px'}}>2. เอกสาร Harvest</p>
          <button
                className="btn btn-success"
                onClick={this.handleExportHarvest}
                >
                <p style={{fontColor: "white", fontSize:"16px", fontFamily: 'IBM Plex Sans Thai'}}>Export</p>
          </button>
          </div>
        </Row>
        <Row>
          <div className="row mt-3 ml-4" style={{position: "relative"}}>
            <p className='mt-2' style={{width: '100%', textAlign: 'left', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '20px'}}>3. เอกสาร บันทึกรายงาน</p>
          <button
                className="btn btn-success"
                onClick={this.handleExportAssigned}
                >
                <p style={{fontColor: "white", fontSize:"16px", fontFamily: 'IBM Plex Sans Thai'}}>Export</p>
          </button>
          </div>
        </Row>
        <Row>
          <div className="row mt-3 ml-4" style={{position: "relative"}}>
            <p className='mt-2' style={{width: '100%', textAlign: 'left', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '20px'}}>4. เอกสาร บันทึกการให้น้ำและอาหาร</p>
          <button
                className="btn btn-success"
                onClick={this.handleExportFW}
                >
                <p style={{fontColor: "white", fontSize:"16px", fontFamily: 'IBM Plex Sans Thai'}}>Export</p>
          </button>
          </div>
        </Row>
       
            <AdminNavBar pageName="Export เอกสาร"/>
            <AdminFooter active={true}/>
            {abnormalData != null ? 
                <ExcelFile filename="สิ่งผิดปกติ" hideElement={true}>
                <ExcelSheet data={abnormalData} name="Employees">
                    <ExcelColumn label="วันที่แจ้ง" value="ablog_time"/>
                    <ExcelColumn label="สถานที่" value="ablog_place1"/>
                    <ExcelColumn label="สถานที่" value="ablog_place2"/>
                    <ExcelColumn label="สิ่งผิดปกติ" value="ablog_cause"/>
                    <ExcelColumn label="ผู้รับผิดชอบ" value="ablog_worker"/>
            
                    {/* <ExcelColumn label="Marital Status" value={(col) => col.is_married ? "Married" : "Single"}/> */}
                </ExcelSheet>
                </ExcelFile>
                    :
                    null
            }
            {havestData != null ? 
                <ExcelFile filename="บันทึกการเก็บเกี่ยว" hideElement={true}>
                <ExcelSheet data={havestData} name="Employees">
                    <ExcelColumn label="วันที่" value="havestlog_date"/>
                    <ExcelColumn label="สถานที่" value="havestlog_place1"/>
                    <ExcelColumn label="สถานที่" value="havestlog_place2"/>
                    <ExcelColumn label="น้ำหนัก" value="havestlog_weight"/>
                    <ExcelColumn label="อาหารสะสม" value="havestlog_food"/>
                    <ExcelColumn label="น้ำสะสม" value="havestlog_water"/>
                    <ExcelColumn label="FCR" value="havestlog_fcr"/>
                    <ExcelColumn label="ผู้รับผิดชอบ" value="havestlog_worker"/>

                    {/* <ExcelColumn label="Marital Status" value={(col) => col.is_married ? "Married" : "Single"}/> */}
                </ExcelSheet>
                </ExcelFile>
                    :
                    null
            }
            {assignedData != null ? 
                <ExcelFile filename="บันทึกรายงาน" hideElement={true}>
                <ExcelSheet data={assignedData} name="Employees">
                    <ExcelColumn label="วันที่" value="assignedlog_time"/>
                    <ExcelColumn label="รายการทำงาน" value="assignedlog_name"/>
                    <ExcelColumn label="สถานที่" value="assignedlog_place1"/>
                    <ExcelColumn label="สถานที่" value="assignedlog_place2"/>
                    <ExcelColumn label="ผู้รับผิดชอบ" value="assignedlog_worker"/>

                    {/* <ExcelColumn label="Marital Status" value={(col) => col.is_married ? "Married" : "Single"}/> */}
                </ExcelSheet>
                </ExcelFile>
                    :
                    null
            }
             {FWData != null ? 
                <ExcelFile filename="บันทึกการให้น้ำและอาหาร" hideElement={true}>
                <ExcelSheet data={FWData} name="Employees">
                    <ExcelColumn label="วันที่" value="fwlog_date"/>
                    <ExcelColumn label="ชื่องาน" value="fwlog_name"/>
                    <ExcelColumn label="สถานที่" value="fwlog_place1"/>
                    <ExcelColumn label="สถานที่" value="fwlog_place2"/>
                    <ExcelColumn label="จำนวนน้ำ,อาหารที่ให้" value="fwlog_val"/>

                    {/* <ExcelColumn label="Marital Status" value={(col) => col.is_married ? "Married" : "Single"}/> */}
                </ExcelSheet>
                </ExcelFile>
                    :
                    null
            }
          </div>

          {/* <div className="content">
            <button
                className="btn btn-primary d-flex"
                onClick={this.jobAssignment}
                >
                <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
                <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>กลับหน้าฟอร์ม</p>
            </button>
              <Card className="card-stats mt-4">
                <div className="row" style={{height: "48px"}} onClick={() => this.setState({isOpen1: !this.state.isOpen1})}>
                    <div className="col-10" style={{borderRight: "1px solid #d3d3d3"}}>
                        <p className="ml-3 mt-3" style={{color: "black", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>1.1 สถานที่ตั้งและบริเวณโดยรอบ</p>
                    </div>
                    <div className="col-2" style={{ display: "flex", alignItems: "center" }}>
                        <i
                            style={{
                            fontWeight: "bold",
                            fontSize: "14px",
                            color: "black",
                            }}
                            className={`nc-icon nc-minimal-down`}
                        />
                    </div>
                </div>
                <Collapse isOpen={this.state.isOpen1} style={{borderTop: "1px solid #d3d3d3"}}>
                    <CardBody>
                        {this.handleTopics("1. ฟาร์มตั้งอยู่ในแหล่งที่ไม่มีน้ำท่วมขัง หรือมีการป้องกัน", 1)}
                        {this.handleTopics("2. ตั้งอยูในพื้นที่ที่ไม่ส่งผลกระทบต่อชุมชนโดยรอบ เว้นแต่มีมาตรการป้องกันผลกระทบต่อชุมชน", 2)}
                        {this.handleTopics("3. ตั้งอยู่ในสภาพแวดล้อมที่ไม่เสี่ยงต่อหารปนเปื้อนจากอันตรายทางกายภาพ เคมีและชีวภาพ", 3)}
                        {this.handleTopics("4. สิ่งแวดล้อมในฟาร์มเช่น อุณหภูมิ ความชื้น ฝุ่น ละออง กลิ่น อยู่ในเกณฑ์ที่เหมาะสม ไม่ก่อให้เกิดอันตรายต่อจิ้งหรีดและผู้ปฎิบัติงาน", 4)}
                        {this.handleTopics("5. การคมนาคมขนส่งเข้า-ออก ฟาร์ม สะดวกต่อการรับ-ส่ง วัสดุ อุปกรณ์ อาหาร และผลิตผล", 5)}
                    </CardBody>
                </Collapse>
            </Card>
            <Card className="card-stats mt-4">
                <div className="row" style={{height: "48px"}} onClick={() => this.setState({isOpen2: !this.state.isOpen2})}>
                    <div className="col-10" style={{borderRight: "1px solid #d3d3d3"}}>
                        <p className="ml-3 mt-3" style={{color: "black", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>1.2 ผังและลักษณะฟาร์ม</p>
                    </div>
                    <div className="col-2" style={{ display: "flex", alignItems: "center" }}>
                        <i
                            style={{
                            fontWeight: "bold",
                            fontSize: "14px",
                            color: "black",
                            }}
                            className={`nc-icon nc-minimal-down`}
                        />
                    </div>
                </div>
                <Collapse isOpen={this.state.isOpen2} style={{borderTop: "1px solid #d3d3d3"}}>
                    <CardBody>
                        {this.handleTopics("1. พื้นที่ฟาร์มมีฃนาดแหมาะสม กล่องเลี้ยงมีขนาดและจำนวนเหมาะสม แต่ละบ่อควรมีระยะห่างจากกันเพื่อป้องกันการปนเปื้อน การติดเชื้อข้าม และเพื่อความสะดวกในการปฎิบัติงาน หรือมีวิธีการปฎิบัติงานในแต่ละบ่อ", 6)}
                        {this.handleTopics("2. ออกแบบพื้นที่การเพาะเลี้ยงเป็นระบบปิด เพื่อป้องกันการหลุดรอดของจิ้งหรีดออกภายนอกกล่องเลี้ยงหรือโรงเพาะเลี้ยง และป้องกันจิ้งหรีดจากธรรมชาติปะปน ป้องกันสัตว์เลี้ยง สัตว์พาหะ ศัตรูจิ้งหรีด รวมทั้งการเข้า-ออกของผู้ไม่เกี่ยวข้อง", 7)}
                        {this.handleTopics("3. ติดตั้งระบบควบคุมอุณหภูมิ ความชื้น และการหมุนเวียนอากาศและตั้งค่าที่เหมาะสมกับกับการเจริญเติบโตของจิ้งหรีด", 8)}
                        {this.handleTopics("4. มีการวางผังฟาร์มที่ดี จัดแยกส่วนพื้นที่เป็นสัดส่วนอย่างชัดเจน เหมาะสมตามวัตถุประสงค์ เช่นบริเวณเลี้ยงจิ้งหรีด เก็บอาหาร เก็บน้ำ เก็บอุปกรณ์เวชภัณฑ์และสารเคมี บริเวณชำระล้างวัสดุอุปกรณ์ บริเวณรวบรวมจิ้งหรีดหลังการเก็บเกี่ยว รวบรวมมูลจิ้งหรีด ขยะและสิ่งปฎิกูล", 9)}
                        {this.handleTopics("5. พื้นที่เลี้ยงแยกส่วนชัดเจนกับที่อยู่อาศัย มีการป้องกัน สิ่งรบกวนจากบริเวณที่อยู่อาศัยหรือชุมชนโดยรอบอย่างมีประสิทธิภาพ เช่น ควันพิษ ฝุ่นละออง เสียง และป้องกันสิ่งรบกวนจากการเลี้ยงจิ้งหรีดต่อที่อยู่อาศัยหรือชุมชน เช่นกลิ่น เสียง ฝุ่นละออง เป็นต้น", 10)}
                    </CardBody>
                </Collapse>
            </Card>
            <Card className="card-stats mt-4">
                <div className="row" style={{height: "48px"}} onClick={() => this.setState({isOpen3: !this.state.isOpen3})}>
                    <div className="col-10" style={{borderRight: "1px solid #d3d3d3"}}>
                        <p className="ml-3 mt-3" style={{color: "black", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>1.3 โรงเพาะเลี้ยง</p>
                    </div>
                    <div className="col-2" style={{ display: "flex", alignItems: "center" }}>
                        <i
                            style={{
                            fontWeight: "bold",
                            fontSize: "14px",
                            color: "black",
                            }}
                            className={`nc-icon nc-minimal-down`}
                        />
                    </div>
                </div>
                <Collapse isOpen={this.state.isOpen3} style={{borderTop: "1px solid #d3d3d3"}}>
                    <CardBody>
                        {this.handleTopics("1. โรงเพาะเลี้ยงทำด้วยวัสดุแข็งแรงคงทน ไม่ดูดซับน้ำ และง่ายต่อการทำความสะอาดง่ายและบำรุงรักษา ", 11)}
                        {this.handleTopics("2. พื้นโรงเพาะเลี้ยงต้องไม่ลื่นในระหว่างปฏิบัติงาน ไม่มีน้ำขัง ", 12)}
                        {this.handleTopics("3. หน้าต่าง(ถ้ามี) ลาดเอียงลงอย่างเหมาะสม ผนังส่วนที่เป็นกระจกแก้วมีการป้องกันการแตกกระจายของเศษแก้ว", 13)}
                        {this.handleTopics("4. โรงเพาะเลี้ยงสามารถกันแดด กันฝน มีระบบการกรองอากาศและระบบควบคุมการหมุนเวียนอากาศที่มีประสิทธิภาพ สามารถควบคุมการระบายอากาศ กลิ่น อุณหภูมิและความชื้น", 14)}
                        {this.handleTopics("5. ห้อง ประตู หน้าต่างของโรงเพาะเลี้ยงสามารถป้องกันการเข้าสู่ไม่ให้ศัตรูและสัตว์พาหะเข้าสู่โรงเพาะเลี้ยงได้ ", 15)}
                    </CardBody>
                </Collapse>
            </Card>
            <Card className="card-stats mt-4">
                <div className="row" style={{height: "48px"}} onClick={() => this.setState({isOpen4: !this.state.isOpen4})}>
                    <div className="col-10" style={{borderRight: "1px solid #d3d3d3"}}>
                        <p className="ml-3 mt-3" style={{color: "black", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>1.3.1 การกำจัดน้ำเสีย มูลฝอย</p>
                    </div>
                    <div className="col-2" style={{ display: "flex", alignItems: "center" }}>
                        <i
                            style={{
                            fontWeight: "bold",
                            fontSize: "14px",
                            color: "black",
                            }}
                            className={`nc-icon nc-minimal-down`}
                        />
                    </div>
                </div>
                <Collapse isOpen={this.state.isOpen4} style={{borderTop: "1px solid #d3d3d3"}}>
                    <CardBody>
                        {this.handleTopics("1. มีวิธีการบำบัด การปรับปรุงคุณภาพน้ำเสียหรือกากของเสีย ไม่ก่อให้เกิดแหล่งสะสมของสัตว์พาหะ", 16)}
                        {this.handleTopics("1.1 ติดตั้งอุปกรณ์สำหรับการบำบัดน้ำเสียจากฟาร์ม", 17)}
                        {this.handleTopics("1.1.1 ตะแกรงดักจับสิ่งแปลกปลอมเช่น ขุยมะพร้าว เศษแผงไข่", 18)}
                        {this.handleTopics("1.1.2. ถังดักไขมัน ก่อนปล่อยสู่ท่อระบายน้ำ", 19)}
                        {this.handleTopics("2. จัดให้มีที่ทิ้งหรือรองรับขยะมูลฝอยและสิ่งเปรอะเปื้อนที่เพรยงพอและถูกสุขลักษณะ ไม่เป็นแหล่งอาศัยของสัตว์พาหะและใส่ในภาชนะที่มีฝาปิดสนิท", 20)}
                        {this.handleTopics("3. มีวิธีการกำจัดขยะมูลฝอยหรือของเสียด้วยวิธีการที่เหมาะสมและถูกสุขลักษณะ โดยมีการแยกขยะติดเชื้อและขยะไม่ติดเชื้อ", 21)}
                    </CardBody>
                </Collapse>
            </Card>
            <Card className="card-stats mt-4">
                <div className="row" style={{height: "48px"}} onClick={() => this.setState({isOpen5: !this.state.isOpen5})}>
                    <div className="col-10" style={{borderRight: "1px solid #d3d3d3"}}>
                        <p className="ml-3 mt-3" style={{color: "black", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', position: "relative"}}>1.4 แสงสว่าง</p>
                    </div>
                    <div className="col-2" style={{ display: "flex", alignItems: "center" }}>
                        <i
                            style={{
                            fontWeight: "bold",
                            fontSize: "14px",
                            color: "black",
                            }}
                            className={`nc-icon nc-minimal-down`}
                        />
                    </div>
                </div>
                <Collapse isOpen={this.state.isOpen5} style={{borderTop: "1px solid #d3d3d3"}}>
                    <CardBody>
                        {this.handleTopics("1. ความเข้มแสงในบริเวณทั่วไปมีอย่างน้อย 220 ลักซ์ (หลอดไฟบ้านทั่วไป)", 22)}
                        {this.handleTopics("2. มีฝาครอบหลอดไฟในโรงเพาะเลี้ยงและมีการรักษาของฝาครอบอย่างสม่ำเสมอ", 23)}
                    </CardBody>
                </Collapse>
            </Card>
            <Row className='mb-3' style={{marginTop: '50px'}}>
                <Col md="12" xs="12" onClick={() => this.onSubmit()}>
                    <div className='d-flex justify-content-center' style={{borderColor: '#0dc5ca', borderRadius: '8px', height: '40px', border:'1px solid #0dc5ca', color:"#0dc5ca"}}>
                        <i style={{marginTop: '12px'}} className="far fa-save mr-2 fa-lg"></i>
                        <p className="text-center" style={{ fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', lineHeight: '40px'}}>บันทึก</p>
                    </div>
                </Col>
            </Row>
            <AdminNavBar pageName="1. โครงสร้างและการออกแบบ"/>
            <AdminFooter active={true}/>
          </div> */}
        </>
      );
    }
  }
  
  export default withRouter(Export);
