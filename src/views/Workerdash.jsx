import AdminNavBar from "../components/Navbars/AdminNavbar"
import ache from "../assets/img/ache.png"
import axios from 'axios';
import React from "react";
import { Line, Bar, Doughnut } from "react-chartjs-2";
import { VectorMap } from "react-jvectormap";
import {  Pie } from "react-chartjs-2";
import WorkerFooter from '../components/WorkerNavBarFooter/WorkerFooter'
import WorkerinsectariumCard from "./Workerinsectariumcard"
import { withRouter } from "react-router-dom";
import Cookies from 'js-cookie'
import {
  Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Row,
  Col,
  Collapse
} from "reactstrap";
import Steps, {Step} from "rc-steps"
import { CircularProgressbar, buildStyles, CircularProgressbarWithChildren } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

const Chart = require("chart.js");
Chart.pluginService.register({
  beforeDraw: function(chart) {
    if (chart.config.options.elements.center) {
      //Get ctx from string
      var ctx = chart.chart.ctx;

      //Get options from the center object in options
      var centerConfig = chart.config.options.elements.center;
      var fontStyle = centerConfig.fontStyle || "Arial";
      var txt = centerConfig.text;
      var color = centerConfig.color || "#000";
      var sidePadding = centerConfig.sidePadding || 20;
      var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2);
      //Start with a base font of 30px
      ctx.font = "30px " + fontStyle;

      //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
      var stringWidth = ctx.measureText(txt).width;
      var elementWidth = chart.innerRadius * 2 - sidePaddingCalculated;

      // Find out how much the font can grow in width.
      var widthRatio = elementWidth / stringWidth;
      var newFontSize = Math.floor(30 * widthRatio);
      var elementHeight = chart.innerRadius * 2;

      // Pick a new font size so it will not be larger than the height of label.
      var fontSizeToUse = Math.min(newFontSize, elementHeight);

      //Set font settings to draw it correctly.
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";
      var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
      var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
      ctx.font = fontSizeToUse + "px " + fontStyle;
      ctx.fillStyle = color;

      //Draw text in center
      ctx.fillText(txt, centerX, centerY);
    }
  }
});

// default color for the charts
let chartColor = "#FFFFFF";

// ##############################
// // // Function that converts a hex color number to a RGB color number
// #############################
const hexToRGB = (hex, alpha) => {
  var r = parseInt(hex.slice(1, 3), 16),
    g = parseInt(hex.slice(3, 5), 16),
    b = parseInt(hex.slice(5, 7), 16);

  if (alpha) {
    return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
  } else {
    return "rgb(" + r + ", " + g + ", " + b + ")";
  }
};
const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
const valuesensor = [
  {number:1, temp:25, hum:25},
  {number:2, temp:30, hum:12}
]
const handleGraphColorTemp = (percentage) => {
  if (percentage == 32) {
    return "#25f19b";
  } else if (percentage > 32 && percentage < 35) {
    return "#FF9900";
  } else if (percentage < 32 && percentage > 29) {
    return "#FF9900";
  } else if (percentage >= 35 || percentage <= 29) {
    return "#FF0000";
  }
};

const handleGraphColorAmm = (percentage) => {
  if (percentage < 10) {
    return "#25f19b";
  } else if (percentage == 10) {
    return "#FF9900";
  } else if (percentage > 10) {
    return "#FF0000";
  }
};

const handleGraphColorHum = (percentage) => {
  if (percentage <= 50) {
    return "#25f19b";
  } else if (percentage > 50 && percentage <= 60) {
    return "#FF9900";
  } else if (percentage > 60) {
    return "#FF0000";
  }
};

class Workerdash extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data:[],
      ids:"",
      allpound:[],
      escapeInsectReport: "normal",
      isOpen: false,
      isOpen2: false,
      sensorvalue:[],
      farmStatus:"ปกติ",
      humidity:"0"
    }
    this.onClick1 = this.onClick1.bind(this);
    this.onClick2 = this.onClick2.bind(this);
  }
  componentDidMount() {
    // this.getAcceptedcount();
    var query = new URLSearchParams(this.props.location.search);
    var params = query.get('farm_id');
    this.setState({farmIdParam: params})
    console.log(params)
    this.getinsrctarium(params);
    this.getallpound(params);
    this.setgatesensor();
    this.getvaluesensor();
     this.interval = setInterval(this.getallpound, 1000);
   
     this.interval = setInterval(this.getvaluesensor, 5000);

}
componentWillUnmount() {
  // Clear the interval right before component unmount
  clearInterval(this.interval);
}

getvaluesensor = async () => {
  try {
  const getvaluesensor = await axios.get(`${URL}/api/sendDHTinsect/${Cookies.get('farmId')}/${this.state.data.insectarium_name}`)
      this.setState({ sensorvalue: getvaluesensor.data});
  
      console.log("sensor",this.state.sensorvalue)
    } catch (err) {
      console.error(err.message);
    }
  };

    

  setgatesensor = () =>{
      return   this.state.sensorvalue.map((item,index) => (

          <Row>
          <p className="text-center ml-4 mt-2" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>เซนเซอร์ชุดที่ {item.number}</p>

          
          <Row className="justify-content-center">
          <Col md="4" xs="4">
            
            <div style={{ width: "100%", margin: "0 auto" }}>
              <p
                className="text-center"
                style={{
                  fontColor: "#58585e",
                  fontSize: "14px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                }}
              >
                อุณหภูมิ
              </p>
              <CircularProgressbar
                value={item.temp}
                text={`${item.temp}°C`}
                styles={buildStyles({
                  pathColor: handleGraphColorTemp(item.temp)})} />
              
            </div>
          </Col>
        
          <Col md="4" xs="4">
            <div style={{ width: "100%", margin: "0 auto" }}>
              <p
                className="text-center"
                style={{
                  fontColor: "#58585e",
                  fontSize: "14px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                }}
              >
                ความชื้น
              </p>
              <CircularProgressbar
                value={item.hum}
                text={`${item.hum}%`}
                styles={buildStyles({
                  pathColor: handleGraphColorHum(item.hum)})} />
              
            </div>
          </Col>
      </Row>
      </Row>
              ))
    }

renderWorkers = (workers) => {
  if (!workers) { return }
  return workers.map((worker, index) => (
    worker.firstName + ", "
  ))
}

getinsrctarium = (params) => {
  const { ids } = this.state
  const idInsectarium = ids ? ids : params
  axios.get(`${URL}/insectarium/${idInsectarium}`)
      .then(res => {
      console.log(res.data)
      this.setState({ data: res.data[0] });
      console.log("param",params)
      if(params){
        console.log("pspspssp");
        this.setState({ ids: params });
      }
      console.log("eiei")
      console.log(this.state.data)

      })
      
  }

  

  getallpound = (params) => {
    const { ids } = this.state
    const idInsectarium = ids ? ids : params
    axios.get(`${URL}/allpoundv2/${Cookies.get('farmId')}/${this.state.data.insectarium_name}`)
        .then(res => {
        
        this.setState({ allpound: res.data });
  
        })
        
  }

  renderInsectariumCards = (allpound) => {
    return allpound.map((allpound, index) => (
      <WorkerinsectariumCard
      allpound={allpound}
      index={index}
      farmId={this.state.farmIdParam}
      />
    ))
  }

  onClick1 = () => {

      this.setState(prevState => ({
        data: {                   // object that we want to update
            ...prevState.data,    // keep all other key-value pairs
            'insectarium_status':"ปกติ"       // update the value of specific key
        }
    }))
    console.log("bf",this.state.ids);
    const status = {status:"ปกติ"} 
    console.log(status)
    axios.post(`${URL}/updateinsectariumstatus/${this.state.ids}`,status)
    .then(res => {
      console.log(res)})
      console.log("at",this.state.ids);

      
  }

  
  onClick2 = () => {

    this.setState(prevState => ({
      data: {                   // object that we want to update
          ...prevState.data,    // keep all other key-value pairs
          'insectarium_status':"ไม่ปกติ"       // update the value of specific key
      }
  }))
    
    const status = {status:"ไม่ปกติ"} 
    console.log(status)
    axios.post(`${URL}/updateinsectariumstatus/${this.state.ids}`,status)
    .then(res => {
      console.log(res)})
      
  }

  farm = () => { 
    // - you can now access this.props.history for navigation
    this.props.history.push({pathname: "/admin/workerhome", search: '?farm_id=' + this.state.farmIdParam, state: this.state});
  };


  render() {
 
    const { escapeInsectReport, isOpen,isOpen2, farmStatus } = this.state

    return (
      <>
        <div className="content">
          <button
            className="btn btn-primary d-flex"
            onClick={this.farm}
            >
              <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
              <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>กลับหน้าแดชบอร์ด</p>
          </button>
          {/* <div className="mb-3">
            <Steps current={2}>
              <Step icon={<i className={`nc-icon nc-check-2`} />}/>
              <Step icon={<i className={`nc-icon nc-check-2`} />}/>
              <Step/>
              <Step/>
              <Step/>
            </Steps>
          </div> */}
          <Row>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col xs="12">
                      <div className="border mb-3 mt-1 d-flex" style={{height: '50px', borderRadius: '12px', justifyContent: 'center', alignItems: 'center'}}>
                        <div className="w-100" style={{display:'contents'}}>
                          <p style={{fontColor:"#58585e", fontSize:"18px", fontFamily: 'IBM Plex Sans Thai',marginTop: '20px', fontWeight: 'bold'}}>สถานะโรงเลี้ยง {this.state.data.insectarium_name}</p>
                          <div className="border w-25 mb-4" style={{height: '30px',marginTop: '20px', borderRadius: '4px', backgroundColor: this.state.data.insectarium_status === "ปกติ" ? '#09c676' : '#e34849', color:'white', textAlign: 'center', marginLeft: '8px'}}>
                            <p style={{fontColor: "white", fontSize:"18px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px'}}> {this.state.data.insectarium_status} </p>
                          </div>
                        </div>
                      </div>
                        {/* <div>
                          <p className="w-100" style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai', top: '58%', textAlign: 'center', right: '0%', position: 'absolute'}}>ทำความสะอาดล่าสุดเมื่อ {FarmData.recentClean}</p>
                        </div> */}
                    </Col>
                  </Row>
                  
                  <Row onClick={() =>  this.setState({isOpen2: !isOpen2})}>
                    <Col xs="12">
                      <div
                        className="w-100"
                        style={{
                          display: "flex",
                          flexWrap: "wrap",
                          marginBottom: "-18px",
                          justifyContent: "space-between",
                        }}
                      >
                        <p
                          className="ml-3"
                          style={{
                            fontColor: "white",
                            fontSize: "16px",
                            fontFamily: "IBM Plex Sans Thai",
                            fontWeight: "bold",
                            
                          }}
                        >
                          ชุดเซนเซอร์ทั้งหมด
                        </p>
                        <div className="mt-1 ml-3" onClick={() => this.setState({isOpen2: !isOpen2})}>
                          <i
                            className="nc-icon nc-minimal-down"
                            style={{
                              color: "black",
                              fontWeight: "bold",
                              float: "right",
                            }}
                          />
                        </div>
                      </div>
                      <hr />
                    </Col>
                  </Row>
                  
                  <Collapse isOpen={this.state.isOpen2}>
                  
                    {this.setgatesensor(valuesensor)}
                    
                  </Collapse>
            
                  <Row>
                    <p className="text-center ml-4 mt-4" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>แจ้งจิ้งหรีดหลุด</p>
                  </Row>
                  <Row>
                    <div className="row mt-3" style={{position: "relative", bottom: "0px", width: '80%', zIndex: '100', height: '50px', boxShadow: "-2px 2px 11px -6px #333", borderRadius: '12px', left:"15%"}}>
                      <div className="col" style={{backgroundColor: this.state.data.insectarium_status === "ปกติ" ? "#09c676" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '12px', borderBottomLeftRadius: '12px'}} >
                        <p style={{color:  this.state.data.insectarium_status === "ปกติ" ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ปกติ</p>
                      </div>
                      <div className="col" style={{backgroundColor: this.state.data.insectarium_status === "ไม่ปกติ" ? "#e34849" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopRightRadius: '12px', borderBottomRightRadius: '12px'}} onClick={this.onClick2}>
                        <p style={{color: this.state.data.insectarium_status === "ไม่ปกติ" ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>จิ้งหรีดหลุด</p>
                      </div>
                    </div>
                  </Row>
                  {/* <Row>
                    <p className="text-center ml-4 mt-4" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>พนักงานประจำบ่อ</p>
                  </Row> */}
                  <Row className="mb-3">
                    {/* <p className="text-center ml-4 mt-2" style={{fontColor:"#58585e", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}> {this.renderWorkers(FarmData.workers)} </p> */}
                  </Row>
                </CardBody>
                <CardFooter style={{height: '56px', background: 'linear-gradient(90deg, rgba(34,134,249,1) 0%, rgba(5,214,187,1) 100%)', borderBottomRightRadius: '12px', borderBottomLeftRadius: '12px', display: 'flex', flexWrap:'wrap', width: '100%' }} onClick={() => this.setState({isOpen: !isOpen})}>
                    <p style={{color: "white", width: '50%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '17px', marginTop: '15px', position: 'relative', left:'22%'}}>บ่อทั้งหมด</p>
                    <i className= {isOpen === false ? "nc-icon nc-minimal-down" : "nc-icon nc-minimal-up"} style={{color:'white', position: "relative", left: '11%', top: '50%', fontWeight: 'bold'}} />
                </CardFooter>
              </Card>
            </Col>
            <Collapse isOpen={this.state.isOpen} className="w-100">
              {this.renderInsectariumCards(this.state.allpound)}
            </Collapse>
          </Row>
          <AdminNavBar pageName={"โรงเลี้ยง"}/>
         
          <img style={{display: "block", marginLeft: "auto", marginRight: "auto"}} className="mt-3" height="40%" width="40%" src={ache} />

          <WorkerFooter/>
          
        </div>
      </>
    );
  }
}

export default withRouter(Workerdash);
