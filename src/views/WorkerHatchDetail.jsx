import React, {useState,useEffect} from "react";
import axios from 'axios'
import {
  Card,
  CardBody,
  Row,
  Col,
  FormGroup,
  Input,
  Collapse,
} from "reactstrap";
import {
  CircularProgressbar,
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

const handleGraphColorTemp = (percentage) => {
  if (percentage == 32) {
    return "#25f19b";
  } else if (percentage > 32 && percentage < 35) {
    return "#FF9900";
  } else if (percentage < 32 && percentage > 29) {
    return "#FF9900";
  } else if (percentage >= 35 || percentage <= 29) {
    return "#FF0000";
  }
};

const handleGraphColorAmm = (percentage) => {
  if (percentage < 10) {
    return "#25f19b";
  } else if (percentage == 10) {
    return "#FF9900";
  } else if (percentage > 10) {
    return "#FF0000";
  }
};

const handleGraphColorHum = (percentage) => {
  if (percentage <= 50) {
    return "#25f19b";
  } else if (percentage > 50 && percentage <= 60) {
    return "#FF9900";
  } else if (percentage > 60) {
    return "#FF0000";
  }
};
const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
const WorkerHatchDetail = ({
  percentage1,
  percentage2,
  card,
  index,
  id
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [eggstatus, seteggstatus] = useState(card.egg_lossc);
  const [hum, sethum] = useState("")
  const [temp, settemp] = useState("")

  
  const onClick1 = () => {
       
    seteggstatus("ปกติ")

  const status = {status:"ปกติ"} 
  console.log(status)
    axios.post(`${URL}/updateeggstatus/${card.egg_id}`,status)
   .then(res => {
     console.log(res)})
    
  };
  const onClick2 = () => {

    seteggstatus("ไม่ปกติ")
    console.log("dddddddddddd")
    const status = {status:"ไม่ปกติ"}   
    axios.post(`${URL}/updateeggstatus/${card.egg_id}`,status); 
  };
  const gethummidity = async () => {
    try {
  const gethummidity = await axios.get(`${URL}/api/sendhumiegg`)
     
      sethum(gethummidity.data.humiritypound);
      console.log(gethummidity)
    
    } catch (err) {
      console.error(err.message);
    }
  };

  const gettemperature = async () => {
    try {
  const gettemperature = await axios.get(`${URL}/api/sendtempegg`)
     
      settemp(gettemperature.data.temperaturepound); 
      console.log(gettemperature)
    
    } catch (err) {
      console.error(err.message);
    }
  };

      useEffect(()=>{   
        gethummidity()
        gettemperature()
      },[]);
  return (
    <Card className="card-stats" style={{width: "110%" ,marginTop:"5%",marginLeft:"-5%",position: "relative"}}  id={index} key={index}>
      <CardBody >
        <Row  onClick={() => setIsOpen(!isOpen)}>
          <Col xs="12">
            <div
              className="w-100"
              style={{
                display: "flex",
                flexWrap: "wrap",
                marginBottom: "-18px",
                justifyContent: "space-between",
              }}
            >
              <p
                className="ml-3"
                style={{
                  fontColor: "white",
                  fontSize: "16px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                }}
              >
                {"ห้องอบไข่ที่ "}
                {card.egg_name}
              </p>
              <div
                className="border"
                style={{
                  height: "25px",
                  borderRadius: "4px",
                  backgroundColor: eggstatus === "ปกติ" ? "#09c676" : "#e34849",
                  color: "white",
                  textAlign: "center",
                  marginLeft: "8px",
                  width: "50px",
                  margin: "-2px 0 0 auto",
                }}
              >
                <p
                  style={{
                    fontColor: "white",
                    fontSize: "12px",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    marginTop: "4px",
                  }}
                >
                  {eggstatus}
                </p>
              </div>
              <div className="mt-1 ml-3" onClick={() => setIsOpen(!isOpen)}>
                <i
                  className="nc-icon nc-minimal-down"
                  style={{
                    color: "black",
                    fontWeight: "bold",
                    float: "right",
                  }}
                />
              </div>
            </div>
            <hr />
          </Col>
        </Row>
        <Collapse isOpen={isOpen}>
          <Row className="justify-content-center">
            <Col md="3" xs="4">
              <div style={{ width: "100%", margin: "0 auto" }}>
                <p
                  className="text-center"
                  style={{
                    fontColor: "#58585e",
                    fontSize: "14px",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                  }}
                >
                  อุณหภูมิ
                </p>
                <CircularProgressbar
                  value={temp}
                  text={`${temp}°C`}
                  styles={buildStyles({
                    pathColor: handleGraphColorTemp(temp)})} />
              </div>
            </Col>
            <Col md="3" xs="4">
              <div style={{ width: "100%", margin: "0 auto" }}>
                <p
                  className="text-center"
                  style={{
                    fontColor: "#58585e",
                    fontSize: "14px",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                  }}
                >
                  ความชื้น
                </p>
                <CircularProgressbar
                  value={hum}
                  text={`${hum}%`}
                  styles={buildStyles({
                    pathColor: handleGraphColorHum(hum)})} />
              </div>
            </Col>
          </Row>
          <Row>
            <p className="text-center ml-4 mt-4" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>แจ้งจิ้งหรีดหลุด</p>
          </Row>
          <Row>
                    <div className="row mt-3" style={{position: "relative", bottom: "0px", width: '80%', zIndex: '100', height: '50px', boxShadow: "-2px 2px 11px -6px #333", borderRadius: '12px', left:"15%"}}>
                      <div className="col" style={{backgroundColor: eggstatus === "ปกติ" ? "#09c676" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '12px', borderBottomLeftRadius: '12px'}} >
                        <p style={{ width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ปกติ</p>
                      </div>
                      <div className="col" style={{backgroundColor: eggstatus === "ไม่ปกติ" ? "#e34849" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopRightRadius: '12px', borderBottomRightRadius: '12px'}} onClick={onClick2}>
                        <p style={{ width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>จิ้งหรีดหลุด</p>
                      </div>
                    </div>
                  </Row>
                  <Row>
                    {/* <p className="text-center ml-4 mt-4" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>พนักงานประจำบ่อ</p> */}
                  </Row>
                  <Row className="mb-3">
                    {/* <p className="text-center ml-4 mt-2" style={{fontColor:"#58585e", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>นายวิชัย, นางวรรณี, นางปารีณา</p> */}
                  </Row>
                 
                  <Row>
            <div className="row mt-3" 
                  style={{position: "relative",
                  bottom: "15px", 
                  width: '80%', 
                  zIndex: '100', 
                  height: '30px', 
                  boxShadow: "-2px 2px 11px -6px #333", 
                  borderRadius: '12px', 
                  left:"15%"}}>
              <div className="col" style={{backgroundColor: "#504A4B", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '12px', borderBottomLeftRadius: '12px',borderTopRightRadius: '12px', borderBottomRightRadius: '12px'}}>
                <a href={'/admin/WorkerHatchy?hatchery_id=' + card.egg_id}>
                    <p style={{color: "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ดูรายละเอียด</p>
                </a>
              </div>
             
             </div>
          </Row>
          
        </Collapse>
      </CardBody>
    </Card>
  )
    
 
};

export default WorkerHatchDetail;
