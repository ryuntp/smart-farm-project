import React, { useState, useEffect } from "react";
import {
    Badge,
    Button,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Row,
    Col,
    Collapse
  } from "reactstrap";

  import _, { conforms } from 'lodash';

  const NotificaitonCalendar = (props) => {
      const { pageName, fouronenoti, fourfournoti, sevennoti} = props
        console.log("sevennoti",sevennoti)
        console.log("fourfournoti",fourfournoti)
        console.log("fouronenoti",fouronenoti)
        
    //   const [data, setData] = useState([
    //       {
    //         insectariumId: 1,
    //         pondId: 4,
    //         issue: "จิ้งหรีดตาย"
    //     },
    //       {
    //         insectariumId: 3,
    //         // pondId: 7,
    //         issue: "แอมโมเนีย"
    //       }
    //   ])

    //   useEffect(() => {
    //       getNotifications()
    //   }, [])

    //   const getNotifications = () => {
    //       if (pageName === "Dashboard") {
    //           console.log('pageName', pageName);
    //           //axious blabla
    //       } else {
    //         console.log('pageName2', pageName);
    //           //axious blabla
    //       }
    //   } 

    //   const renderAbs = (datum) => {
    //     const abnormals = _.pickBy(datum, (value, key) => _.startsWith(value, "ไม่"))
    //     console.log('abnormals', abnormals);
    //     let absArr = []
    //     for (var name in abnormals) {
    //         if (name === "pound_dead") {absArr.push(<div>จิ้งหรีดตาย</div>)}
    //         else if (name === "pound_enemies") {absArr.push(<div>พบศัตรูจิ้งหรีด</div>) }
    //         else if (name === "insectarium_lossc") {absArr.push(<div>พบจิ้งหรีดหลุด</div>) }
    //         else if (name === "pound_status") {absArr.push(<div className="font-weight-bold">ไม่ปกติ</div>) }
    //         else if (name === "insectarium_status") {absArr.push(<div className="font-weight-bold">ไม่ปกติ</div>) }
    //         else { absArr.push(<div>มีกลิ่นเน่า</div>) }
            
    //     }
    //     return absArr.map((absArr) => absArr)
    //   }
    const renderNotiList1 = (sevennoti) => {
        if (!sevennoti && !fourfournoti && !fouronenoti) { return }
        
        console.log("allNotis",sevennoti)
      //   if (pageName === "Dashboard") {

          return sevennoti.map((datum, index) => (
              
                  <CardBody>
                  <div style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontSize: '14px'}} class="text-danger">พรุ่งนี้นำลูกจิ้งหรีดมาลงบ่อ</div>
                      <p style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontSize: '14px'}}>
                          {datum.egg_id ? `ห้องอบที่ ${datum.egg_name}` : null}  {datum.box_id ? `กล่องที่ ${datum.box_name}` : null}
                      </p>
                      
                  </CardBody>

                  
              
              
          ))
          }

      const renderNotiList2 = (fouronenoti) => {
          if (!sevennoti && !fourfournoti && !fouronenoti) { return }
          
        else{
            return fouronenoti.map((datum, index) => (
                
                    <CardBody>
                    <div style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontSize: '14px'}} class="text-danger">พรุ่งนี้วางไข่</div>
                        <p style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontSize: '14px'}}>
                            {datum.insectarium_id ? `โรงเลี้ยงที่ ${datum.insectarium_name}` : null} {datum.insectariumId} {datum.pound_id ? `บ่อที่ ${datum.pound_name}` : null}
                        </p>
                        
                    </CardBody>
                
                
            ))
        }
        

             
            
      }

      const renderNotiList3 = (fourfournoti) => {
          if (!sevennoti && !fourfournoti && !fouronenoti) { return }
          
          console.log("allNotis",sevennoti)
        //   if (pageName === "Dashboard") {

            return fourfournoti.map((datum, index) => (
                
                    <CardBody>
                    <div style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontSize: '14px'}} class="text-danger">พรุ่งนี้ Harvest</div>
                        <p style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontSize: '14px'}}>
                            {datum.insectarium_id ? `โรงเลี้ยงที่ ${datum.insectarium_name}` : null} {datum.insectariumId} {datum.pound_id ? `บ่อที่ ${datum.pound_name}` : null}
                        </p>
                        
                    </CardBody>
                
                
            ))
         
            
      }
        //   } else {
        //     return allNotis.map((datum, index) => (
        //         <Card key={index} id={index} className='mb-1'>
        //             <CardBody>
        //             <p style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontSize: '14px'}}>
        //                 อีก {datum.dateDiff}วัน ต้อง{datum.job} ที่โรงเลี้ยง {datum.insectariumId} {datum.pondId ? `บ่อที่ ${datum.pondId}` : null}
        //             </p>
        //             </CardBody>
        //         </Card>
        //     ))
        //   }
      

      return (
        <Card className='mb-1'>
          {renderNotiList1(sevennoti)}
          {renderNotiList2(fouronenoti)}
          {renderNotiList3(fourfournoti)}
          </Card>
        
        
        
      
      )
  }

  export default NotificaitonCalendar