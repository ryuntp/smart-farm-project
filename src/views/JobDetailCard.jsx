import React, { useState,useEffect } from "react";
import axios from 'axios'
import WorkerFooter from "../components/WorkerNavBarFooter/WorkerFooter"
import {
  Button,
  Label,
  Card,
  CardBody,
  Row,
  Col,
  FormGroup,
  Input,
  Collapse,
} from "reactstrap";
import {
  CircularProgressbar,
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import _ from "lodash"
//const URL ="http://localhost:8080"
 const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
const percentage1 = 25;
const percentage2 = 88;

const handleGraphColorTemp = (percentage) => {
  if (percentage == 32) {
    return "#25f19b";
  } else if (percentage > 32 && percentage < 35) {
    return "#FF9900";
  } else if (percentage < 32 && percentage > 29) {
    return "#FF9900";
  } else if (percentage >= 35 || percentage <= 29) {
    return "#FF0000";
  }
};

const handleGraphColorAmm = (percentage) => {
  if (percentage < 10) {
    return "#25f19b";
  } else if (percentage == 10) {
    return "#FF9900";
  } else if (percentage > 10) {
    return "#FF0000";
  }
};

const handleGraphColorHum = (percentage) => {
  if (percentage <= 50) {
    return "#25f19b";
  } else if (percentage > 50 && percentage <= 60) {
    return "#FF9900";
  } else if (percentage > 60) {
    return "#FF0000";
  }
};

const JobDetailCard = ({ item, disable,index,id, detail}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [input, settaskinput] = useState([]);
  const [check, settaskcheck] = useState([]);
  const [newinput, setnewinput] = useState([])
  const [newcheck, setnewcheck] = useState()
  const [valuesubmit, setvalue] = useState([])
  const [workerInputValue, setWorkerInputValue] = useState(_.get(item, 'subwtask_val', []))
  const [show,setshow] = useState ("")
  const toggle = () => setIsOpen(!isOpen);
  
  const showheadwork = () =>{
    if(item.insectarium_id === "0"){
      setshow(`กล่อง ${item.box_id}`)
    }
    else if(item.egg_id === "0"){
      setshow(`บ่อ ${item.pound_id}`)
    }
    else if(item.activity_id === "30"){
      setshow(`บ่อ ${item.pound_id} ไป กล่อง ${item.box_id} `)
    }
    else{
      setshow(`กล่อง ${item.box_id} ไป บ่อ ${item.pound_id}`)
    }
  }
  const taskinput = async () => {
    try {
  const taskinput = await axios.get(`${URL}/wstaskinput/${item.activity_id}`)
      //console.log(taskinput.data)
      settaskinput(taskinput.data);
    
    
    } catch (err) {
      //console.error('ERROR ',err.message);
    }
    
  };


  const taskcheck = async () => {
    try {
  const taskcheck = await axios.get(`${URL}/wstaskcheck/${item.activity_id}`)
      // console.log(taskcheck.data)
      settaskcheck(taskcheck.data);
    
    
    } catch (err) {
      //console.error('ERROR ',err.message);
    }
    // const alltask = check.concat(input)
    // setvalue(alltask)
    // console.log(alltask)
  };

  const handleChange1  = (e) => {
    // console.log('index: ' + index);
    console.log('property name: '+ e.target.id);
    let newArr = [...input]
    newArr[e.target.id].workertask_value = e.target.value
    setnewinput(newArr)

    console.log(newinput)
    
  }

  const Checkclick  = (e) => {
    // console.log('index: ' + index);
    console.log('property name: '+ e.target.id);
    let newArr = [...check]
    newArr[e.target.id].workertask_value = "1"

    setnewcheck(newArr)
    // if(newArr[e.target.id].workertask_value = "1"){
    //   newArr[e.target.id].workertask_value = "0"
      
    // }
    //   else if(){
    //     newArr[e.target.id].workertask_value = "1"
    //   }
    console.log(newcheck)
    
  }

  const handleSubmit = () =>{
    const alltask = newinput.concat(newcheck)
    console.log(alltask)
      axios({
        method: 'post',
        url: `${URL}/updatewsubtaskstatusandval/${item.assigned_id}/${item.pound_id}/${item.insectarium_id}`,
        data: alltask
    })

    
    
  }


  
  const renderInput = (input) => {
    console.log('fuckinInput =>', input);
    if (!input) { return }
    return input.map((task,index) => (
        <Row className="mt-3">
            <Col md={12} xs={12}>
              <small
                style={{
                  fontSize: "14px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                }}
              >
                {task.workertask_name}
              </small>
            </Col>
            <Col md={12} xs={12} key={index}>
              <FormGroup className="mt-3">
                
                <Input 
                
                placeholder={item.subwtask_val === null ? "ยังไม่ได้กรอกรายละเอียด" : item.subwtask_val[index]}
                type="input"
                name="workertask_value"
                id ={index}
                value = {task.workertask_value}
                onChange={e =>handleChange1(e)}
                disabled
                 />
                 
              </FormGroup>
            </Col>
          </Row>
    ))
  }


  const renderCheck = (check) => {
    if (!check) { return }
    return check.map((task , index) => (
        <Row className="mt-3">
            <Col md={12} xs={12}>
              <small
                style={{
                  fontSize: "14px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                }}
              >
                {task.workertask_name}
              </small>
            </Col>
            <Col md={12} xs={12}>
              <FormGroup className="mt-3" key={index}
              style={{
                marginLeft: "20px"
              }}>
                <Label check>
                  <Input 
                  type="checkbox"
                  id ={index}
                  onClick = {(e) => Checkclick(e)}
                  defaultChecked={ item.subwtask_val === null ? false : true}
                  disabled
                   />
                    <span className="form-check-sign" />
                      ทำแล้ว
                </Label>
              </FormGroup>
            </Col>
          </Row>
    ))
  }

 
  useEffect(() => {
    taskinput()
    taskcheck()
    showheadwork()
    //console.log("The value after update", newcheck)
    
  },[])



  return (
    <Card className="card-stats mt-2">
      <CardBody>
        <Row>
          <Col xs="12">
            <div
              className="w-100"
              onClick={toggle}
              style={{
                display: "flex",
                flexWrap: "wrap",
                marginBottom: "-18px",
                justifyContent: "space-between",
              }}
            >
              <p
                className="ml-3"
                style={{
                  fontColor: "white",
                  fontSize: "16px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                }}
              >
                {show}
              </p>
              <div
                className="border"
                style={{
                  height: "25px",
                  borderRadius: "4px",
                  backgroundColor: item.subwtask_status === "เสร็จแล้ว" ? "#09c676" : "#e34849",
                  color: "white",
                  textAlign: "center",
                  marginLeft: "8px",
                  width: item.subwtask_status === "เสร็จ" ? "50px" : "70px",
                  margin: "-2px 0 0 auto",
                }}
              >
                <p
                  style={{
                    fontColor: "white",
                    fontSize: "12px",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    marginTop: "4px",
                  }}
                >
                  {item.subwtask_status}
                </p>
              </div>
              <div className="mt-1 ml-3" onClick={toggle}>
                <i
                  className="nc-icon nc-minimal-down"
                  style={{
                    color: "black",
                    fontWeight: "bold",
                    float: "right",
                  }}
                />
              </div>
            </div>
            <hr />
          </Col>
        </Row>
        <Collapse isOpen={isOpen}>
         
          {renderInput(input)}
          {renderCheck(check)}
          
         
            
        </Collapse>
      </CardBody>
    </Card>
  );
};

export default JobDetailCard;
