import axios from 'axios';
import React, { useState } from "react";
import {
  Button,
  Label,
  Input,
  Modal, ModalHeader, ModalBody, ModalFooter
} from "reactstrap";
import "../assets/css/editPondModal.css"
import _ from "lodash"

const EditPondModal = (props) => {
    const { species, toggle, modal, setModal, farmId, pondId } = props
    const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"

    const [selectedSpecie, setSelectedSpecie] = useState(null)

    const renderSpecieCheckboxes = (species) => {
        return species.map((specie, index) => (
            <div className="form-check-radio d-flex">
                <Label check>
                <Input
                    defaultValue={specie.id}
                    id='specie_'{...index}
                    name="exampleRadios"
                    type="radio"
                    onChange={() => setSelectedSpecie(specie.id)}
                />
                <span className="form-check-sign" />
                </Label>
                <small className="ml-2" style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai'}}>{specie.name}</small>
            </div>
        ))
    }

    const onSubmit = () => {
        toggle()
        axios.post(`${URL}/editcrickettype/${selectedSpecie}/${pondId}/${farmId}`)
        .then(res => {
            if (_.get(res, "status", null) === 200) {
                window.location.href = `/admin/farm?farm_id=${farmId}`
            }
        })
    }
    return (
        <Modal isOpen={modal} toggle={toggle} className="custom-modal-style">
        <ModalHeader toggle={toggle}><h2 className="text-center" style={{color:"black", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>แก้ไขบ่อ</h2></ModalHeader>
            <ModalBody>
                {renderSpecieCheckboxes(species)}
            </ModalBody>
            <ModalFooter>
            <Button color="primary" onClick={onSubmit}>บันทึก</Button>{' '}
            <Button color="secondary" onClick={toggle}>ยกเลิก</Button>
        </ModalFooter>
    </Modal>
    )
}

export default EditPondModal