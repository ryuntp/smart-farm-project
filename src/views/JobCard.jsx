import React from "react";
import styled from "styled-components";
import { Card, CardBody, Row, Col } from "reactstrap";

const handleColorType = (bgcolor) => {
  switch (bgcolor) {
    case "Bret":
      return "#03a9f3";
    case "Bruh":
      return "#f56342";
    default:
      return "#000";
  }
};

const WorkTodo = styled.div`
  width: 50%;
  height: auto;
  border-radius: 4px;
  background-color: ${({ bgcolor }) => handleColorType(bgcolor)};
  color: white;
  text-align: center;
  margin-left: 6px;
`;

const WorkTodoText = styled.p`
  color: white;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
  font-weight: bold;
  margin-top: 4px;
`;

const WorkName = styled.p`
  color: #333e63;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
  font-weight: bold;
  margin-top: 8px;
  margin-left: 8px;
`;

const WorkPlace = styled.div`
  margin-top: 2%;
`;

const ContentWrapper = styled.div`
  display: flex;
  margin: 2% 0;
`;

const ContentText = styled.p`
  margin-left: 8px;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
`;

const JobCard = ({ dumbAPI }) => {
  return dumbAPI.map((item) => (
    <Card class="card-calendar" key={item.id}>
      <div className="row">
        <div
          className="col-10"
          style={{ padding: "2% 8%", borderRight: "1px solid #d3d3d3" }}
        >
          <WorkTodo bgcolor={item.username}>
            <WorkTodoText>{item.name}</WorkTodoText>
          </WorkTodo>
          <WorkName>{item.username}</WorkName>
          <WorkPlace>
            <ContentWrapper>
              <i
                style={{
                  fontWeight: "bold",
                  fontSize: "18px",
                  color: "#0dc5ca",
                  marginLeft: "8px",
                }}
                className={`nc-icon nc-pin-3`}
              />
              <ContentText>โรงเลี้ยง1 บ่อ 2,4,6</ContentText>
            </ContentWrapper>
            <ContentWrapper>
              <i
                style={{
                  fontWeight: "bold",
                  fontSize: "18px",
                  color: "#0dc5ca",
                  marginLeft: "8px",
                }}
                className={`nc-icon nc-single-02`}
              />
              <ContentText>ดวงแก้ว, หฤทัย</ContentText>
            </ContentWrapper>
            <ContentWrapper>
              <i
                style={{
                  fontWeight: "bold",
                  fontSize: "18px",
                  color: "#0dc5ca",
                  marginLeft: "8px",
                }}
                className={`nc-icon nc-single-02`}
              />
              <ContentText>ดวงแก้ว, หฤทัย</ContentText>
            </ContentWrapper>
          </WorkPlace>
        </div>
        <div
          className="col-2"
          style={{ display: "flex", alignItems: "center" }}
        >
          <i
            style={{
              fontWeight: "bold",
              fontSize: "18px",
              color: "#0dc5ca",
            }}
            className={`nc-icon nc-single-02`}
          />
        </div>
      </div>
    </Card>
  ));
};

export default JobCard;
