/*!

=========================================================
* Paper Dashboard PRO React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import axios from "axios"

// react plugin used to create datetimepicker
import ReactDatetime from "react-datetime";
import moment from "moment";
import { Moment } from "moment";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select from "react-select";
import AdminFooter from "../components/AdminNavbarFooter/AdminFooter"
// react plugin used to create switch buttons
import Switch from "react-bootstrap-switch";
// plugin that creates slider
import Slider from "nouislider";
import AdminNavBar from '../components/Navbars/AdminNavbar'
import WorkerInquiryFooter from '../components/WorkerNavBarFooter/WorkerInquiryFooter'
import { Redirect } from 'react-router-dom';
import Cookies from 'js-cookie'

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  FormGroup,
  Form,
  Button,
  Progress,
  Row,
  Col,
  Label,
  Input,
  Collapse
} from "reactstrap";

// core components
import ImageUpload from "components/CustomUpload/ImageUpload.jsx";

class Formbeforework extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Sick: 0,
      Cloth:0,
      Wear:0,
      shoes:0,
      wash:0,
      nail:0,
      smell:0,
      sickSymtom: null
    };
  }
  componentDidMount() {
  }

  handleSelectInsectarium = () => {
    this.setState({})
  }

  handleTimeChange = (startTime, type) => {
    const hour = startTime.getHours()
    const minuteLength = String(startTime.getMinutes()).length
    const minute = (minuteLength === 1) ? `0` + startTime.getMinutes() : startTime.getMinutes()
    const combined = hour + ":" + minute
    if (type === "start"){
        this.setState({startTime: combined})
    } else {
        this.setState({endTime: combined})
    }
  }

  onSubmit = () => {
      const { Sick, sickSymtom, Cloth, Wear, shoes, wash, nail, smell } = this.state

      const submittedData = {
        sick: Sick,
        cloth: Cloth,
        wear: Wear,
        shoes: shoes,
        wash: wash,
        nail: nail,
        smell: smell,
        sickSymtom: sickSymtom
      }

      console.log('submittedData', submittedData);

      axios({
        method: 'post',
        url: `https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com/workerform/${Cookies.get('workerId')}`,
        data: submittedData
      }).then(
          res => {
              if (res.status === 200){
                  this.setState({statusResponse: 200})
                  console.log('resresres', res);
              }
              else {
                  return
              }
          }
      )


  }

  render() {
      const { pond, insectarium, worker ,toggleSick,Sick,Cloth,Wear,shoes,wash,nail,smell} = this.state
      if (this.state.statusResponse === 200) {
        return (
            <Redirect to={`/admin/workerhome?farm_id=${Cookies.get('farmId')}`} />
        )
      }
    return (
      <>
        <div className="content">
          <Row>
            <div style={{width: '100%', display: 'flex'}} className="justify-content-between mt-3">
                <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '12px'}}>1. ไม่มีอาการป่วย/ไม่มีบาดแผลเสี่ยงติดเชื้อ</p>
                
            </div>
            </Row>
            <Row className="ml-4 mt-2">
          <div className="row" style={{width: '100%', height: '40px'}}>
            <div className="col" style={{backgroundColor: Sick === 0 ? "#08d1c0" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '8px', borderBottomLeftRadius: '8px'}} onClick={()=> this.setState({ toggleSick: false, Sick: 0 })}>
              <p style={{color: Sick === 0 ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ใช่</p>
            </div>
            <div className="col" style={{backgroundColor: Sick === 0 ? "white" : "#f1b825", padding: '0px 0px 0px 0px', display: 'relative', border:'solid 1px lightgray', borderLeft: 'none', borderTopRightRadius: '8px', borderBottomRightRadius: '8px'}} onClick={() => this.setState({toggleSick: !toggleSick, Sick: 1})}>
              <p style={{color: Sick === 0 ? "gray" : "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่, มีอาการป่วย</p>
            </div>
          </div>
        </Row>
        <Collapse isOpen={toggleSick}>
          

          <Col className="mt-2">
          <FormGroup className="mt-3">
                    <Input placeholder="กรอกรายละเอียดอาการ" type="input" onChange={e => this.setState({sickSymtom: e.target.value})}/>
                </FormGroup>
          </Col>
        </Collapse>
        <Row>
            <div style={{width: '100%', display: 'flex'}} className="justify-content-between mt-3">
                <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '12px'}}>2. เสื้อผ้าสะอาด ถูกต้องตามระเบียบ</p>
                
            </div>
            </Row>
            <Row className="ml-4 mt-2">
          <div className="row" style={{width: '100%', height: '40px'}}>
            <div className="col" style={{backgroundColor: Cloth === 0 ? "#08d1c0" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '8px', borderBottomLeftRadius: '8px'}} onClick={()=> this.setState({ Cloth: 0 })} onClick={()=> this.setState({ Cloth: 0 })}>
              <p style={{color: Cloth === 0 ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ใช่</p>
            </div>
            <div className="col" style={{backgroundColor: Cloth === 0 ? "white" : "#f1b825", padding: '0px 0px 0px 0px', display: 'relative', border:'solid 1px lightgray', borderLeft: 'none', borderTopRightRadius: '8px', borderBottomRightRadius: '8px'}} onClick={() => this.setState({ Cloth: 1})}>
              <p style={{color: Cloth === 0 ? "gray" : "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่</p>
            </div>
          </div>
        </Row>

        <Row>
            <div style={{width: '100%', display: 'flex'}} className="justify-content-between mt-3">
                <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '12px'}}>3. สวมหมวก/เน็ตคลุมผมเรียบร้อย</p>
                
            </div>
            </Row>
            <Row className="ml-4 mt-2">
          <div className="row" style={{width: '100%', height: '40px'}}>
            <div className="col" style={{backgroundColor: Wear === 0 ? "#08d1c0" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '8px', borderBottomLeftRadius: '8px'}} onClick={()=> this.setState({ Wear: 0 })}>
              <p style={{color: Wear === 0 ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ใช่</p>
            </div>
            <div className="col" style={{backgroundColor: Wear === 0 ? "white" : "#f1b825", padding: '0px 0px 0px 0px', display: 'relative', border:'solid 1px lightgray', borderLeft: 'none', borderTopRightRadius: '8px', borderBottomRightRadius: '8px'}} onClick={() => this.setState({ Wear: 1})}>
              <p style={{color: Wear === 0 ? "gray" : "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่</p>
            </div>
          </div>
        </Row>

        <Row>
            <div style={{width: '100%', display: 'flex'}} className="justify-content-between mt-3">
                <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '12px'}}>4. สวมใส่รองเท้าที่จัดเตรียมไว้สำหรับเข้าในโรงเพาะเลี้ยงจิ้งหรีด</p>
                
            </div>
            </Row>
            <Row className="ml-4 mt-2">
          <div className="row" style={{width: '100%', height: '40px'}}>
            <div className="col" style={{backgroundColor: shoes === 0 ? "#08d1c0" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '8px', borderBottomLeftRadius: '8px'}} onClick={()=> this.setState({ shoes: 0 })}>
              <p style={{color: shoes === 0 ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ใช่</p>
            </div>
            <div className="col" style={{backgroundColor: shoes === 0 ? "white" : "#f1b825", padding: '0px 0px 0px 0px', display: 'relative', border:'solid 1px lightgray', borderLeft: 'none', borderTopRightRadius: '8px', borderBottomRightRadius: '8px'}} onClick={() => this.setState({ shoes: 1})}>
              <p style={{color: shoes === 0 ? "gray" : "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่</p>
            </div>
          </div>
        </Row>

        <Row>
            <div style={{width: '100%', display: 'flex'}} className="justify-content-between mt-3">
                <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '12px'}}>5. ล้างมือตามขั้นตอนที่ถูกต้อง/ฆ่าเชื้อด้วยแอลกอฮอล์ที่มือและรองเท้าก่อนเข้าพื้นที่โรงเลี้ยง</p>
                
            </div>
            </Row>
            <Row className="ml-4 mt-2">
          <div className="row" style={{width: '100%', height: '40px'}}>
            <div className="col" style={{backgroundColor: wash === 0 ? "#08d1c0" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '8px', borderBottomLeftRadius: '8px'}} onClick={()=> this.setState({ wash: 0 })}>
              <p style={{color: wash === 0 ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ใช่</p>
            </div>
            <div className="col" style={{backgroundColor: wash === 0 ? "white" : "#f1b825", padding: '0px 0px 0px 0px', display: 'relative', border:'solid 1px lightgray', borderLeft: 'none', borderTopRightRadius: '8px', borderBottomRightRadius: '8px'}} onClick={() => this.setState({ wash: 1})}>
              <p style={{color: wash === 0 ? "gray" : "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่</p>
            </div>
          </div>
        </Row>


        <Row>
            <div style={{width: '100%', display: 'flex'}} className="justify-content-between mt-3">
                <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '12px'}}>6. เล็บมือสั้นสะอาด</p>
                
            </div>
            </Row>
            <Row className="ml-4 mt-2">
          <div className="row" style={{width: '100%', height: '40px'}}>
            <div className="col" style={{backgroundColor: nail === 0 ? "#08d1c0" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '8px', borderBottomLeftRadius: '8px'}} onClick={()=> this.setState({ nail: 0 })}>
              <p style={{color: nail === 0 ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ใช่</p>
            </div>
            <div className="col" style={{backgroundColor: nail === 0 ? "white" : "#f1b825", padding: '0px 0px 0px 0px', display: 'relative', border:'solid 1px lightgray', borderLeft: 'none', borderTopRightRadius: '8px', borderBottomRightRadius: '8px'}} onClick={() => this.setState({ nail: 1})}>
              <p style={{color: nail === 0 ? "gray" : "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่</p>
            </div>
          </div>
        </Row>

        <Row>
            <div style={{width: '100%', display: 'flex'}} className="justify-content-between mt-3">
                <p style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px', marginLeft: '12px'}}>7. ไม่ใช้น้ำหอม หรือผลิตภัณฑ์ที่มีกลิ่นฉุน</p>
                
            </div>
            </Row>
            <Row className="ml-4 mt-2">
          <div className="row" style={{width: '100%', height: '40px'}}>
            <div className="col" style={{backgroundColor: smell === 0 ? "#08d1c0" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '8px', borderBottomLeftRadius: '8px'}} onClick={()=> this.setState({ smell: 0 })}>
              <p style={{color: smell === 0 ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ใช่</p>
            </div>
            <div className="col" style={{backgroundColor: smell === 0 ? "white" : "#f1b825", padding: '0px 0px 0px 0px', display: 'relative', border:'solid 1px lightgray', borderLeft: 'none', borderTopRightRadius: '8px', borderBottomRightRadius: '8px'}} onClick={() => this.setState({ smell: 1})}>
              <p style={{color: smell === 0 ? "gray" : "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ไม่</p>
            </div>
          </div>
        </Row>
          <AdminNavBar pageName="แบบฟอร์มก่อนเข้างาน"/>
          <div onClick={() => this.onSubmit()}>
            <WorkerInquiryFooter/>
        </div>
        </div>
      </>
    );
  }
}

export default Formbeforework;
