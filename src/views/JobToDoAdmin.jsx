import { cardSubtitle } from "assets/jss/material-kit-react";
import React, { useState,useEffect } from "react";
import styled from "styled-components";
import axios from 'axios'
import JobDetailCard from "./JobDetailCard";
import WorkerFooter from "../components/WorkerNavBarFooter/WorkerFooter"
import AdminFooter from "../components/AdminNavbarFooter/AdminFooter"
import AdminNavBar from '../components/Navbars/AdminNavbar'
import { withRouter } from "react-router-dom";
import Cookies from "js-cookie";
import _ from "lodash"
import {
  Row,
  Button,
  Modal, 
  ModalHeader, 
  ModalBody, 
} from "reactstrap";
 const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
//const URL ="http://localhost:8080"
const InfoRow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
`;

const InfoContent = styled.div``;

const InfoDetail = styled.div`
  margin: 2% 0;
`;

const ContentWrapper = styled.div`
  display: flex;
  margin: 5% 0;
`;

const ContentText = styled.p`
  margin-left: 8px;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
`;

const handleColorType = (bgcolor) => {
  switch (bgcolor) {
    case "เสร็จแล้ว":
      return "#09c676";
    case "ยังไม่เสร็จ":
      return "#e34849";
    default:
      return "#000";
  }
};

const WorkTodo = styled.div`
  width: 45%;
  height: auto;
  border-radius: 4px;
  background-color: ${({ bgcolor }) => handleColorType(bgcolor)};
  color: white;
  text-align: center;
`;

const WorkTodoText = styled.p`
  color: white;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
  font-weight: bold;
  margin-top: 4px;
`;

const workAPI = {
  id: 1,
  status: "ยังไม่ได้ทำ",
  name: "ตรวจดูสถานที่อบไข่",
  ponds: [2, 5, 7],
  startTime: "10.00",
  endTime: "13.00",
  insectarium: {
      number: 1
  },
  workers: [
    {
      id: 1,
      firstName: "ลิซ่า"
    },
    {
      id: 2,
      firstName: "เจนนี่"
    }
]
};

const insectariumAPI = [
  {
    id: 1,
    puddle: "บ่อที่1",
  },
  { id: 2, puddle: "บ่อที่2" },
  { id: 3, puddle: "บ่อที่3" },
];

const percentage1 = 25;
const percentage2 = 5;
const percentage3 = 35;

const JobToDoAdmin = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [active, setActive] = useState(null);
  const [id,setid]=useState("")
  const [jobapi, setjob]=useState([]);
  const [actname, setname]=useState("");
  const [poundseq, setpoundlist] = useState([]);
  const [boxseq, setboxlist] = useState([]);
  const [Workerseq, setWorkerlist] = useState([]);
  const [Workername, setWorkername] = useState([]);
  const [detail, setdetail] = useState([]);
  const [success, setfin] = useState("none")
  const [both, setboth]= useState("none")
  const [nameandval ,settasknameandval] = useState([])
  const [modal, setModal] = useState(false)
  const [date,setdate] =useState("")
  const [cardId,setCardId] =useState("")

  const toggle = (id) => {
    setIsOpen(!isOpen);
    setActive(id)
  }


  

  const onejob = async (params) => {
    
    const onejob = await axios.get(`${URL}/oneact/${params}`)
    const data = onejob.data[0]
    console.log('onejob', onejob.data[0]);
        setjob(onejob.data[0]);
        setpoundlist(onejob.data[0].assigned_pound);
        setboxlist(onejob.data[0].box_id)
        setWorkerlist(onejob.data[0].assigned_worker);

        var datearray = onejob.data[0].assigned_date.split("-");
        var newdate = datearray[1] + '-' + datearray[0] + '-' + datearray[2];
        setdate(newdate);

        if(onejob.data[0].assigned_insectarium === "0"){
          setboth(`ห้องอบไข่ ${data.egg_id} กล่อง ${renderPonds(data.box_id)}`)
        }
        else if(onejob.data[0].egg_id === "0"){
          setboth(`โรงเลี้ยง ${data.assigned_insectarium} บ่อ ${renderPonds(data.assigned_pound)}`)
        }
        else if(onejob.data[0].activity_id === "30"){
          setboth(`โรงเลี้ยง ${data.assigned_insectarium} บ่อ ${renderPonds(data.assigned_pound)} ไป ห้องอบไข่ ${data.egg_id} กล่อง ${renderPonds(data.box_id)} `)
        }
        else{
          setboth(`ห้องอบไข่ ${data.egg_id} กล่อง ${renderPonds(data.box_id)} ไป โรงเลี้ยง ${data.assigned_insectarium} บ่อ ${renderPonds(data.assigned_pound)}`)
        }
  
        const activityname = await axios.get(`${URL}/allact23/${onejob.data[0].activity_id}`)
        // console.log('activityname', activityname.data);
            setname(activityname.data[0]);
  
        const workername = await axios.get(`${URL}/workername/{${onejob.data[0].assigned_worker}}`)
        // console.log('activityname', workername.data);
          setWorkername(workername.data);

         const managertasknameandval = await axios.get(`${URL}/managertasknameandval/${onejob.data[0].assigned_id}`)
          settasknameandval(managertasknameandval.data)
          console.log(nameandval) 
    
        
    }
  
    const ponddetail = async (params) => {
      try {
    const ponddetail = await axios.get(`${URL}/subwstatus/${params}`)
        // console.log(ponddetail.data)
        setdetail(ponddetail.data);

      } catch (err) {
        // console.error('ERROR ',err.message);
      }
      
    };

    const Alltasksuccess = async (params) => {
      try {
    const Alltasksuccess = await axios.get(`${URL}/subwstatusnotfin/${params}`)
         console.log(Alltasksuccess.data)
        if(Alltasksuccess.data.length == 0){
          setfin("block");
        }
        else{
          setfin("none");
        }
      
      } catch (err) {
        // console.error('ERROR ',err.message);
      }
      
    };
    
  const renderPonds = (poundseq) => {
    if (!poundseq) { return }
    return poundseq.map((pound) => (
        pound + " "
    ))
}
const renderWorker = (Workername) => {
  if (!Workername) { return }
  return Workername.map((name) => (
      name.worker_name + " "
  ))
}


const renderNameandVal = (nameandval) => {
  if (!nameandval) { return }
  return nameandval.map((task,index) => (
    <InfoDetail>
  
    <b style={{fontFamily: 'IBM Plex Sans Thai' ,fontSize: 17}}>{task.managertask_name}</b>
    <br></br>
    <b style={{fontFamily: 'IBM Plex Sans Thai'}}>- {task.mantaskval_val}</b>
    </InfoDetail>
  ))
}

  const renderJobCards = (cards) => {
      return cards.map((item, index) => (
        <JobDetailCard
          disable={true}
          isOpen={isOpen}
          toggle={toggle}
          active={active}
          item={item}
          index={index}
          id={id}
          detail={jobapi}
          
        />
        )
    )
  }

  const handleSubmit = () =>{
    
      axios({
        method: 'post',
        url: `${URL}/updatewassignedstatus/${id}`
    })
  }
 
  useEffect(() => {
    var query = new URLSearchParams(props.location.search);
    var params = query.get('card_id');
    setid(params)
    onejob(params)
    ponddetail(params)
    Alltasksuccess(params)
    console.log(success)
    
  },[])

  const handleDeleteJob = () => {
    setModal(!modal)
    axios.post(`${URL}/delassginedcard/${id}`)

    window.location.href = "/admin/calendar"
  }

  const dashboard = () => { 
    // - you can now access this.props.history for navigation
    props.history.push({pathname: "/admin/calendar"});
  };

  const editjob = () => { 
    // - you can now access this.props.history for navigation
    props.history.push({pathname: "/admin/edit-job", details: { actname, jobapi, Workername, id, Workerseq, nameandval }});
  };

  var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0');

  const toTimestamp = (year,month,day,hour,minute,second) => {
    var datum = new Date(Date.UTC(year,month-1,day,hour,minute,second));
    return datum.getTime()/1000;
  }
  let jobTimeStamp = null
  if (date && _.get(jobapi, "assigned_timestart", null)) {
    let JTS = toTimestamp(date.split("-")[2], date.split("-")[1], date.split("-")[0], jobapi.assigned_timestart.split(":")[0], jobapi.assigned_timestart.split(":")[1], "00")
    jobTimeStamp = JTS
  }

  let DateNowTimeStamp = toTimestamp(today.getFullYear(), mm, dd, today.getHours(), today.getMinutes(), today.getSeconds() )

  if (!jobapi) { return } 
  return (
  
    <div className="content" style={{ marginTop: "15vh" }}>
      <button
        className="btn btn-primary d-flex mb-5"
        onClick={dashboard}
        >
          <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
          <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>กลับหน้ามอบหมายงาน</p>
      </button>
      <InfoRow>
        <InfoContent>
          <WorkTodo bgcolor={jobapi.assigned_status}>
            <WorkTodoText>{jobapi.assigned_status}</WorkTodoText>
          </WorkTodo>
          <h2 style={{fontFamily: 'IBM Plex Sans Thai'}}>{actname.activity_name}</h2>
          <ContentWrapper>
            <i
              style={{
                fontWeight: "bold",
                fontSize: "18px",
                color: "#0dc5ca",
              }}
              className={`nc-icon nc-pin-3`}
            />
            <ContentText>{both}</ContentText>
          </ContentWrapper>
          <ContentWrapper>
            <i
              style={{
                fontWeight: "bold",
                fontSize: "18px",
                color: "#0dc5ca",
              }}
              className={`nc-icon nc-single-02`}
            />
            <ContentText>{renderWorker(Workername)}</ContentText>
          </ContentWrapper>
          <ContentWrapper>
            <i
              style={{
                fontWeight: "bold",
                fontSize: "18px",
                color: "#0dc5ca",
              }}
              className={`nc-icon nc-calendar-60`}
            />
            <ContentText>{date}</ContentText>
            </ContentWrapper>
          <ContentWrapper>
            <i
              style={{
                fontWeight: "bold",
                fontSize: "18px",
                color: "#0dc5ca",
              }}
              className={`nc-icon nc-time-alarm`}
            />
            <ContentText>{jobapi.assigned_timestart}-{jobapi.assigned_timefin} น.</ContentText>
          </ContentWrapper>
        </InfoContent>
      </InfoRow>
      <InfoRow>
        <InfoDetail>
        <h3 style={{fontFamily: 'IBM Plex Sans Thai'}}>รายละเอียดงาน</h3>
          {renderNameandVal(nameandval)}
          <br></br>
          <b style={{fontFamily: 'IBM Plex Sans Thai'}}>{jobapi.assigned_note}</b>
          
        </InfoDetail>
      </InfoRow>
      <h2 style={{fontFamily: 'IBM Plex Sans Thai'}}>งานที่ต้องทำ</h2>
      {renderJobCards(detail)}
      {((jobTimeStamp) && (jobTimeStamp > DateNowTimeStamp)) ? 
        <div>
          <Button className="w-100 d-flex justify-content-center" color="primary" onClick={editjob}>
            <i class="fas fa-pencil-alt mt-1 mr-2"></i>
            <p className="text-center" style={{ fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>แก้ไข</p>
          </Button>{' '}
        </div>
        :
        null
      }
      <div>
        <Button className="w-100 d-flex justify-content-center" color="danger" onClick={()=> setModal(!modal)}>
          <i class="far fa-trash-alt mt-1 mr-2"></i>
          <p className="text-center" style={{ fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>ลบงาน</p>
        </Button>{' '}
      </div>
      <AdminNavBar pageName="งาน"/>
      <AdminFooter active={true}/>
      <Modal isOpen={modal} style={{top: '30%'}} className="custom-modal-style h-25">
            <ModalHeader className="">
              <div className="d-flex justify-content-center">
                <h5 className="text-center" style={{color:"black", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}> ต้องการลบงานนี้? </h5>
                <i style={{position: "absolute", right: '5%'}} className="far fa-times-circle fa-2x text-danger" onClick={() => setModal(!modal)}></i>
              </div>
            </ModalHeader>
                <ModalBody>
                    <Row className="justify-content-center">
                      <Button className="ml-3" color="primary" onClick={() => handleDeleteJob()}>ยืนยัน</Button>
                      <Button className="mr-3" color="danger" onClick={()=> setModal(!modal)}>ยกเลิก</Button>{' '}
                    </Row>
                </ModalBody>
        </Modal>
    </div>
  );
};

export default withRouter(JobToDoAdmin);
