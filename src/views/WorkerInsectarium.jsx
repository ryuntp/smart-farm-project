import Cookies from "js-cookie";
import axios from "axios";
import ache from "../assets/img/ache.png";
import Select from "react-select";
import React from "react";
import WorkerFooter from "../components/WorkerNavBarFooter/WorkerFooter";

import { Button, FormGroup, Form, Input, Row, Col, Collapse } from "reactstrap";
import Steps, { Step } from "rc-steps";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import AdminNavBar from "../components/Navbars/AdminNavbar";
import { withRouter } from "react-router-dom";
import _ from "lodash";
var ids;

const URL = "https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com";
const handleGraphColorTemp = (percentage) => {
  if (percentage == 32) {
    return "#25f19b";
  } else if (percentage > 32 && percentage < 35) {
    return "#FF9900";
  } else if (percentage < 32 && percentage > 29) {
    return "#FF9900";
  } else if (percentage >= 35 || percentage <= 29) {
    return "#FF0000";
  }
};

const handleGraphColorAmm = (percentage) => {
  if (percentage < 10) {
    return "#25f19b";
  } else if (percentage == 10) {
    return "#FF9900";
  } else if (percentage > 10) {
    return "#FF0000";
  }
};

const handleGraphColorHum = (percentage) => {
  if (percentage <= 50) {
    return "#25f19b";
  } else if (percentage > 50 && percentage <= 60) {
    return "#FF9900";
  } else if (percentage > 60) {
    return "#FF0000";
  }
};
class WorkerInsectarium extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      data: [],
      rodSmellReport: "ปกติ",
      deadInsects: "ปกติ",
      hum: "0",
      temp: "0",
      amm: null,
      ammDisplay: null,
      enemy: "",
      enemyDisplay: "",
      insectariumData: {
        id: 1,
        insectariumStatus: "ปกติ",
        temp: "25",
        ammonia: "5",
        humidity: "35",
        rodSmell: "ปกติ",
        deadInsects: "ปกติ",
        escapeInsects: "ปกติ",
        accumulatedWater: 11,
        accumulatedFood: 21,
      },
      toggleEnemyReport: "",
      enemyReport: "",
      insectariumStatus: "ปกติ",
    };
    this.checkstatus = this.checkstatus.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
  }

  componentDidMount() {
    var query = new URLSearchParams(this.props.location.search);
    var params = query.get("pound_id");

    var farmIdParam = query.get("farm_id");
    this.setState({ farmIdParam: farmIdParam });
    console.log(params);
    ids = params;

    this.gettemp();
    //this.gethum();
    this.getpond();
    this.gethum();
    // const URL = window.location.href
    // const query = new URLSearchParams(URL);
    //  const pondId = query.get("http://localhost:3000/admin/insectarium?pound_id")
    //  console.log(pondId)
    //  console.log(URL)
    //  this.setState({id:pondId})
    //this.interval = setInterval(this.getpond, 1000);
  }
  componentWillUnmount() {
    // Clear the interval right before component unmount
    //clearInterval(this.interval);
  }

  handleChange(event) {
    this.setState({
      amm: event.target.value,
    });
  }

  handleChange2(event) {
    this.setState({
      enemy: event.target.value,
    });
  }
  handleSubmit = () => {
    this.setState({ ammDisplay: this.state.amm });
    const data = { amm: this.state.amm };
    axios({
      method: "post",
      url: `${URL}/updateamm/${this.state.id}`,
      data: data,
    });
  };
  Enemynormal = () => {
    axios({
      method: "post",
      url: `${URL}/updateamm/${this.state.id}`,
    });
  };

  handleSubmit2 = () => {
    const { toggleEnemyReport } = this.state;
    this.setState({ toggleEnemyReport: !toggleEnemyReport });
    this.setState(
      (prevState) => ({
        data: {
          ...prevState.data,
          pound_enemies: "ไม่ปกติ",
        },
      }),
      this.checkstatus
    );

    const data = { eid: this.state.enemy.value };
    axios({
      method: "post",
      url: `${URL}/updateenemies/${this.state.id}`,
      data: data,
    });
    console.log(data);
  };
  getpond = () => {
    axios.get(`${URL}/pound/${ids}`).then((res) => {
      console.log("lovely", res.data);
      this.setState({ data: res.data[0] });
      this.setState({ enemyDisplay: _.get(res, "data[0].enemies_id") });
      if (_.get(res, "data[0].enemies_id") !== "0") {
        this.setState({ toggleEnemyReport: true });
      }
      console.log(this.state.data);
      this.setState({ id: this.state.data.pound_id });

      console.log(this.state.data.pound_id);
      console.log(this.state.id);
    });
  };

  checkstatus = () => {
    if (
      this.state.data.pound_stench == "ปกติ" &&
      this.state.data.pound_dead == "ปกติ" &&
      this.state.data.pound_enemies == "ปกติ"
    ) {
      this.setState((prevState) => ({
        data: {
          // object that we want to update
          ...prevState.data, // keep all other key-value pairs
          pound_status: "ปกติ", // update the value of specific key
        },
      }));
      const data = {
        id: this.state.id,
        status: "ปกติ",
        dead: this.state.data.pound_dead,
        smell: this.state.data.pound_stench,
        enemy: this.state.data.pound_enemies,
        workerType: Cookies.get("workerType"),
        workerId: Cookies.get("workerId"),
      };
      axios({
        method: "post",
        url: `${URL}/updatepoundstatus/${this.state.id}/${Cookies.get(
          "workerId"
        )}`,
        data: data,
      });
      console.log("if");
    } else {
      this.setState((prevState) => ({
        data: {
          // object that we want to update
          ...prevState.data, // keep all other key-value pairs
          pound_status: "ไม่ปกติ", // update the value of specific key
        },
      }));
      const data = {
        id: this.state.id,
        status: "ไม่ปกติ",
        dead: this.state.data.pound_dead,
        smell: this.state.data.pound_stench,
        enemy: this.state.data.pound_enemies,
        enemyid: this.state.enemy.value,
        workerType: Cookies.get("workerType"),
        workerId: Cookies.get("workerId"),
      };
      axios({
        method: "post",
        url: `${URL}/updatepoundstatus/${this.state.id}/${Cookies.get(
          "workerId"
        )}`,
        data: data,
      });
      console.log(data);
      console.log("else");
    }

    console.log(this.state.data.pound_stench);
    console.log(this.state.data.pound_dead);
  };

  gethum = () => {
    axios.get(`${URL}/api/sendhumiinsect`).then((res) => {
      console.log(res);
      this.setState({ hum: res.data.humirityinsect });
    });
  };
  gettemp = () => {
    axios.get(`${URL}/api/sendtempinsect`).then((res) => {
      console.log(res);
      this.setState({ temp: res.data.temperatureinsect });
    });
  };

  farm = () => {
    // - you can now access this.props.history for navigation
    this.props.history.push({
      pathname: "/admin/workerfarm",
      search: "?farm_id=" + this.state.farmIdParam,
      state: this.state,
    });
  };
  getEnemyLabel = (id) => {
    console.log("idid", id);
    switch (id) {
      case "2":
        return "ไรขาว";
      case "3":
        return "ไรแดง";
      case "4":
        return "มด";
      case "5":
        return "จิ้งจก";
      case "6":
        return "แมลงสาบ";
      case "7":
        return "แมงมุม";
      case "8":
        return "นก";
      case "9":
        return "อื่นๆ";
      default:
        return "ศัตรูจิ้งหรีด";
    }
  };
  render() {
    const {
      enemyDisplay,
      insectariumStatus,
      rodSmellReport,
      deadInsects,
      insectariumData,
      toggleEnemyReport,
      enemyReport,
      id,
      amm,
      enemy,
    } = this.state;

    return (
      <>
        <div className="content">
          <button className="btn btn-primary d-flex" onClick={this.farm}>
            <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
            <p
              style={{
                fontColor: "white",
                fontSize: "12px",
                fontFamily: "IBM Plex Sans Thai",
              }}
            >
              กลับหน้าโรงเลี้ยง
            </p>
          </button>
          <Row>
            <Col xs="12">
              <div
                className="border mb-3 mt-1 d-flex"
                style={{
                  height: "50px",
                  borderRadius: "12px",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <div className="w-100" style={{ display: "contents" }}>
                  <p
                    style={{
                      fontColor: "#58585e",
                      fontSize: "18px",
                      fontFamily: "IBM Plex Sans Thai",
                      marginTop: "20px",
                      fontWeight: "bold",
                    }}
                  >
                    สถานะบ่อเลี้ยงที่ {this.state.data.pound_name}
                  </p>
                  <div
                    className="border w-25 mb-4"
                    style={{
                      height: "30px",
                      borderRadius: "4px",
                      marginTop: "20px",
                      backgroundColor:
                        this.state.data.pound_status === "ปกติ"
                          ? "#09c676"
                          : "#e34849",
                      color: "white",
                      textAlign: "center",
                      marginLeft: "8px",
                    }}
                  >
                    <p
                      style={{
                        fontColor: "white",
                        fontSize: "18px",
                        fontFamily: "IBM Plex Sans Thai",
                        fontWeight: "bold",
                        marginTop: "4px",
                      }}
                    >
                      {this.state.data.pound_status}
                    </p>
                  </div>
                </div>
              </div>
              {/* <div>
                <p className="w-100" style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai', top: '58%', textAlign: 'center', right: '0%', position: 'absolute'}}>ทำความสะอาดล่าสุดเมื่อ 16/1/64 23.00น.</p>
              </div> */}
            </Col>
          </Row>
          {/* <div className="mb-3">
            <Steps current={2}>
              <Step icon={<i className={`nc-icon nc-check-2`} />}/>
              <Step icon={<i className={`nc-icon nc-check-2`} />}/>
              <Step/>
              <Step/>
              <Step/>
            </Steps>
          </div> */}
          <div
            className="mb-3 w-25 p-3 text-center"
            style={{
              backgroundColor: "#e7e7e7",
              borderRadius: "20px",
              margin: "auto",
              textAlignLast: "center",
            }}
          >
            <p
              style={{
                fontColor: "#000000",
                fontSize: "12px",
                fontFamily: "IBM Plex Sans Thai",
              }}
            >
              {this.state.data.pound_duration} วัน
            </p>
          </div>
          {/* <Row>
          <Col md="4" xs="4">
            <div>
              <p className="text-center" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>อุณหภูมิ</p>
              <CircularProgressbar 
              value={this.state.temp} 
              text={`${this.state.temp}°C`} 
              styles={buildStyles({
                pathColor: handleGraphColorTemp(this.state.temp)})} />
            </div>
          </Col>
          <Col md="4" xs="4">
            <div>
              <p className="text-center" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>แอมโมเนีย</p>
              <CircularProgressbar 
              value={ this.state.ammDisplay || this.state.data.pound_amm} 
              text={`${ this.state.ammDisplay || this.state.data.pound_amm}ppm`} 
              styles={buildStyles({
                pathColor: handleGraphColorAmm(this.state.ammDisplay || this.state.data.pound_amm)})} />
            </div>
          </Col>
          <Col md="4" xs="4">
          <div>
            <p className="text-center" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>ความชื้น</p>
              <CircularProgressbar 
              value={this.state.hum} 
              text={`${this.state.hum}%`} 
              styles={buildStyles({
                pathColor: handleGraphColorHum(this.state.hum)})} />
          </div>
          </Col>
        </Row> */}
          <Row className="mt-4 mb-4">
            <Col md="4" xs="4">
              <div>
                <p
                  className="text-center"
                  style={{
                    fontColor: "#58585e",
                    fontSize: "14px",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                  }}
                >
                  มีกลิ่นเน่า
                </p>
                <div
                  className="border"
                  style={{
                    height: "35px",
                    borderRadius: "4px",
                    backgroundColor:
                      this.state.data.pound_stench === "ปกติ"
                        ? "#09c676"
                        : "#e34849",
                    color: "white",
                    textAlign: "center",
                    width: "100%",
                    marginTop: "-2px",
                  }}
                >
                  <p
                    style={{
                      fontColor: "white",
                      fontSize: "12px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      marginTop: "4px",
                      textAlign: "center",
                      lineHeight: "28px",
                    }}
                  >
                    {this.state.data.pound_stench}
                  </p>
                </div>
              </div>
            </Col>
            <Col md="4" xs="4">
              <div>
                <p
                  className="text-center"
                  style={{
                    fontColor: "#58585e",
                    fontSize: "14px",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                  }}
                >
                  จิ้งหรีดตาย
                </p>
                <div
                  className="border"
                  style={{
                    height: "35px",
                    borderRadius: "4px",
                    backgroundColor:
                      this.state.data.pound_dead === "ปกติ"
                        ? "#09c676"
                        : "#e34849",
                    color: "white",
                    textAlign: "center",
                    width: "100%",
                    marginTop: "-2px",
                  }}
                >
                  <p
                    style={{
                      fontColor: "white",
                      fontSize: "12px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      marginTop: "4px",
                      textAlign: "center",
                      lineHeight: "28px",
                    }}
                  >
                    {this.state.data.pound_dead}
                  </p>
                </div>
              </div>
            </Col>
            <Col md="4" xs="4">
              <div>
                <p
                  className="text-center"
                  style={{
                    fontColor: "#58585e",
                    fontSize: "14px",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                  }}
                >
                  ศัตรูจิ้งหรีด
                </p>
                <div
                  className="border"
                  style={{
                    height: "35px",
                    borderRadius: "4px",
                    backgroundColor:
                      this.state.data.pound_enemies === "ปกติ"
                        ? "#09c676"
                        : "#e34849",
                    color: "white",
                    textAlign: "center",
                    width: "100%",
                    marginTop: "-2px",
                  }}
                >
                  <p
                    style={{
                      fontColor: "white",
                      fontSize: "12px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      marginTop: "4px",
                      textAlign: "center",
                      lineHeight: "28px",
                    }}
                  >
                    {this.state.data.pound_enemies}
                  </p>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <div
              className="row mt-3 mb-5"
              style={{
                position: "relative",
                bottom: "0px",
                width: "90%",
                zIndex: "100",
                height: "50px",
                borderRadius: "12px",
                left: "10%",
              }}
            >
              <div
                className="col border-top border-left border-bottom"
                style={{
                  backgroundColor: "white",
                  padding: "0px 0px 0px 0px",
                  display: "relative",
                  borderTopLeftRadius: "12px",
                  borderBottomLeftRadius: "12px",
                  borderRight: "none",
                }}
              >
                <p
                  className="mt-2"
                  style={{
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "12px",
                  }}
                >
                  ปริมาณอาหาร
                </p>
                <p
                  style={{
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "12px",
                    marginTop: "-16px",
                  }}
                >
                  (สะสม)
                </p>
                <p
                  style={{
                    color: "#0dc5ca",
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "30px",
                    marginTop: "-8px",
                  }}
                >
                  {this.state.data.pound_foodc}
                </p>
              </div>
              <div
                className="col border"
                style={{
                  backgroundColor: "white",
                  padding: "0px 0px 0px 0px",
                  display: "relative",
                  borderTopRightRadius: "12px",
                  borderBottomRightRadius: "12px",
                }}
              >
                <p
                  className="mt-2"
                  style={{
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "12px",
                  }}
                >
                  ปริมาณน้ำ
                </p>
                <p
                  style={{
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "12px",
                    marginTop: "-16px",
                  }}
                >
                  (สะสม)
                </p>
                <p
                  style={{
                    color: "#0dc5ca",
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "30px",
                    marginTop: "-8px",
                  }}
                >
                  {this.state.data.pound_waterc}
                </p>
              </div>
            </div>
          </Row>
          {/* <Row>
          <div className="row mt-3 ml-4" style={{position: "relative"}}>
            <p className='mt-2' style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '14px'}}>ให้น้ำ</p>
          </div>
        </Row>
        <Row>
          <Form action="#" method="#" className="mt-2 ml-4">
            <FormGroup style={{display: "flex"}}>
              <Input className="mr-4" style={{height: "38px", width: "110%"}} placeholder="ใส่ปริมาณ(ม.ล.)" type="number" />
            <Button className="btn-round mt-0 ml-4" color="info" type="submit">
              ยืนยัน
            </Button>
            </FormGroup>
          </Form>
        </Row> */}
          {/* <Row>
          <div className="row ml-4" style={{position: "relative"}}>
            <p className='' style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '14px'}}>แอมโมเนีย</p>
          </div>
        </Row>
        <Row>
          <Form  className="mt-2 ml-4">
            <FormGroup style={{display: "flex"}}>
              <Input className="mr-4" 
              style={{height: "38px", width: "110%"}} 
              placeholder="ใส่ปริมาณ" 
              type="number" 
              defaultValue={this.state.amm} 
              value={this.state.amm} 
              name='ammno'
              onChange={this.handleChange}/>
            <Button className="btn-round mt-0 ml-4" color="info"  onClick={() => this.handleSubmit()}>
              ยืนยัน
            </Button>
            </FormGroup>
          </Form>
        </Row> */}
          {/* <Row>
          <div className="row ml-4" style={{position: "relative"}}>
            <p className='' style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '14px'}}>แอมโมเนีย</p>
          </div>
        </Row>
        <Row>
          <Form action="#" method="#" className="mt-2 ml-4">
            <Button style={{width: '270%'}} className="mt-0" color="info" type="submit">
              แจ้งพนักงาน
            </Button>
          </Form>
        </Row> */}
          <Row>
            <div className="row mt-3 ml-4" style={{ position: "relative" }}>
              <p
                className="mt-2"
                style={{
                  width: "100%",
                  textAlign: "center",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                  fontSize: "14px",
                }}
              >
                มีกลิ่นเน่า
              </p>
            </div>
          </Row>
          <Row className="ml-4 mt-2">
            <div className="row" style={{ width: "100%", height: "40px" }}>
              <div
                className="col"
                style={{
                  backgroundColor:
                    this.state.data.pound_stench === "ปกติ"
                      ? "#08d1c0"
                      : "white",
                  padding: "0px 0px 0px 0px",
                  display: "relative",
                  borderTopLeftRadius: "8px",
                  borderBottomLeftRadius: "8px",
                }}
              >
                <p
                  style={{
                    color:
                      this.state.data.pound_stench === "ปกติ"
                        ? "white"
                        : "gray",
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "16px",
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                  }}
                >
                  ไม่มีกลิ่นเน่า
                </p>
              </div>
              <div
                className="col"
                style={{
                  backgroundColor:
                    this.state.data.pound_stench === "ปกติ"
                      ? "white"
                      : "#f1b825",
                  padding: "0px 0px 0px 0px",
                  display: "relative",
                  border: "solid 1px lightgray",
                  borderLeft: "none",
                  borderTopRightRadius: "8px",
                  borderBottomRightRadius: "8px",
                }}
                onClick={() =>
                  this.setState(
                    (prevState) => ({
                      data: {
                        ...prevState.data,
                        pound_stench: "ไม่ปกติ",
                      },
                    }),
                    this.checkstatus
                  )
                }
              >
                <p
                  style={{
                    color:
                      this.state.data.pound_stench === "ปกติ"
                        ? "gray"
                        : "white",
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "16px",
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                  }}
                >
                  มีกลิ่นเน่า
                </p>
              </div>
            </div>
          </Row>
          <Row>
            <div className="row mt-3 ml-4" style={{ position: "relative" }}>
              <p
                className="mt-2"
                style={{
                  width: "100%",
                  textAlign: "center",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                  fontSize: "14px",
                }}
              >
                จิ้งหรีดตาย
              </p>
            </div>
          </Row>
          <Row className="ml-4 mt-2">
            <div className="row" style={{ width: "100%", height: "40px" }}>
              <div
                className="col"
                style={{
                  backgroundColor:
                    this.state.data.pound_dead === "ปกติ" ? "#08d1c0" : "white",
                  padding: "0px 0px 0px 0px",
                  display: "relative",
                  borderTopLeftRadius: "8px",
                  borderBottomLeftRadius: "8px",
                }}
              >
                <p
                  style={{
                    color:
                      this.state.data.pound_dead === "ปกติ" ? "white" : "gray",
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "16px",
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                  }}
                >
                  ไม่มีจิ้งหรีดตาย
                </p>
              </div>
              <div
                className="col"
                style={{
                  backgroundColor:
                    this.state.data.pound_dead === "ปกติ" ? "white" : "#f1b825",
                  padding: "0px 0px 0px 0px",
                  display: "relative",
                  border: "solid 1px lightgray",
                  borderLeft: "none",
                  borderTopRightRadius: "8px",
                  borderBottomRightRadius: "8px",
                }}
                onClick={() =>
                  this.setState(
                    (prevState) => ({
                      data: {
                        ...prevState.data,
                        pound_dead: "ไม่ปกติ",
                      },
                    }),
                    this.checkstatus
                  )
                }
              >
                <p
                  style={{
                    color:
                      this.state.data.pound_dead === "ปกติ" ? "gray" : "white",
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "16px",
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                  }}
                >
                  มีจิ้งหรีดตาย
                </p>
              </div>
            </div>
          </Row>
          <Row>
            <div className="row mt-3 ml-4" style={{ position: "relative" }}>
              <p
                className="mt-2"
                style={{
                  width: "100%",
                  textAlign: "center",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                  fontSize: "14px",
                }}
              >
                ศัตรูจิ้งหรีด
              </p>
            </div>
          </Row>
          <Row className="ml-4 mt-2">
            <div className="row" style={{ width: "100%", height: "40px" }}>
              <div
                className="col"
                style={{
                  backgroundColor:
                    this.state.data.pound_enemies === "ปกติ"
                      ? "#08d1c0"
                      : "white",
                  padding: "0px 0px 0px 0px",
                  display: "relative",
                  borderTopLeftRadius: "8px",
                  borderBottomLeftRadius: "8px",
                }}

                //onClick={()=> this.setState({ toggleEnemyReport: false}),this.Enemynormal}
              >
                <p
                  style={{
                    color:
                      this.state.data.pound_enemies === "ปกติ"
                        ? "white"
                        : "gray",
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "16px",
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                  }}
                >
                  ไม่พบศัตรู
                </p>
              </div>
              <div
                className="col"
                style={{
                  backgroundColor:
                    this.state.data.pound_enemies === "ปกติ"
                      ? "white"
                      : "#f1b825",
                  padding: "0px 0px 0px 0px",
                  display: "relative",
                  border: "solid 1px lightgray",
                  borderLeft: "none",
                  borderTopRightRadius: "8px",
                  borderBottomRightRadius: "8px",
                }}
                onClick={() =>
                  this.setState(
                    (prevState) => ({
                      data: {
                        ...prevState.data,
                        pound_enemies: "ไม่ปกติ",
                      },
                      toggleEnemyReport: !toggleEnemyReport,
                    }),
                    this.checkstatus
                  )
                }
              >
                <p
                  style={{
                    color:
                      this.state.data.pound_enemies === "ปกติ"
                        ? "gray"
                        : "white",
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "16px",
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                  }}
                >
                  พบศัตรู
                </p>
              </div>
            </div>
          </Row>
          <Collapse isOpen={toggleEnemyReport && enemyDisplay}>
            <Row className="mt-3">
              <div className="row ml-4" style={{ position: "relative" }}>
                <p
                  className=""
                  style={{
                    width: "100%",
                    textAlign: "center",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    fontSize: "14px",
                  }}
                >
                  ชนิดศัตรู
                </p>
              </div>
            </Row>
            <Row style={{ marginBottom: "300px" }} className="">
              <div className="w-100">
                <Form className="mt-6 ml-4  ">
                  <FormGroup style={{ display: "flex" }}>
                    <Select
                      size="3"
                      isSearchable={false}
                      className="mr-3 w-100"
                      classNamePrefix="react-select"
                      name="enemy"
                      value={
                        enemy ||
                        (enemyDisplay && enemyDisplay !== "0"
                          ? {
                              value: enemyDisplay,
                              label: this.getEnemyLabel(enemyDisplay),
                            }
                          : null)
                      }
                      onChange={(value) => this.setState({ enemy: value })}
                      options={[
                        {
                          value: "",
                          label: "ศัตรูจิ้งหรีด",
                          isDisabled: true,
                        },
                        { value: "2", label: "ไรขาว" },
                        { value: "3", label: "ไรแดง" },
                        { value: "4", label: "มด" },
                        { value: "5", label: "จิ้งจก" },
                        { value: "6", label: "แมลงสาบ" },
                        { value: "7", label: "แมงมุม" },
                        { value: "8", label: "นก" },
                        { value: "9", label: "อื่นๆ" },
                      ]}
                      placeholder="ระบุศัตรูจิ้งหรีด"
                    />
                    <Button
                      className="btn-round mt-0 ml-4"
                      color="info"
                      onClick={() => this.handleSubmit2()}
                    >
                      ยืนยัน
                    </Button>
                  </FormGroup>
                </Form>
                <img
                  style={{
                    display: "block",
                    marginLeft: "auto",
                    marginRight: "auto",
                  }}
                  className="mt-3"
                  height="40%"
                  width="40%"
                  src={ache}
                />
              </div>
            </Row>
          </Collapse>
          <AdminNavBar pageName={"บ่อเลี้ยง"} />

          <WorkerFooter />
        </div>
      </>
    );
  }
}

export default withRouter(WorkerInsectarium);
