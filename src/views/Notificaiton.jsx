import React, { useState, useEffect } from "react";
import {
    Badge,
    Button,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Row,
    Col,
    Collapse
  } from "reactstrap";

  import _ from 'lodash';

  const Notification = (props) => {
      const { pageName, poundsNoti, farmNoti, hatchNoti, boxNoti} = props

    //   const [data, setData] = useState([
    //       {
    //         insectariumId: 1,
    //         pondId: 4,
    //         issue: "จิ้งหรีดตาย"
    //     },
    //       {
    //         insectariumId: 3,
    //         // pondId: 7,
    //         issue: "แอมโมเนีย"
    //       }
    //   ])

    //   useEffect(() => {
    //       getNotifications()
    //   }, [])

    //   const getNotifications = () => {
    //       if (pageName === "Dashboard") {
    //           console.log('pageName', pageName);
    //           //axious blabla
    //       } else {
    //         console.log('pageName2', pageName);
    //           //axious blabla
    //       }
    //   } 

      const renderAbs = (datum) => {
        const abnormals = _.pickBy(datum, (value, key) => _.startsWith(value, "ไม่"))
        console.log('abnormals', abnormals);
        let absArr = []
        for (var name in abnormals) {
            if (name === "pound_dead") {absArr.push(<div>จิ้งหรีดตาย</div>)}
            else if (name === "pound_status" || name === "box_status") {absArr.push(<div className="font-weight-bold">ไม่ปกติ</div>) }
            else if (name === "pound_enemies" ||name === "box_enemies") {absArr.push(<div>พบศัตรูจิ้งหรีด</div>) }
            else if (name === "box_bac") {absArr.push(<div>พบเชื้อรา</div>) }
            else if (name === "box_coconut") {}
            else if (name === "insectarium_lossc") {absArr.push(<div>พบจิ้งหรีดหลุด</div>) }
            else if (name === "insectarium_status" || name === "egg_lossc") {absArr.push(<div className="font-weight-bold">ไม่ปกติ</div>) }
            else { absArr.push(<div>มีกลิ่นเน่า</div>) }
            
        }
        return absArr.map((absArr) => absArr)
      }

      const renderNotiList = () => {
          if (!poundsNoti && !farmNoti && !hatchNoti && !boxNoti) { return }
          const allNotis = poundsNoti.concat(farmNoti, hatchNoti, boxNoti)
        //   if (pageName === "Dashboard") {
            return allNotis.map((datum, index) => (
                <a href={datum.egg_id ? `/admin/hatchery?hatchery_id=${datum.egg_id}` : (datum.insectarium_id && !datum.pound_id) ? `/admin/farm?farm_id=${datum.insectarium_id}` : (datum.insectarium_id && datum.pound_id) ? `/admin/insectarium?pound_id=${datum.pound_id}&farm_id=${datum.insectarium_id}` : null}>
                    <Card key={index} id={index} className='mb-1'>
                        <CardBody>
                            <p style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontSize: '14px'}}>
                                {datum.egg_name ? `ห้องอบไข่ที่ ${datum.egg_name}` : null} {datum.box_id ? `กล่องที่ ${datum.box_name}` : null} {datum.insectarium_id ? `โรงเลี้ยงที่ ${datum.insectarium_name}` : null} {datum.pound_id ? `บ่อที่ ${datum.pound_name}` : null}
                            </p>
                            <div style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontSize: '14px'}} class="text-danger">{renderAbs(datum)}</div>
                        </CardBody>
                    </Card>
                </a>
            ))
        //   } else {
        //     return allNotis.map((datum, index) => (
        //         <Card key={index} id={index} className='mb-1'>
        //             <CardBody>
        //             <p style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontSize: '14px'}}>
        //                 อีก {datum.dateDiff}วัน ต้อง{datum.job} ที่โรงเลี้ยง {datum.insectariumId} {datum.pondId ? `บ่อที่ ${datum.pondId}` : null}
        //             </p>
        //             </CardBody>
        //         </Card>
        //     ))
        //   }
      }

      return (
       renderNotiList()
      )
  }

  export default Notification