import { cardSubtitle } from "assets/jss/material-kit-react";
import ache from "../assets/img/ache.png"
import React, { useEffect,useState } from "react";
import axios from 'axios'
import styled from "styled-components";
import Workerhomedetail from "./Workerhomedetail";
import WorkerHatchDetail from "./WorkerHatchDetail";
import AdminNavBar from "../components/Navbars/AdminNavbar"
import WorkerFooter from "../components/WorkerNavBarFooter/WorkerFooter"
import Cookies from "js-cookie";
import { withRouter } from "react-router-dom";
import _ from "lodash"
import {

  Row,
} from "reactstrap";
const InfoRow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
`;

const InfoContent = styled.div``;

const InfoDetail = styled.div`
  margin: 2% 0;
`;

const ContentWrapper = styled.div`
  display: flex;
  margin: 5% 0;
`;

const ContentText = styled.p`
  margin-left: 8px;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
`;

const handleColorType = (bgcolor) => {
  switch (bgcolor) {
    case "Bret":
      return "#03a9f3";
    case "Bruh":
      return "#f56342";
    default:
      return "#000";
  }
};
const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"

const WorkTodo = styled.div`
  width: 85%;
  height: auto;
  border-radius: 4px;
  background-color: ${({ bgcolor }) => handleColorType(bgcolor)};
  color: white;
  text-align: center;
`;

const WorkTodoText = styled.p`
  color: white;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
  font-weight: bold;
  margin-top: 4px;
`;

const workAPI = {
  id: 1,
  name: "Leanne Graham",
  username: "Bret",
  email: "Sincere@april.biz",
};

/*const insectariumAPI = [
  {
    id: 1,
    puddle: "โรงที่ 1",
  },
  { id: 2, puddle: "โรงที่ 2" },
  { id: 3, puddle: "โรงที่ 3" },
];*/

// const hatcheriesAPI = [
//   {
//     id: 1,
//     room: "ห้องอบไข่ที่ 1",
//   },
//   { id: 2, room: "ห้องอบไข่ที่ 2" },
//   { id: 3, room: "ห้องอบไข่ที่ 3" },
// ];


const percentage1 = 25;
const percentage2 = 5;
const percentage3 = 35;

const Workerhome = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [active, setActive] = useState(null);

  const toggle = (id) => {

    setIsOpen(!isOpen);
    setActive(id)
    
  }
  const [hatcheriesAPI, setegg]=useState(null)
  const [insectariumAPI, setAPI]=useState(null)
  const [conttype1, setCount1]=useState([])
  const [conttype2, setCount2]=useState([])
  const [conttype3, setCount3]=useState([])
  const [gettemp, settemp]=useState("")
  const [gethum, sethum]=useState("")
  const farmId = Cookies.get('farmId')
  const Allinsect = async () => {
    try {
  const Allinsect = await axios.get(`${URL}/allinsectarium/${farmId}`)
  console.log('Allinsect', Allinsect);
      setAPI(Allinsect.data);

    
    } catch (err) {
      console.error('jhgjhgjughjg',err.message);
    }
  };



  const Allegg = async () => {
    try {
  const Allegg = await axios.get(`${URL}/allegg/${farmId}`)
  console.log('Allegg', Allegg);
      setegg(Allegg.data);

    
    } catch (err) {
      console.error('jhgjhgjughjg',err.message);
    }
  };
  const gethummidity = async () => {
    try {
  const gethummidity = await axios.get(`${URL}/api/sendhumiinsect`)
     
      sethum(gethummidity.data.humirityinsect);
      console.log(gethummidity)
    
    } catch (err) {
      console.error(err.message);
    }
  };

  const gettemperature = async () => {
    try {
  const gettemperature = await axios.get(`${URL}/api/sendtempinsect`)
     
      settemp(gettemperature.data.temperatureinsect);
      console.log(gettemperature)
    
    } catch (err) {
      console.error(err.message);
    }
  };

  const countcrickettype1 = async () => {
    try {
  const countcrickettype1 = await axios.get(`${URL}/countcrickettypev2/1/${farmId}`)
     
      setCount1(countcrickettype1.data[0].count);
    
    } catch (err) {
      console.error(err.message);
    }
  };

  const countcrickettype2 = async () => {
    try {
  const countcrickettype2 = await axios.get(`${URL}/countcrickettypev2/2/${farmId}`)
     
      setCount2(countcrickettype2.data[0].count);
    
    } catch (err) {
      console.error(err.message);
    }
  };

  const countcrickettype3 = async () => {
    try {
  const countcrickettype3 = await axios.get(`${URL}/countcrickettypev2/3/${farmId}`)
     
      setCount3(countcrickettype3.data[0].count);
    
    } catch (err) {
      console.error(err.message);
    }
  };



  useEffect(()=>{
    Allinsect()
    Allegg()
    countcrickettype1()
    countcrickettype2()
    countcrickettype3()
    setTimeout(() => {
      Allegg()
      Allinsect()
      countcrickettype1()
      countcrickettype2()
      countcrickettype3()
    },3000)
  },[]);


  const renderJobCards = (cards) => {
    console.log('cards', cards);
    console.log('cards', insectariumAPI[0].insectarium_id);
      return cards.map((card, index) => (
        <Workerhomedetail
          percentage1={gettemp}
          percentage2={percentage2}
          percentage3={gethum}
          isOpen={isOpen}
          toggle={toggle}
          active={active}
          card={card}
          index={index}
          id={insectariumAPI[index].insectarium_id}
        />
        )
    )
  }

  const renderHatcheryCards = (cards) => {
    if(!cards){
      return
    }
      return cards.map((card, index) => (
        <WorkerHatchDetail
          percentage1={percentage1}
          percentage2={percentage2}
          card={card}
          index={index}
          id={hatcheriesAPI[index].egg_id}
        />
        )
    )
  }


  return (

    <div className="content" >
   
                    <div className="row mt-3" style={{backgroundColor: "white",  width: '110%', height: '100px'}}>
                      <div className="col border" style={{backgroundColor: "white", padding: '13px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '12px', borderBottomLeftRadius: '12px'}}>
                        <p className='mt-2' style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '25px'}}>สะดิ้ง</p>
                        <p style={{color: '#0dc5ca', width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '20px',marginTop: '-20px'}}>{conttype1} บ่อ</p>
                      </div>
                      <div className="col border" style={{backgroundColor: "white", padding: '13px 0px 0px 0px', display: 'relative', borderTopRightRadius: 'none', borderBottomRightRadius: 'none'}}>
                        <p className='mt-2' style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '25px'}}>ทองดำ</p>
                        <p style={{color: '#0dc5ca', width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '20px', marginTop: '-20px'}}>{conttype2} บ่อ</p>
                      </div>
                      <div className="col border" style={{backgroundColor: "white", padding: '13px 0px 0px 0px', display: 'relative', borderTopRightRadius: '12px', borderBottomRightRadius: '12px'}}>
                        <p className='mt-2' style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '25px'}}>ทองแดง</p>
                        <p style={{color: '#0dc5ca', width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '20px', marginTop: '-20px'}}>{conttype3} บ่อ</p>
                      </div>
                    
                    </div>

                    <div >

{renderHatcheryCards(hatcheriesAPI)}
{insectariumAPI ? renderJobCards(insectariumAPI) : null}
<AdminNavBar pageName={"Dashboard"}/>
                      <WorkerFooter insectariumId={Cookies.get('farmId')}/>
</div>
<img style={{display: "block", marginLeft: "auto", marginRight: "auto"}} className="mt-3" height="40%" width="40%" src={ache} />
    </div>
  );
};

export default withRouter(Workerhome);
