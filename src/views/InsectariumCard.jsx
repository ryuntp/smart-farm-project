import axios from "axios";

import React, { useEffect, useState } from "react";
// react plugin used to create charts
import { Line, Bar, Doughnut } from "react-chartjs-2";
// react plugin for creating vector maps
import { VectorMap } from "react-jvectormap";
import { Pie } from "react-chartjs-2";
import AdminFooter from "../components/AdminNavbarFooter/AdminFooter";
// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  Collapse,
  UncontrolledTooltip,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import Steps, { Step } from "rc-steps";
import {
  CircularProgressbar,
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import EditPondModal from "./EditPondModal";
import "react-circular-progressbar/dist/styles.css";

const URL = "https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com";

const handleGraphColorTemp = (percentage) => {
  if (percentage == 32) {
    return "#25f19b";
  } else if (percentage > 32 && percentage < 35) {
    return "#FF9900";
  } else if (percentage < 32 && percentage > 29) {
    return "#FF9900";
  } else if (percentage >= 35 || percentage <= 29) {
    return "#FF0000";
  }
};

const handleGraphColorAmm = (percentage) => {
  if (percentage < 10) {
    return "#25f19b";
  } else if (percentage == 10) {
    return "#FF9900";
  } else if (percentage > 10) {
    return "#FF0000";
  }
};

const handleGraphColorHum = (percentage) => {
  if (percentage <= 50) {
    return "#25f19b";
  } else if (percentage > 50 && percentage <= 60) {
    return "#FF9900";
  } else if (percentage > 60) {
    return "#FF0000";
  }
};

const InsectariumCard = (props) => {
  const { allpound, index, farmId } = props;
  const [hum, sethum] = useState("");
  const [temp, settemp] = useState("");
  const [modal, setModal] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setModal(!modal);
  const [species, setSpecies] = useState([
    { id: 1, name: "สะดิ้ง" },
    { id: 2, name: "ทองดำ" },
    { id: 3, name: "ทองแดง" },
  ]);

  // if (!allpound) { return null}
  // else{
  //     useEffect(()=>{
  //         gettemperature()
  //       });

  //       useEffect(()=>{
  //         gethummidity()
  //       });
  // }
  // useEffect(()=>{
  //     gettemperature()
  //   },[]);

  //   useEffect(()=>{
  //     gethummidity()
  //   },[]);

  const gethummidity = async () => {
    try {
      const gethummidity = await axios.get(`${URL}/api/sendhumipound`);

      sethum(gethummidity.data.humiritypound);
      //console.log(gethummidity)
    } catch (err) {
      console.error(err.message);
    }
  };

  const gettemperature = async () => {
    try {
      const gettemperature = await axios.get(`${URL}/api/sendtemppound`);

      settemp(gettemperature.data.temperaturepound);
      //console.log(gettemperature)
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <Col lg="3" md="6" sm="6" className="mt-2" key={index}>
      <Card className="card-stats">
        <CardBody>
          <Row onClick={() => setIsOpen(!isOpen)}>
            <Col xs="12">
              <div
                className="w-100"
                style={{ display: "flex", marginBottom: "-18px" }}
              >
                <p
                  className="ml-3"
                  style={{
                    fontColor: "white",
                    fontSize: "16px",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                  }}
                >
                  {" "}
                  บ่อ {allpound.pound_name}
                </p>
                <div
                  className="border"
                  style={{
                    height: "25px",
                    borderRadius: "4px",
                    backgroundColor:
                      allpound.pound_status === "ปกติ" ? "#09c676" : "#e34849",
                    color: "white",
                    textAlign: "center",
                    marginLeft: "8px",
                    width: "50px",
                    marginTop: "-2px",
                  }}
                >
                  <p
                    style={{
                      fontColor: "white",
                      fontSize: "12px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      marginTop: "4px",
                    }}
                  >
                    {allpound.pound_status}
                  </p>
                </div>
                <div className="mt-1 w-50 ml-3">
                  <i
                    className="nc-icon nc-minimal-down"
                    style={{
                      color: "black",
                      fontWeight: "bold",
                      float: "right",
                    }}
                    onClick={() => setIsOpen(!isOpen)}
                  />
                </div>
              </div>
              <hr />
            </Col>
          </Row>
          <Collapse isOpen={isOpen}>
            {/* <Row className="d-flex justify-content-center">
                        <div className="mb-3">
                        <Steps current={3}>
                            <Step icon={<i className={`nc-icon nc-check-2`} />}/>
                            <Step icon={<i className={`nc-icon nc-check-2`} />}/>
                            <Step icon={<i className={`nc-icon nc-check-2`} />}/>
                            <Step/>
                            <Step/>
                        </Steps>
                        </div>
                    </Row> */}
            <Row>
              <div
                className="mb-3 w-25 p-3 text-center"
                style={{
                  backgroundColor: "#e7e7e7",
                  borderRadius: "20px",
                  margin: "auto",
                  textAlignLast: "center",
                }}
              >
                <p
                  style={{
                    fontColor: "#000000",
                    fontSize: "12px",
                    fontFamily: "IBM Plex Sans Thai",
                  }}
                >
                  {allpound.pound_duration} วัน
                </p>
              </div>
            </Row>
            <Row className="mt-2 mb-4">
              <Col md="4" xs="4">
                <div>
                  <p
                    className="text-center"
                    style={{
                      fontColor: "#58585e",
                      fontSize: "14px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                    }}
                  >
                    มีกลิ่นเน่า
                  </p>
                  <div
                    className="border"
                    style={{
                      height: "35px",
                      borderRadius: "4px",
                      backgroundColor:
                        allpound.pound_stench === "ปกติ"
                          ? "#09c676"
                          : "#e34849",
                      color: "white",
                      textAlign: "center",
                      width: "100%",
                      marginTop: "-2px",
                    }}
                  >
                    <p
                      style={{
                        fontColor: "white",
                        fontSize: "12px",
                        fontFamily: "IBM Plex Sans Thai",
                        fontWeight: "bold",
                        marginTop: "4px",
                        textAlign: "center",
                        lineHeight: "28px",
                      }}
                    >
                      {allpound.pound_stench}
                    </p>
                  </div>
                </div>
              </Col>
              <Col md="4" xs="4">
                <div>
                  <p
                    className="text-center"
                    style={{
                      fontColor: "#58585e",
                      fontSize: "14px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                    }}
                  >
                    จิ้งหรีดตาย
                  </p>
                  <div
                    className="border"
                    style={{
                      height: "35px",
                      borderRadius: "4px",
                      backgroundColor:
                        allpound.pound_dead === "ปกติ" ? "#09c676" : "#e34849",
                      color: "white",
                      textAlign: "center",
                      width: "100%",
                      marginTop: "-2px",
                    }}
                  >
                    <p
                      style={{
                        fontColor: "white",
                        fontSize: "12px",
                        fontFamily: "IBM Plex Sans Thai",
                        fontWeight: "bold",
                        marginTop: "4px",
                        textAlign: "center",
                        lineHeight: "28px",
                      }}
                    >
                      {allpound.pound_dead}
                    </p>
                  </div>
                </div>
              </Col>
              <Col md="4" xs="4">
                <div>
                  <p
                    className="text-center"
                    style={{
                      fontColor: "#58585e",
                      fontSize: "14px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                    }}
                  >
                    ศัตรูจิ้งหรีด
                  </p>
                  <div
                    className="border"
                    style={{
                      height: "35px",
                      borderRadius: "4px",
                      backgroundColor:
                        allpound.pound_enemies === "ปกติ"
                          ? "#09c676"
                          : "#e34849",
                      color: "white",
                      textAlign: "center",
                      width: "100%",
                      marginTop: "-2px",
                    }}
                  >
                    <p
                      style={{
                        fontColor: "white",
                        fontSize: "12px",
                        fontFamily: "IBM Plex Sans Thai",
                        fontWeight: "bold",
                        marginTop: "4px",
                        textAlign: "center",
                        lineHeight: "28px",
                      }}
                    >
                      {allpound.pound_enemies}
                    </p>
                  </div>
                </div>
              </Col>
            </Row>
            {/* <Row>
              <Col md="4" xs="4">
                <div>
                  <p
                    className="text-center"
                    style={{
                      fontColor: "#58585e",
                      fontSize: "14px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                    }}
                  >
                    อุณหภูมิ
                  </p>
                  <CircularProgressbar
                    value={temp}
                    text={`${temp}°C`}
                    styles={buildStyles({
                      pathColor: handleGraphColorTemp(temp),
                    })}
                  />
                </div>
              </Col>
              <Col md="4" xs="4">
                <div>
                  <p
                    className="text-center"
                    style={{
                      fontColor: "#58585e",
                      fontSize: "14px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                    }}
                  >
                    แอมโมเนีย
                  </p>
                  <CircularProgressbar
                    value={allpound.pound_amm}
                    text={`${allpound.pound_amm}ppm`}
                    styles={buildStyles({
                      pathColor: handleGraphColorAmm(allpound.pound_amm),
                    })}
                  />
                </div>
              </Col>
              <Col md="4" xs="4">
                <div>
                  <p
                    className="text-center"
                    style={{
                      fontColor: "#58585e",
                      fontSize: "14px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                    }}
                  >
                    ความชื้น
                  </p>
                  <CircularProgressbar
                    value={hum}
                    text={`${hum}%`}
                    styles={buildStyles({
                      pathColor: handleGraphColorHum(hum),
                    })}
                  />
                </div>
              </Col>
            </Row> */}
            <Row>
              <div
                className="row mt-3"
                style={{
                  position: "relative",
                  bottom: "0px",
                  width: "90%",
                  zIndex: "100",
                  height: "50px",
                  borderRadius: "12px",
                  left: "10%",
                }}
              >
                <div
                  className="col border-top border-left border-bottom"
                  style={{
                    backgroundColor: "#white",
                    padding: "0px 0px 0px 0px",
                    display: "relative",
                    borderTopLeftRadius: "12px",
                    borderBottomLeftRadius: "12px",
                    borderRight: "none",
                  }}
                >
                  <p
                    className="mt-2"
                    style={{
                      width: "100%",
                      textAlign: "center",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      fontSize: "12px",
                    }}
                  >
                    ปริมาณอาหาร
                  </p>
                  <p
                    style={{
                      width: "100%",
                      textAlign: "center",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      fontSize: "12px",
                      marginTop: "-16px",
                    }}
                  >
                    (สะสม)
                  </p>
                  <p
                    style={{
                      color: "#0dc5ca",
                      width: "100%",
                      textAlign: "center",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      fontSize: "30px",
                      marginTop: "-8px",
                    }}
                  >
                    {allpound.pound_foodc}
                  </p>
                </div>
                <div
                  className="col border"
                  style={{
                    backgroundColor: "white",
                    padding: "0px 0px 0px 0px",
                    display: "relative",
                    borderTopRightRadius: "12px",
                    borderBottomRightRadius: "12px",
                  }}
                >
                  <p
                    className="mt-2"
                    style={{
                      width: "100%",
                      textAlign: "center",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      fontSize: "12px",
                    }}
                  >
                    ปริมาณน้ำ
                  </p>
                  <p
                    style={{
                      width: "100%",
                      textAlign: "center",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      fontSize: "12px",
                      marginTop: "-16px",
                    }}
                  >
                    (สะสม)
                  </p>
                  <p
                    style={{
                      color: "#0dc5ca",
                      width: "100%",
                      textAlign: "center",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      fontSize: "30px",
                      marginTop: "-8px",
                    }}
                  >
                    {allpound.pound_waterc}
                  </p>
                </div>
              </div>
            </Row>
            <Row className="mb-3" style={{ marginTop: "70px" }}>
              <Col md="6" xs="6" onClick={toggle}>
                <div
                  className=""
                  style={{
                    borderColor: "#0dc5ca",
                    borderRadius: "8px",
                    height: "40px",
                    border: "1px solid #0dc5ca",
                  }}
                >
                  <p
                    className="text-center"
                    style={{
                      color: "#0dc5ca",
                      fontSize: "14px",
                      fontFamily: "IBM Plex Sans Thai",
                      fontWeight: "bold",
                      lineHeight: "40px",
                    }}
                  >
                    แก้ไขบ่อ
                  </p>
                </div>
              </Col>
              <Col md="6" xs="6">
                <div
                  className="border"
                  style={{
                    backgroundColor: "black",
                    borderRadius: "8px",
                    height: "40px",
                  }}
                >
                  <a
                    href={
                      "/admin/insectarium?pound_id=" +
                      allpound.pound_id +
                      "&farm_id=" +
                      farmId
                    }
                  >
                    <p
                      className="text-center"
                      style={{
                        color: "white",
                        fontSize: "14px",
                        fontFamily: "IBM Plex Sans Thai",
                        fontWeight: "bold",
                        lineHeight: "40px",
                      }}
                    >
                      ดูรายละเอียด
                    </p>
                  </a>
                </div>
              </Col>
            </Row>
          </Collapse>
        </CardBody>
      </Card>
      <EditPondModal
        species={species}
        toggle={toggle}
        modal={modal}
        pondId={allpound.pound_id}
        farmId={farmId}
        setModal={setModal}
      />
    </Col>
  );
};

export default InsectariumCard;
