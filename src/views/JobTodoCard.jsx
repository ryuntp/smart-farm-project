import React, { useState,useEffect } from "react";
import axios from 'axios'
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { Card, CardBody, Row, Col } from "reactstrap";

const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
const handleColorType = (bgcolor) => {
  switch (bgcolor) {
    case "เสร็จแล้ว":
      return "#09c676";
    case "ยังไม่เสร็จ":
      return "#e34849";
    default:
      return "#000";
  }
};

const WorkTodo = styled.div`
  width: 50%;
  height: auto;
  border-radius: 4px;
  background-color: ${({ bgcolor }) => handleColorType(bgcolor)};
  color: white;
  text-align: center;
  margin-left: 6px;
`;

const WorkTodoText = styled.p`
  color: white;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
  font-weight: bold;
  margin-top: 4px;
`;

const WorkName = styled.p`
  color: #333e63;
  font-size: 20px;
  font-family: "IBM Plex Sans Thai";
  font-weight: bold;
  margin-top: 12px;
  margin-left: 8px;
`;

const WorkPlace = styled.div`
  margin-top: 2%;
`;

const ContentWrapper = styled.div`
  display: flex;
  margin: 2% 0;
`;

const ContentText = styled.p`
  margin-left: 8px;
  font-size: 14px;
  font-family: "IBM Plex Sans Thai";
`;

const JobTodoCard = ({ item, history, Date,index, workerId}) => {
  console.log(history);
  // const redirectHandler = () => {
  //   history.push({ pathname: "/admin/jobdetail", state: item });
  // };
  const [actname, setname]=useState("");
  const [both, setboth]= useState("")
  const [date,setdate] =useState("")

  

  const showwork = () =>{
    
    var datearray = Date.split("-");
    var newdate = datearray[1] + '-' + datearray[0] + '-' + datearray[2];
    setdate(newdate);

    if(item.assigned_insectarium === "0"){
      setboth(`ห้องอบไข่ ${item.egg_id} กล่อง ${renderPonds(item.box_id)}`)
    }
    else if(item.egg_id === "0"){
      setboth(`โรงเลี้ยง ${item.assigned_insectarium} บ่อ ${renderPonds(item.assigned_pound)}`)
    }
    else if(item.activity_id === "30"){
      setboth(`โรงเลี้ยง ${item.assigned_insectarium} บ่อ ${renderPonds(item.assigned_pound)} ไป ห้องอบไข่ ${item.egg_id} กล่อง ${renderPonds(item.box_id)} `)
    }
    else{
      setboth(`ห้องอบไข่ ${item.egg_id} กล่อง ${renderPonds(item.box_id)} ไป โรงเลี้ยง ${item.assigned_insectarium} บ่อ ${renderPonds(item.assigned_pound)}`)
    }
  }
  const activityname = async () => {

    try {
  const activityname = await axios.get(`${URL}/allact23/${item.activity_id}`)
      setname(activityname.data[0]);
  
    } catch (err) {
      console.error('ERROR ',err.message);
    }
  }



  const renderPonds = (assigned_pound) => {
    if (!assigned_pound) { return }
    return assigned_pound.map((pound) => (
        pound + " "
    ))
}

 
useEffect(() => {
  showwork()
  activityname()
  
  
},[])
  return (
    <a href={'jobdetail?card_id=' + item.assigned_id + '&worker_id=' + workerId}>
    <Card className="card-calendar" key={item.assigned_id}>
      <div className="row">
        <div
          className="col-10"
          style={{ padding: "2% 8%", borderRight: "1px solid #d3d3d3" }}
        >
          <WorkTodo bgcolor={item.assigned_status}>
            <WorkTodoText>{item.assigned_status}</WorkTodoText>
          </WorkTodo>
          <WorkName>{actname.activity_name}</WorkName>
          <WorkPlace>
            <ContentWrapper>
              <i
                style={{
                  fontWeight: "bold",
                  fontSize: "18px",
                  color: "#0dc5ca",
                  marginLeft: "8px",
                }}
                className={`nc-icon nc-pin-3`}
              />
              <ContentText>{both}</ContentText>
            </ContentWrapper>
            <ContentWrapper>
              <i
                style={{
                  fontWeight: "bold",
                  fontSize: "18px",
                  color: "#0dc5ca",
                  marginLeft: "8px",
                }}
                className={`far fa-calendar-alt`}
              />
              <ContentText>{date}</ContentText>
            </ContentWrapper>
            <ContentWrapper>
              <i
                style={{
                  fontWeight: "bold",
                  fontSize: "18px",
                  color: "#0dc5ca",
                  marginLeft: "8px",
                }}
                className={`far fa-clock`}
              />
              <ContentText>{item.assigned_timestart} - {item.assigned_timefin}</ContentText>
            </ContentWrapper>
          </WorkPlace>
        </div>
        <div
          className="col-2"
          style={{ display: "flex", alignItems: "center" }}
         
        >
          <i
            style={{
              fontWeight: "bold",
              fontSize: "18px",
              color: "#0dc5ca",
            }}
            className={`nc-icon nc-minimal-right`}
          />
        </div>
      </div>
    </Card>
    </a>
  );
};

export default withRouter(JobTodoCard);
