import React from "react";
// react plugin used to create datetimepicker
import axios from "axios"
import ReactDatetime from "react-datetime";
// react plugin that creates an input with badges
import TagsInput from "react-tagsinput";
// react plugin used to create DropdownMenu for selecting items
import Select from "react-select";
import AdminFooter from "../components/AdminNavbarFooter/AdminFooter"
// react plugin used to create switch buttons
import Switch from "react-bootstrap-switch";
import { Redirect } from 'react-router-dom';
// plugin that creates slider
import Slider from "nouislider";
import AdminNavBar from '../components/Navbars/AdminNavbar'
import { withRouter } from "react-router-dom";
import Cookies from "js-cookie";
// import circlePlusIcon from "../assets/img/circle-plus-icon.png"

import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    FormGroup,
    Progress,
    Row,
    Col,
    Label,
    Input,
    Button,
    CustomInput
  } from "reactstrap";

  class NewJobType extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        tagsinput: ["Amsterdam", "Washington", "Sydney", "Beijing"],
        adminWorkList: [],
        workerWorkList: [],
        adminInputs: [],
        workerInputs: [],
        workerCheckLists: [],
        adminWorkListCount: 1,
        workerWorkListCount: 1,
        adminWorkName: "",
        workerTypeCount: 1
      };
    }circlePlusIcon
    componentDidMount() {
    }

    addIndex = (index) => {
        return (
            index + 1
        )
    }

    onChangeAdminWorkInput = (e) => {
        let obj = {};
        obj[e.target.id] = e.target.value
        this.setState(obj);
    }

    onChangeWorkerWorkType = (type, id) => {
        let obj = {};
        obj[id] = type
        this.setState(obj);
    }

    renderAdminWorkForms = () => {
        const { adminWorkList } = this.state 
        if (adminWorkList.length === 0) { return }
        return (
            adminWorkList.map((form, index) => (
                <Row key={`form_` + index+1} id={`form_` + index+1}>
                    <Col className="mt-2">
                        <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>งานของผู้จัดการ</small>
                        <FormGroup className="mt-3">
                            <Input key={`input_` + index+1} id={`admin_input_` + this.addIndex(index)} placeholder="กรอกงานที่ผู้จัดการต้องทำ" type="input" onChange={e => this.onChangeAdminWorkInput(e)}/>
                        </FormGroup>
                    </Col>
                </Row>
            ))
        )
    }
    renderWorkerWorkForms = () => {
        const { workerWorkList } = this.state 
        if (workerWorkList.length === 0) { return }
        return (
            workerWorkList.map((form, index) => (
                <div key={`worker_form_` + index+1} id={`form_` + index+1}>
                    <Row>
                        <Col className="mt-2">
                            <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>งานของพนักงาน</small>
                            <FormGroup className="mt-3">
                                <Input key={`worker_input_` + index+1} id={`worker_input_` + this.addIndex(index)} placeholder="กรอกงานที่พนักงานต้องทำ" type="input" onChange={e => this.onChangeAdminWorkInput(e)}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <div className="form-check form-check-inline ml-4" style={{fontFamily: 'IBM Plex Sans Thai', fontSize: '14px', fontWeight: 'bold'}}>
                            <CustomInput className="mr-4" type="radio" key={`checkList_` + index+1} id={`checkList_` + this.addIndex(index)} name={`radio_` + index+1} label="เช็คลิสต์" onChange={e => this.onChangeWorkerWorkType("checkList", `type_${this.addIndex(index)}`)}/>
                            <CustomInput type="radio" key={`radio_` + index+1} id={`radio_` + this.addIndex(index)} name={`radio_` + index+1} label="ช่องกรอก" onChange={e => this.onChangeWorkerWorkType("input", `type_${this.addIndex(index)}`)}/>
                        </div>
                    </Row>
                </div>
            ))
        )
    }

    renderAddAdminWork = () => {
        const { adminWorkList, adminWorkListCount } = this.state
        let updatedWorkList = adminWorkList.concat(
            "1"
        )
        this.setState({adminWorkList: updatedWorkList, adminWorkListCount: adminWorkListCount + 1})
    }

    renderAddWorkerWork = () => {
        const { workerWorkList, workerWorkListCount, workerTypeCount } = this.state
        let updatedWorkList = workerWorkList.concat(
            "1"
        )
        this.setState({workerWorkList: updatedWorkList, workerWorkListCount: workerWorkListCount + 1, workerTypeCount: workerTypeCount + 1})
    }

    onSubmit = () => {
        const { adminWorkListCount, adminWorkName, workerWorkListCount, workerTypeCount } = this.state
        let adminWorks = []
        let workerWorks = []
        let typeInput = []
        for(let i=0; i < adminWorkListCount; i++){
            adminWorks.push(this.state[`admin_input_${i}`])
        }

        for(let w=0; w < workerWorkListCount; w++){
            workerWorks.push(this.state[`worker_input_${w}`])
        }

        for(let t=0; t < workerTypeCount; t++){
            typeInput.push(this.state[`type_${t}`])
        }
        console.log('adminWorks', adminWorks);
        console.log('workerWorks', workerWorks);
        console.log('typeInput', typeInput);
        console.log('adminWorkName', adminWorkName);

        const submittedData = {
            mantask: adminWorks,
            worktask: workerWorks,
            typeInput: typeInput,
            actname: adminWorkName,
            farmId: Cookies.get('farmId')
        }

        axios({
            method: 'post',
            url: 'https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com/addactv2',
            data: submittedData
          }).then(
              res => {
                  if (res.status === 200){
                      this.setState({statusResponse: 200})
                  }
                  else {
                      return
                  }
              }
          )
    }

    jobAssignment = () => { 
        // - you can now access this.props.history for navigation
        this.props.history.push({pathname: "/admin/job-assignment", state: this.state});
      };

    render() {
        console.log('this.state.statusResponse', this.state.statusResponse);
        if (this.state.statusResponse === 200) {
            return (
                <Redirect to="/admin/job-assignment" />
            )
        }
      return (
        <>
          <div className="content">
            <button
                className="btn btn-primary d-flex"
                onClick={this.jobAssignment}
                >
                <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
                <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>กลับหน้ามอบหมายงาน</p>
            </button>
            <Row>
                <Col className="mt-2">
                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>ชื่องาน</small>
                    <FormGroup className="mt-3">
                        <Input placeholder="กรอกชื่องาน" type="input" onChange={e => this.setState({adminWorkName: e.target.value})}/>
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col className="mt-2">
                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>งานของผู้จัดการ</small>
                    <FormGroup className="mt-3">
                        <Input key={`input_0`} id={`admin_input_0`} placeholder="กรอกงานที่ผู้จัดการต้องทำ" type="input" onChange={e => this.onChangeAdminWorkInput(e)}/>
                    </FormGroup>
                </Col>
            </Row>
            {this.renderAdminWorkForms()}
            <Row className="justify-content-center">
                <Button style={{border: 'dashed 1px gray', backgroundColor: 'white'}} className="w-75" onClick={() => this.renderAddAdminWork()}>
                    <div style={{fontFamily: 'IBM Plex Sans Thai', color: '#0dc5ca', fontSize: '14px', fontWeight: 'bold'}}>
                        <i class="fas fa-plus-circle fa-lg mr-2"></i>เพิ่มงานผู้จัดการ
                    </div>
                </Button>
            </Row>
            <Row>
                <Col className="mt-2">
                    <small style={{fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>งานของพนักงาน</small>
                    <FormGroup className="mt-3">
                        <Input key={`worker_input_0`} id={`worker_input_0`} placeholder="กรอกงานที่พนักงานต้องทำ" type="input" onChange={e => this.onChangeAdminWorkInput(e)}/>
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <div className="form-check form-check-inline ml-4" style={{fontFamily: 'IBM Plex Sans Thai', fontSize: '14px', fontWeight: 'bold'}}>
                    <CustomInput className="mr-4" type="radio" key={`radio_0`} id={`checkList_0`} name={`radio_0`} label="เช็คลิสต์" onChange={e => this.onChangeWorkerWorkType("checkList", "type_0")}/>
                    <CustomInput type="radio" key={`radio_0`} id={`radio_0`} name={`radio_0`}  label="ช่องกรอก" onChange={e => this.onChangeWorkerWorkType("input", "type_0")}/>
                </div>
            </Row>
            {this.renderWorkerWorkForms()}
            <Row className="justify-content-center">
                <Button style={{border: 'dashed 1px gray', backgroundColor: 'white'}} className="w-75"  onClick={() => this.renderAddWorkerWork()}>
                    <div style={{fontFamily: 'IBM Plex Sans Thai', color: '#0dc5ca', fontSize: '14px', fontWeight: 'bold'}}>
                        <i class="fas fa-plus-circle fa-lg mr-2"></i>เพิ่มงานพนักงาน
                    </div>
                </Button>
            </Row>
            <Row className='mb-3' style={{marginTop: '50px'}}>
                <Col md="12" xs="12" onClick={() => this.onSubmit()}>
                    <div className='d-flex justify-content-center' style={{borderColor: '#0dc5ca', borderRadius: '8px', height: '40px', border:'1px solid #0dc5ca', color:"#0dc5ca"}}>
                        <i style={{marginTop: '12px'}} className="far fa-save mr-2 fa-lg"></i>
                        <p className="text-center" style={{ fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', lineHeight: '40px'}}>บันทึก</p>
                    </div>
                </Col>
            </Row>
            <AdminNavBar pageName="เพิ่มงานใหม่"/>
            <AdminFooter active={true}/>
          </div>
        </>
      );
    }
  }
  
  export default withRouter(NewJobType);