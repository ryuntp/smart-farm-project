/*!

=========================================================
* Paper Dashboard PRO React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import axios from "axios"
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { NavLink } from "react-router-dom";
import logo from "assets/img/Artboard 15.png";
import bottom from "assets/img/bottom.png";
import Cookies from "js-cookie";
// import ache from "assets/img/ache.png";
import ache from "assets/img/Logo-achethai.png";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Label,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col,
  Row,
  Alert
} from "reactstrap";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      position : "",
      username:"",
      password:"",
      orgid:"0",
      link:"",
      farmId: null,
      status: null,
      workerType: null,
      falseInput: false
    }
    this.handleClick=this.handleClick.bind(this)
    this.handleChange=this.handleChange.bind(this)
  }
  handleClick = () =>{
    const datar = {username:this.state.email,pass:this.state.password
    }

    console.log('data', datar);

    this.setState({status: "Online"})

        axios({
        method: 'post',
        url: 'https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com/user',
        data: datar
      })
      .then(res => {
        console.log('res.data', res.data)
        if(res.data.code == "200"){
          this.setState({
            farmId: res.data.Data[0].farm_id,
            status: "Online",
            workerType: res.data.Data[0].worker_type,
            workerId: res.data.Data[0].worker_id
          })
          Cookies.set('farmId', res.data.Data[0].farm_id)
          Cookies.set('workerType', res.data.Data[0].worker_type, { expires: 1 })
          Cookies.set('workerId', res.data.Data[0].worker_id, { expires: 1 })
      // console.log(this.state.orgid)
        } else {
          this.setState({falseInput: true})
        }
      })

  }
  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })  

    console.log(this.state.email)
    
  }
  componentDidMount() {
    Cookies.remove('workerType')
    // document.body.classList.toggle("login-page");
  }
  componentWillUnmount() {
    // document.body.classList.toggle("login-page");
  }
  render() {
    const { farmId, status, workerType, falseInput, workerId } = this.state

    if(status === 'Online' && workerType === '2'){
      Cookies.set('workerType', workerType, { expires: 1 })
      return(
        window.location.href = `/admin/homedash?farm_id=${farmId}`
        // <Redirect to={`/admin/homedash?farm_id=${farmId}`} />
      );
    } else if (status === 'Online' && workerType === '1'){
      Cookies.set('workerType', workerType, { expires: 1 })
      Cookies.set('workerId', workerId, { expires: 1 })
      return(
        <Redirect to={`/admin/formbeforework`} />
      );
    }
    return (
      <div className="mt-5">
        <Container>
          <Row>
            <Col className="ml-auto mr-auto" lg="4" md="6">
              <img src={ache} alt="react-logo" width="auto" height="auto"/>
              <CardBody>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-single-02" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input 
                  name="email"
                  value={this.state.email}
                  placeholder="ชื่อผู้ใช้งาน" 
                  type="text" 
                  onChange={this.handleChange}/>
                </InputGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-key-25" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="password"
                    value={this.state.password}
                    placeholder="รหัสผ่าน"
                    type="password"
                    autoComplete="off"
                    onChange={this.handleChange}
                  />
                </InputGroup>
                <Alert color="danger" style={{display: falseInput ? "flex" : "none" }}>
                  ชื่อผู้ใช้งาน/รหัสผ่านผิด
                </Alert>
              </CardBody>
                <div className="text-center">
                  <button type="button" style={{backgroundColor: "black"}} className="btn w-75" onClick={() => this.handleClick()}>เข้าสู่ระบบ</button>
                </div>
                   
            </Col>
         <img style={{position: "fixed", bottom: "0", left: '0'}} src={bottom} alt="react-logo" width="110%" height="auto"/>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
