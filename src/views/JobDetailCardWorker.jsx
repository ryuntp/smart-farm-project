import React, { useState,useEffect } from "react";
import { Redirect } from "react-router-dom";
import axios from 'axios'
import Cookies from 'js-cookie'
import WorkerFooter from "../components/WorkerNavBarFooter/WorkerFooter"
import {
  Alert,
  Label,
  Card,
  CardBody,
  Row,
  Col,
  FormGroup,
  Input,
  Collapse,
  Modal, 
  ModalHeader, 
  ModalBody,
  
} from "reactstrap";

import {
  CircularProgressbar,
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
//const URL ="http://localhost:8080"
 const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
const percentage1 = 25;
const percentage2 = 88;


const handleGraphColorTemp = (percentage) => {
  if (percentage == 32) {
    return "#25f19b";
  } else if (percentage > 32 && percentage < 35) {
    return "#FF9900";
  } else if (percentage < 32 && percentage > 29) {
    return "#FF9900";
  } else if (percentage >= 35 || percentage <= 29) {
    return "#FF0000";
  }
};

const handleGraphColorAmm = (percentage) => {
  if (percentage < 10) {
    return "#25f19b";
  } else if (percentage == 10) {
    return "#FF9900";
  } else if (percentage > 10) {
    return "#FF0000";
  }
};

const handleGraphColorHum = (percentage) => {
  if (percentage <= 50) {
    return "#25f19b";
  } else if (percentage > 50 && percentage <= 60) {
    return "#FF9900";
  } else if (percentage > 60) {
    return "#FF0000";
  }
};

const JobDetailCardWorker = ({ item, disable,index,id, detail}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [input, settaskinput] = useState([]);
  const [check, settaskcheck] = useState([]);
  const [newinput, setnewinput] = useState([])
  const [newcheck, setnewcheck] = useState([])
  const [valuesubmit, setvalue] = useState()
  const [notComplete, setComplete] = useState(false)
  const [Complete, setpost] = useState(false)
  const [success, setfin] = useState("none")
  const [show,setshow] = useState ("")
  const [modal, setModal] = useState(false)
  const [pondnotfin, setpondnotfin] = useState([])
  const [submitLoading, setSubmitLoading] = useState(false)
  // const [checkedItems, setCheckedItems] = useState([]);
  // const [checkedItems2, setCheckedItems2] = useState([]);
  const [num , setnum] = useState("")
  const [onlynum ,setonlynum]=useState(false)
  // const [isCheckAll, setIsCheckAll] = useState();
  // const [isCheck, setIsCheck] = useState([]);

  const toggle = () => setIsOpen(!isOpen);

  const showheadwork = () =>{
    if(item.insectarium_id === "0"){
      setshow(`กล่อง ${item.box_id}`)
      setnum("0")
    }
    else if(item.egg_id === "0"){
      setshow(`บ่อ ${item.pound_id}`)
      setnum("1")
    }
    else if(item.activity_id === "30"){
      setshow(`บ่อ ${item.pound_id} ไป กล่อง ${item.box_id} `)
      setnum("2")
    }
    else{
      setshow(`กล่อง ${item.box_id} ไป บ่อ ${item.pound_id}`)
      setnum("3")
    }
    if (item.activity_id === "24" || item.activity_id === "27"){
      setonlynum(true)
    }
  }
  
  const taskinput = async () => {
    try {
  const taskinput = await axios.get(`${URL}/wstaskinput/${item.activity_id}`)
      //console.log(taskinput.data)
      await settaskinput(taskinput.data);
    
    
    } catch (err) {
      //console.error('ERROR ',err.message);
    }
    
  };



  const taskcheck = async () => {
    try {
  const taskcheck = await axios.get(`${URL}/wstaskcheck/${item.activity_id}`)
      // console.log(taskcheck.data)
      await settaskcheck(taskcheck.data);
    
    
    } catch (err) {
      //console.error('ERROR ',err.message);
    }
    // const alltask = check.concat(input)
    // setvalue(alltask)
    // console.log(alltask)
  };

    const handleChange1  = async (e) => {
    console.log(e.target.value);
    console.log('property name: '+ e.target.id);
    let newArr = [...input]
    newArr[e.target.id].workertask_value = e.target.value
    await setnewinput(newArr)

    await console.log("setstate1",newinput)
    
  }

    const Checkclick  = (e) => {
    // console.log('index: ' + index);
    console.log('property name: '+ e.currentTarget.id);
    let newArr = [...check]
    if(newArr[e.currentTarget.id].workertask_value === null || newArr[e.currentTarget.id].workertask_value === "1"){
      newArr[e.currentTarget.id].workertask_value = "0"
      setvalue("1")
      setnewcheck(newArr)
     setnewcheck((state)=>{
       console.log(state);
       console.log("Update",newcheck)
       return state;
     })
    }
    else{
      newArr[e.currentTarget.id].workertask_value = "1"
      setvalue("0")
      setnewcheck(newArr)
     setnewcheck((state)=>{
       console.log(state);
       console.log("Update",newcheck)
       return state;
     })
    }
    // newArr[e.currentTarget.id].workertask_value = "1"
    // setvalue("1")
    console.log(valuesubmit)
     
    // if(newArr[e.target.id].workertask_value = "1"){
    //   newArr[e.target.id].workertask_value = "0"
      
    // }
    //   else if(){
    //     newArr[e.target.id].workertask_value = "1"
    //   }
    console.log("setstate2", newcheck)
    
  }


  const getpondnotfin = async () => {
    try {
  const getpondnotfin = await axios.get(`${URL}/showsubwtasknotfin/${item.assigned_id}`)
      // console.log(taskcheck.data)
      await setpondnotfin(getpondnotfin.data)
      
    
    
    } catch (err) {
      //console.error('ERROR ',err.message);
    }
    // const alltask = check.concat(input)
    // setvalue(alltask)
    // console.log(alltask)
  };

  const addmoretask = () =>{
    let value = null
    const alltask = newinput.concat(newcheck)
    const checktrue = input.concat(check)
    console.log("alltask",alltask)
    console.log("checktrue",checktrue)
    
    for(var i=0 ; i < checktrue.length;i++){
      if((alltask.length === checktrue.length && Object.keys(checktrue[i]).length === 5 &&
          alltask[i].workertask_value != "0" && alltask[i].workertask_value != "") || (checktrue.length === 0))
          {
        console.log(Object.keys(alltask[i]).length)
        value = "0"
        //setpost(true)
        //setComplete(false)
        console.log("0")
      }
      
      else{
        value = "1"
        setpost(false)
        setComplete(true)
        console.log("1")
        break
        

      }
    }

    if((value === "0" && alltask.length === checktrue.length ) || (checktrue.length === 0)){
      setModal(!modal)
      getpondnotfin()
    }
    console.log(pondnotfin)
  }



  const handleSubmit =  () =>{
    let value = null
    const alltask = newinput.concat(newcheck)
    const checktrue = input.concat(check)
    console.log("alltask",alltask)
    console.log("checktrue",checktrue)
    setSubmitLoading(true)
    for(var i=0 ; i < checktrue.length;i++){
      if((alltask.length === checktrue.length && Object.keys(checktrue[i]).length === 5 &&
          alltask[i].workertask_value != "0" && alltask[i].workertask_value != "") || (checktrue.length === 0))
          {
        console.log(Object.keys(alltask[i]).length)
        value = "0"
        setpost(true)
        setComplete(false)
        console.log("0")
      }
      
      else{
        value = "1"
        setpost(false)
        setComplete(true)
        console.log("1")
        break
        

      }
    }
    
    if((value === "0" && alltask.length === checktrue.length ) || (checktrue.length === 0)){
      console.log(pondnotfin)
      axios({
        method: 'post',
        url: `${URL}/updatewsubtaskstatusandval`,
        data: {
          alltask,
          workerType: Cookies.get('workerType'),
          workerId:Cookies.get('workerId'),
          assigned_id: item.assigned_id, 
          pound_id: item.pound_id, 
          insectarium_id: item.insectarium_id ,
          box_id:item.box_id,
          egg_id:item.egg_id

        }
    }
    )
    .then(res => {
      setSubmitLoading(false)
      console.log('res.data', res)
    })
    console.log({
      alltask,
      workerType: Cookies.get('workerType'),
      workerId:Cookies.get('workerId'),
      assigned_id: item.assigned_id, 
      pound_id: detail.assigned_pound[index], 
      insectarium_id: detail.assigned_insectarium ,
      box_id:detail.box_id[index],
      egg_id:detail.egg_id
    })
    console.error('success');
    
    
    }
};




  const renderInput = (input) => {
    if (!input) { return }
    return input.map((task,index) => (
        <Row className="mt-3">
            <Col md={12} xs={12}>
              <small
                style={{
                  fontSize: "14px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                }}
              >
                {task.workertask_name}
              </small>
            </Col>
            <Col md={12} xs={12} key={index}>
              <FormGroup className="mt-3">
                
                <Input 
                placeholder={item.subwtask_val === null ? "กรอกรายละเอียด" : item.subwtask_val[index]} 
                type={onlynum === true ? "number" : "type"}
                name="workertask_value"
                id ={index}
                value = {task.workertask_value}
                disabled ={item.subwtask_val === null ? false : true}
                onChange={async (e) => await handleChange1(e)}
                 />
                 
              </FormGroup>
            </Col>
          </Row>
    ))
  }


  const renderCheck = (check) => {
    if (!check) { return }
    return check.map((task , index) => (
        <Row className="mt-3">
            <Col md={12} xs={12}>
              <small
                style={{
                  fontSize: "14px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                }}
              >
                {task.workertask_name}
              </small>
            </Col>
            <Col md={12} xs={12}>
              <FormGroup className="mt-3" key={index}
              style={{
                marginLeft: "20px"
              }}>
                <Label check>
                  <Input 
                  type="checkbox"
                  id ={index}
                  onClick = {(e) => Checkclick(e)}
                  defaultChecked={ item.subwtask_val === null ? false : true}
                  disabled ={item.subwtask_val === null ? false : true}
                   />
                    <span className="form-check-sign" />
                      ทำแล้ว
                </Label>
              </FormGroup>
            </Col>
          </Row>
    ))
  }

  const Subtasksuccess = () => {
    
  
      if(item.subwtask_status === "เสร็จแล้ว"){
        setfin("block");
      }
      else{
        setfin("none");
      }
   
    
  };






  const Addmorepond = () =>{
    if (!pondnotfin) { return }
    else if (num ==="0"){
      return   pondnotfin.map((item,index) => (
                <Row>
                  <label key={item} style={{color:"black", fontFamily: 'IBM Plex Sans Thai',fontSize: "15px", fontWeight: 'bold'}}>
                      {" กล่อง "}{item.box_id}
                        
                  </label>
                  <Input type="checkbox" name={item.pound_id} value={item.box_id} checked={item.ischeck} onClick={handleonChange}/>
                </Row>
              ))
    }
    else if (num ==="1"){
      return   pondnotfin.map((item,index) => (
                <Row>
                  <label key={item} style={{color:"black", fontFamily: 'IBM Plex Sans Thai',fontSize: "15px", fontWeight: 'bold'}}>
                      {" บ่อ "}{item.pound_id}
                        
                  </label>
                  <Input type="checkbox" name={item.pound_id} value={item.box_id} checked={item.ischeck} onClick={handleonChange}/>
                </Row>
              ))
    }
    else if (num ==="2"){
      return   pondnotfin.map((item,index) => (
                <Row>
                  <label key={item} style={{color:"black", fontFamily: 'IBM Plex Sans Thai',fontSize: "15px", fontWeight: 'bold'}}>
                      {" บ่อ "}{item.pound_id}{" ไป กล่อง "}{item.box_id}
                        
                  </label>
                  <Input type="checkbox" name={item.pound_id} value={item.box_id} checked={item.ischeck} onClick={handleonChange}/>
                </Row>
              ))
    }
    else if (num ==="3"){
      return   pondnotfin.map((item,index) => (
                <Row>
                  <label key={item} style={{color:"black", fontFamily: 'IBM Plex Sans Thai',fontSize: "15px", fontWeight: 'bold'}}>
                      {" กล่อง "}{item.box_id}{" ไป บ่อ "}{item.pound_id}
                        
                  </label>
                  <Input type="checkbox" name={item.pound_id} value={item.box_id} checked={item.ischeck} onClick={handleonChange}/>
                </Row>
              ))
    }
  }



  const handleSelectAll = (event) => {
    // if(isCheckAll === true){
    //   setIsCheckAll(false);
    //   setCheckedItems(null);
    //   setCheckedItems2(null);
    // }
    // else if(isCheckAll === false || isCheckAll === null){
    //   setIsCheckAll(true);
    //   pondnotfin.forEach(item =>{
    //     setCheckedItems(prevcheckedItems =>({...prevcheckedItems, [item.pound_id] : true }))
    //     setCheckedItems2(prevcheckedItems2 =>({...prevcheckedItems2, [item.box_id] : true }))
    //   })
      
    // }
    // console.log(checkedItems)
    // console.log(checkedItems2)
    // console.log(isCheckAll)
    // // setIsCheck(list.map(li => li.id));
    // if (isCheckAll) {
    //   setIsCheck([]);
    // }

    let notfin = pondnotfin
    notfin.forEach(notfin => notfin.ischeck = event.target.checked) 
    setpondnotfin(notfin)
  };



  const handleonChange = (event) => {
    // setIsCheckAll(null)
    // // updating an object instead of a Map
    // setCheckedItems({...checkedItems, [event.target.name] : event.target.checked });
    // setCheckedItems2({...checkedItems2, [event.target.value] : event.target.checked });
    // console.log(checkedItems)
    // console.log(checkedItems2)
    let notfin = pondnotfin
    notfin.forEach(notfin => {
       if (notfin.pound_id === event.target.name && notfin.box_id === event.target.value)
          notfin.ischeck = event.target.checked
    })
    setpondnotfin(notfin)
  }

  const handleSubmit2 =  () =>{
    const alltask = newinput.concat(newcheck)
    console.log("alltask",alltask)
      axios({
        method: 'post',
        url: `${URL}/updatetaskfin2`,
        data: {
          alltask,
          workerType: Cookies.get('workerType'),
          workerId:Cookies.get('workerId'),
          assigned_id: item.assigned_id,  
          insectarium_id: item.insectarium_id ,
          egg_id:item.egg_id,
          moretask:pondnotfin
        }
    }
    )
    .then(res => {
      console.log('res.data', res)
    })
    console.log({
          alltask,
          workerType: Cookies.get('workerType'),
          workerId:Cookies.get('workerId'),
          assigned_id: item.assigned_id,  
          insectarium_id: item.insectarium_id ,
          egg_id:item.egg_id,
          moretask:pondnotfin
    })
    console.error('success');
    setModal(!modal)
    
    }

  useEffect(() => {
    taskinput()
    taskcheck()
    Subtasksuccess()
    showheadwork()
    getpondnotfin()
    
  },[])



  return (
    <Card className="card-stats mt-2">
      <CardBody>
        <Row onClick={toggle}>
          <Col xs="12">
            <div
            
              className="w-100"
              style={{
                display: "flex",
                flexWrap: "wrap",
                marginBottom: "-18px",
                justifyContent: "space-between",
              }}
            >
              <p
                className="ml-3"
                style={{
                  fontColor: "white",
                  fontSize: "16px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                }}
              >
                {show}
              </p>
              <div
                className="border"
                style={{
                  height: "25px",
                  borderRadius: "4px",
                  backgroundColor: item.subwtask_status === "เสร็จแล้ว" ? "#09c676" : "#e34849",
                  color: "white",
                  textAlign: "center",
                  marginLeft: "8px",
                  width: item.subwtask_status === "เสร็จ" ? "50px" : "70px",
                  margin: "-2px 0 0 auto",
                }}
              >
                <p
                  style={{
                    fontColor: "white",
                    fontSize: "12px",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                    marginTop: "4px",
                  }}
                >
                  {item.subwtask_status}
                </p>
              </div>
              <div className="mt-1 ml-3">
                <i
                  className="nc-icon nc-minimal-down"
                  style={{
                    color: "black",
                    fontWeight: "bold",
                    float: "right",
                  }}
                />
              </div>
            </div>
            <hr />
          </Col>
        </Row>
        <Collapse isOpen={isOpen}>
        
          {renderInput(input)}
          {renderCheck(check)}
          
          <Alert color="danger" style={{display: notComplete ? "flex" : "none" }}>
            กรุณากรอกรายละเอียดงานให้ครบถ้วน
          </Alert>
          <Alert color="success" style={{display: Complete ?  "flex" : "none" }}>
            กรอกงานสำเร็จ
          </Alert>
         
            <Row className='mb-3' style={{marginTop: '10px'}}>
                <Col md="6" xs="6" className="text-right text-md-left">
                <div className='border' 
                  style={{backgroundColor: '#1e90ff', 
                  borderRadius: '8px', 
                  height: '40px',
                  display: item.subwtask_status === "เสร็จแล้ว" ? "none" : "block"
                }}
                  onClick={()=> addmoretask()}

                  >
                    {/* <a href={'jobdetail?card_id=' + id}> */}
                      <p className="text-center" style={{color:"white", fontSize:"18px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', lineHeight: '40px'}}>เพิ่มงานยืนยัน</p>
                    {/* </a> */}
                              
                  </div>
                </Col>

                 <Modal isOpen={modal} style={{top: 'auto'}} className="custom-modal-style h-25">
                <ModalHeader className="">
                  <div className="d-flex justify-content-center">
                    <p className="text-center" style={{color:"black", fontFamily: 'IBM Plex Sans Thai',fontSize: "20px", fontWeight: 'bold'}}> เลือกงานที่จะยืนยันเพิ่ม </p>
                    
                    <i style={{position: "absolute", right: '5%'}} className="far fa-times-circle fa-2x text-danger" onClick={() => setModal(!modal)}></i>
                  </div>
                    <p className="text-center" style={{color:"red", fontFamily: 'IBM Plex Sans Thai',fontSize: "12px", fontWeight: 'bold'}}> **งานที่เลือกเพื่อที่จะยืนยัน จะมีข้อมูลเหมือนกับที่กรอกทั้งหมด** </p>
                </ModalHeader>
                    <ModalBody>
                      
                          <Row>
                            <label style={{color:"black", fontFamily: 'IBM Plex Sans Thai',fontSize: "15px", fontWeight: 'bold'}}>
                                {"เลือกทั้งหมด"}
                                  
                            </label>
                            <Input 
                              type="checkbox" 
                              name="selectAll"       
                              id="selectAll"       
                              onClick={handleSelectAll}
                              />
                          </Row>
                    <div>
                            </div>
                             {Addmorepond(pondnotfin)}
                            <Row className='mb-3' style={{marginTop: '10px'}}>
                                <Col md="6" xs="6" className="text-right text-md-left">
                                    <div className='border' 
                                      style={{backgroundColor: '#e34849', 
                                      borderRadius: '8px', 
                                      height: '40px',
                                      display: item.subwtask_status === "เสร็จแล้ว" ? "none" : "block"
                                    }}
                                      onClick={()=> setModal(!modal)}

                                      >
                                        {/* <a href={'jobdetail?card_id=' + id}> */}
                                          <p className="text-center" style={{color:"white", fontSize:"18px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', lineHeight: '40px'}}>ยกเลิก</p>
                                        {/* </a> */}
                                                  
                                      </div>
                                </Col>
                                 <Col md="6" xs="6" className="text-right text-md-right" >
                                    <div className='border' 
                                    style={{backgroundColor: '#09c676', 
                                    borderRadius: '8px', 
                                    height: '40px',
                                    display: item.subwtask_status === "เสร็จแล้ว" ? "none" : "block"
                                  }}
                                    onClick ={()=>handleSubmit2()}

                                    >
                                      {/* <a href={'jobdetail?card_id=' + id}> */}
                                        <p className="text-center" style={{color:"white", fontSize:"18px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', lineHeight: '40px'}}>ยืนยันทั้งหมด</p>
                                      {/* </a> */}
                                                
                                    </div>
                                  </Col>
                             </Row>
                    </ModalBody>
                </Modal>
                
                <Col md="6" xs="6" className="text-right text-md-right" >
                  <div className='border' 
                  style={{backgroundColor: '#09c676', 
                  borderRadius: '8px', 
                  height: '40px',
                  display: item.subwtask_status === "เสร็จแล้ว" ? "none" : "block"
                }}
                  onClick ={()=>handleSubmit()}

                  >
                    {/* <a href={'jobdetail?card_id=' + id}> */}
                      <p className="text-center" style={{color:"white", fontSize:"18px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', lineHeight: '40px'}}>ยืนยัน</p>
                    {/* </a> */}
                              
                  </div>
                </Col>
            </Row>
            <Row className='mb-3' style={{marginTop: '10px'}}>
                        <Col md="6" xs="6" className="text-right text-md-right">
                        </Col>
                        <Col md="6" xs="6" className="text-right text-md-right" >
                          <div 
                          className='border' 
                          style={{
                            backgroundColor: '#09c676', 
                            borderRadius: '8px', 
                            height: '40px',
                            display: success }}
                          >
                             
                                  <p className="text-center" style={{color:"white", fontSize:"18px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', lineHeight: '40px'}}>งานเสร็จแล้ว</p>
                              
                              
                          </div>
                        </Col>
                    </Row>
        </Collapse>
      </CardBody>
      {submitLoading ? (
              <button class="btn btn-primary" style={{height: "20%", position: "fixed", top: "50%", left: "50%", width: "50%", transform: "translate(-50%, -50%)", zIndex: 200 }} type="button" disabled>
                <span class="spinner-border spinner-border-sm" style={{fontSize: "160px"}} role="status" aria-hidden="true"></span>
                <span class="sr-only">Loading...</span>
            </button>
          ) : null}
    </Card>
    
  );
};

export default JobDetailCardWorker;
