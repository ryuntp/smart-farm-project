import React,{useState,useEffect} from "react";
import styled from "styled-components";
import JobTodoCard from "./JobTodoCard";
import WorkerFooter from "../components/WorkerNavBarFooter/WorkerFooter";
import AdminNavBar from "../components/Navbars/AdminNavbar";
import axios from "axios";
import { withRouter } from "react-router-dom";
import Cookies from "js-cookie";


const DateWrapper = styled.div`
  margin: 3% 0;
  font-size: 4vw;
`;
const JobWrapper = styled.div``;

const dumbAPI = [
  {
    id: 1,
    name: "Leanne Graham",
    username: "Bret",
    email: "Sincere@april.biz",
  },
  {
    id: 2,
    name: "John Graham",
    username: "Bruh",
    email: "Sincere@april.biz",
  },
  {
    id: 3,
    name: "Sarah Graham",
    username: "Bro",
    email: "Sincere@april.biz",
  },
];
const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
var ids;

class JobTodo extends React.Component {
  constructor(props) {
    const currentDate = new Date()
    super(props);
    this.state = {
      Date : currentDate.getDate()+"/"+(currentDate.getMonth()+1)+"/"+currentDate.getFullYear(),
      DateShow :('0'+currentDate.getDate()).slice(-2)+"-"+('0'+(currentDate.getMonth()+1)).slice(-2)+"-"+currentDate.getFullYear(),
      jobdo : [],
      params:""
      
    };
    // this.sendtime = this.sendtime.bind(this);
  }
  callbackFunction = (childData) => {
    var query = new URLSearchParams(this.props.location.search);
    var params = query.get('worker_id');
    const data = ('0'+(childData.getMonth()+1)).slice(-2)+"-"+('0'+childData.getDate()).slice(-2)+"-"+childData.getFullYear()
    const datashow = ('0'+childData.getDate()).slice(-2)+"-"+('0'+(childData.getMonth()+1)).slice(-2)+"-"+childData.getFullYear()
    this.setState({Date: data})
    this.setState({DateShow: datashow})
    axios.get(`${URL}/allactwside/${this.state.Date}/${params}`,{params: this.state.Date})
    .then(res => {
       console.log(res.data)
       this.setState({jobdo: []})
       this.setState({
        jobdo:res.data
      })

    })
}

componentDidMount(){
  var query = new URLSearchParams(this.props.location.search);
  var params = query.get('worker_id');
  this.setState({workerId: params})
  const farmId = Cookies.get('farmId')
  this.setState({farmId: farmId})
  const currentDate = new Date()
  const data = ('0'+(currentDate.getMonth()+1)).slice(-2)+"-"+('0'+currentDate.getDate()).slice(-2)+"-"+currentDate.getFullYear()
  this.setState({
    Date:data
  })
 axios.get(`${URL}/allactwside/${data}/${params}`)
  .then(res => {
     console.log(res.data)
     this.setState({
      jobdo:res.data,
    })
    console.log(this.state.jobdo)
  })
}

dashboard = () => { 
  // - you can now access this.props.history for navigation
  this.props.history.push({pathname: "/admin/workerhome", search: '?farm_id=' + this.state.farmId});
};
  


  render() {
    console.log('this.state.workerId', this.state.workerId)

  return (
    <div className="content">
      <button
          className="btn btn-primary d-flex mb-5"
          onClick={this.dashboard}
          >
            <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
            <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>กลับหน้าแดชบอร์ด</p>
        </button>
      <AdminNavBar pageName="งานที่ต้องทำ" parentCallback = {this.callbackFunction}/>
      <WorkerFooter active={true}/>
      <DateWrapper>
        <p>
          งานวันที่ <b>{this.state.DateShow}</b>
        </p>
      </DateWrapper>
      <JobWrapper>
        {this.state.jobdo.map((item,index) => (
          <JobTodoCard 
          workerId={this.state.workerId}
          item={item}
          index={index}
          Date={this.state.Date} 
          />
        ))}
      </JobWrapper>
      
    </div>
  )
}
}

export default withRouter(JobTodo);
