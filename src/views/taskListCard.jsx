import React, {useState,useEffect} from "react";
import axios from 'axios';

import '../assets/css/calendar.css';

// reactstrap components
import { Card } from "reactstrap";
const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
const TaskListCard = (props) => {
    const {taskList, index} = props
    const [actname, setname]=useState([]);
    const [poundseq, setpoundlist] = useState([]);
    const [Workername, setWorkername] = useState([]);
    const [both, setboth]= useState("")

    const showwork = () =>{
      if(taskList.assigned_insectarium === "0"){
        setboth(`ห้องอบไข่ ${taskList.egg_id} กล่อง ${renderPonds(taskList.box_id)}`)
      }
      else if(taskList.egg_id === "0"){
        setboth(`โรงเลี้ยง ${taskList.assigned_insectarium} บ่อ ${renderPonds(taskList.assigned_pound)}`)
      }
      else if(taskList.activity_id === "30"){
        setboth(`โรงเลี้ยง ${taskList.assigned_insectarium} บ่อ ${renderPonds(taskList.assigned_pound)} ไป ห้องอบไข่ ${taskList.egg_id} กล่อง ${renderPonds(taskList.box_id)} `)
      }
      else{
        setboth(`ห้องอบไข่ ${taskList.egg_id} กล่อง ${renderPonds(taskList.box_id)} ไป โรงเลี้ยง ${taskList.assigned_insectarium} บ่อ ${renderPonds(taskList.assigned_pound)}`)
      }
    }


    

  const activityname = async () => {
    try {
        console.log('huakuy', taskList);
  const activityname = await axios.get(`${URL}/allact23/${taskList.activity_id}`)
  console.log('activityname', activityname.data);
      setname(activityname.data[0]);

    
    } catch (err) {
      console.error('ERROR ',err.message);
    }

    
  };


  const workername = async () => {
    try {
  const workername = await axios.get(`${URL}/workername/{${taskList.assigned_worker}}`)
  console.log('activityname', workername.data);
    setWorkername(workername.data);

    
    } catch (err) {
      console.error('ERROR ',err.message);
    }

    
  };

  const renderPonds = (assigned_pound) => {
    if (!assigned_pound) { return }
    return assigned_pound.map((pound) => (
        pound + " "
    ))
}

const renderWorker = (Workername) => {
    if (!Workername) { return }
    return Workername.map((name) => (
        name.worker_name + " "
    ))
}
    
    useEffect(()=>{
        activityname()
        workername()
        showwork()
        //getworkername()
      }, [taskList]);


    if (!taskList && !actname && !poundseq && !Workername) { return } 
      console.log('actname.activity_name', actname.activity_name);
    return (
        <a href={'jobtodo-admin?card_id=' + taskList.assigned_id}>
            <Card className="card-calendar" key={index}>
            <div className="row" style={{position: "relative", width: '100%'}}>
                <div className="col-5" style={{display: 'relative', borderRight: '1px solid #d3d3d3'}}>
                <div style={{position: 'relative', top:'50%', left: '50%', transform: 'translate(-50%, -50%)', display: 'inline-block'}}>
                    <p style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', color: '#92999e', marginLeft: '8px', marginBottom: '4px', marginTop: '4px'}}>{taskList.assigned_timestart} น.</p>
                    <p style={{width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', color: '#92999e', marginLeft: '8px'}}>{taskList.assigned_timefin} น.</p>
                </div>
                </div>
                <div className="col" style={{padding: '0px 0px 0px 0px', display: 'relative'}}>
                <div className="border w-75 mt-2" style={{height: '30px', borderRadius: '4px', backgroundColor: taskList.assigned_status === "เสร็จแล้ว" ? '#09c676' : '#e34849', color:'white', textAlign: 'center', marginLeft: '8px'}}>
                    <p style={{fontColor: "white", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px'}}>{taskList.assigned_status}</p>
                </div>
                <div className="mt-2">
                    <p style={{marginLeft: '8px', fontSize:"16px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginBottom: '0px'}}>{actname.activity_name}</p>
                    <div className="d-flex mt-2">
                    <i style={{fontWeight: 'bold', fontSize: '18px', color: '#0dc5ca', marginLeft: '8px'}} className={`nc-icon nc-pin-3`} />
                    <p style={{marginLeft: '8px', fontSize:"14px", fontFamily: 'IBM Plex Sans Thai'}}>{both}</p>
                    </div>
                    <div className="d-flex mt-2 mb-2">
                    <i style={{fontWeight: 'bold', fontSize: '18px', color: '#0dc5ca', marginLeft: '8px'}} className={`nc-icon nc-single-02`} />
                    <p style={{marginLeft: '8px', fontSize:"14px", fontFamily: 'IBM Plex Sans Thai'}}>{renderWorker(Workername)}</p>
                    </div>
                </div>
                </div>
            </div>
            </Card>
        </a>
    )

}

export default TaskListCard;