/*!
=========================================================
* Paper Dashboard PRO React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import routes from "routes.js";
import React from "react";
import axios from 'axios';
import classnames from "classnames";
import {
  Button,
  Collapse,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  NavbarBrand,
  Navbar,
  NavItem,
  Card,
  CardBody,
  Nav,
  Container,
  Popover,
  Badge,
  Modal, 
  ModalHeader, 
  ModalBody, 
  ModalFooter
} from "reactstrap";
import { NavLink } from "react-router-dom";
import ache from "../../assets/img/ache.png";
import CalendarIcon from "../../assets/img/calendar.svg";
import Calendars from "react-calendar";
import Notification from "../../views/Notificaiton";
import NotificaitonCalendar from "../../views/NotificaitonCalendar"
import Cookies from "js-cookie"

const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
class AdminNavbar extends React.Component {
  constructor(props) {
    const currentDate = new Date()
    super(props);
    this.state = {
      collapseOpen: false,
      color: "navbar-transparent",
      popoverOpen: false,
      popoverNoti: false,
      notiCountShow: true,
      poundsNoti: [],
      farmNoti: [],
      sevennoti: [],
      fouronenoti: [],
      fourfournoti:[],
      date: currentDate,
      hatchNoti: [],
      boxNoti: [],
      signOut: false

    };
    this.sendtime = this.sendtime.bind(this);
  }
  // componentDidUpdate(e) {
  //   if (
  //     window.outerWidth < 993 &&
  //     e.history.location.pathname !== e.location.pathname &&
  //     document.documentElement.className.indexOf("nav-open") !== -1
  //   ) {
  //     document.documentElement.classList.toggle("nav-open");
  //   }
  // }
  // function that adds color white/transparent to the navbar on resize (this is for the collapse)
  // updateColor = () => {
  //   if (window.innerWidth < 993 && this.state.collapseOpen) {
  //     this.setState({
  //       color: "bg-white"
  //     });
  //   } else {
  //     this.setState({
  //       color: "navbar-transparent"
  //     });
  //   }
  // };
  // getBrand = () => {
  //   var name;
  //   this.props.routes.map((prop, key) => {
  //     if (prop.collapse) {
  //       prop.views.map((prop, key) => {
  //         if (prop.layout + prop.path === this.props.location.pathname) {
  //           name = prop.name;
  //         }
  //         return null;
  //       });
  //     } else {
  //       if (prop.redirect) {
  //         if (prop.layout + prop.path === this.props.location.pathname) {
  //           name = prop.name;
  //         }
  //       } else {
  //         if (prop.layout + prop.path === this.props.location.pathname) {
  //           name = prop.name;
  //         }
  //         // filter pathname

  //       }
  //     }
  //     return null;
  //   });
  //   return name;
  // }
  // this function opens and closes the sidebar on small devices

  componentDidMount(){
    const currentDate = new Date()
    const data = ('0'+(currentDate.getMonth()+1)).slice(-2)+"-"+('0'+currentDate.getDate()).slice(-2)+"-"+currentDate.getFullYear()
   axios.get(`${URL}/allactv2/${data}/${Cookies.get('farmId')}`,{params: data})
    .then(res => {
       this.setState({
        taskLists:res.data
      })
    })

    // if (this.state.taskLists[0]){
    //    axios.get(`${URL}/poundlistass/${this.state.taskLists[0].assigned_id}`,{params: data})
    //   .then(res => {
    //      console.log(res.data)
    //      this.setState({
    //       pound :res.data
    //     })
    //     console.log(this.state.pound)
    //   })
    // }
  }
    

  sendtime = async (e) =>{
    
    const {date, onChange} = this.state
    await this.setState({date: e})

    await console.log('date', date);
    this.props.parentCallback(this.state.date)
    console.log(date)
  }

  toggleSidebar = () => {
    document.documentElement.classList.toggle("nav-open");
  };
  // this function opens and closes the collapse on small devices
  // it also adds navbar-transparent class to the navbar when closed
  // ad bg-white when opened
  toggleCollapse = () => {
    let newState = {
      collapseOpen: !this.state.collapseOpen,
    };
    if (!this.state.collapseOpen) {
      newState["color"] = "bg-white";
    } else {
      newState["color"] = "navbar-transparent";
    }
    this.setState(newState);
  };

  togglePopOver = () => {
    this.setState({ popoverOpen: !this.state.popoverOpen });
  };

  toggleNoti = () => {
    this.setState({ popoverNoti: !this.state.popoverNoti });
  };

  getNotifications = () => {
    let ponds = []
    let farm = []
    let hatch = []
    let boxes = []
    if (this.props.pageName === "Dashboard") {
        console.log('pageNamess', this.props.pageName);
          axios.get(`${URL}/notidashp/${Cookies.get('farmId')}`)
          .then(res => {
            console.log('notidashp', res.data)
            ponds.push(res.data)
            this.setState({
              poundsNoti: res.data
            })
          })
          axios.get(`${URL}/notidashi/${Cookies.get('farmId')}`)
          .then(res => {
            console.log('notidashi', res.data)
            farm.push(res.data)
            this.setState({
              farmNoti: res.data
            })
            
          })
          axios.get(`${URL}/notidashe/${Cookies.get('farmId')}`)
          .then(res => {
            hatch.push(res.data)
            this.setState({
              hatchNoti: res.data
            })
            
          })
          axios.get(`${URL}/notidashb/${Cookies.get('farmId')}`)
          .then(res => {
            boxes.push(res.data)
            this.setState({
              boxNoti: res.data
            })
            
          })
        
        }
        //axious blabla
        else {
          console.log('pageName2', this.props.pageName);
            //axious blabla
        }
    }


    getNotificationscalendar = () => {
      let seven = []
      let fourone = []
      let fourfour =[]
      if (this.props.pageName === "ปฏิทินมอบหมายงาน") {
          console.log('pageNamess', this.props.pageName);
            axios.get(`${URL}/noticalen7dayv2/${Cookies.get('farmId')}`)
            .then(res => {
              console.log('noticalen7', res.data)
              seven.push(res.data)
              this.setState({
                sevennoti: res.data
              })
              console.log(this.state.sevennoti)
            })
            axios.get(`${URL}/noticalenmanydayv2/41/${Cookies.get('farmId')}`)
            .then(res => {
              console.log('noticalen41', res.data)
              fourone.push(res.data)
              this.setState({
                fouronenoti: res.data
              })
            })
            axios.get(`${URL}/noticalenmanydayv2/44/${Cookies.get('farmId')}`)
            .then(res => {
              console.log('noticalen41', res.data)
              fourfour.push(res.data)
              this.setState({
                fourfournoti: res.data
              })
            })
          
          }
          //axious blabla
          else {
            console.log('pageName2', this.props.pageName);
              //axious blabla
          }
      }

  componentDidMount = () => {
    this.getNotifications()
    this.getNotificationscalendar()
  }



  render() {
    const { hatchNoti, boxNoti, dataNoti, notiCountShow, poundsNoti, farmNoti,sevennoti,fourfournoti,fouronenoti  } = this.state 
    const {date, onChange} = this.state
    const year = date.getFullYear() + 543
    const month = []
    month[0] = "มกราคม";
    month[1] = "กุมภาพันธ์";
    month[2] = "มีนาคม";
    month[3] = "เมษายน";
    month[4] = "พฤษภาคม";
    month[5] = "มิถุนายน";
    month[6] = "กรกฏาคม";
    month[7] = "สืิงหาคม";
    month[8] = "กันยายน";
    month[9] = "ตุลาคม";
    month[10] = "พฤศจิกายน";
    month[11] = "ธันวาคม";
    let thaiMonth = month[date.getMonth()]
    return (
      <>
        <Navbar
          className={classnames("navbar-absolute fixed-top", this.state.color)}
          expand="lg"
        >
          <Container fluid>
            <div>
              <div className="navbar-wrapper">
                <div className="navbar-minimize">
                  <Button
                    className="btn-icon btn-round"
                    color="default"
                    id="minimizeSidebar"
                    onClick={this.props.handleMiniClick}
                  >
                    <i className="nc-icon nc-minimal-right text-center visible-on-sidebar-mini" />
                    <i className="nc-icon nc-minimal-left text-center visible-on-sidebar-regular" />
                  </Button>
                </div>
                {(this.props.pageName === "Dashboard") ? (
                  <i class="fas fa-sign-out-alt fa-2x mt-4 text-white" onClick={() => this.setState({ signOut: !this.state.signOut })}></i>
                ) : null}
                <div
                  className={classnames("navbar-toggle", {
                    toggled: this.state.sidebarOpen,
                  })}
                >
                  {/* <button
                    className="navbar-toggler text-light font-weight-bold"
                    type="button"
                    onClick={this.toggleSidebar}
                  >
                    <span className="nc-icon nc-minimal-left" />
                  </button> */}
                </div>
                <NavbarBrand href="#pablo" onClick={(e) => e.preventDefault()}>
                  {this.props.pageName === "Dashboard" ? (
                    <img
                      style={{
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                      }}
                      className="mt-3"
                      height="40%"
                      width="40%"
                      src={ache}
                    />
                  ) : null}
                </NavbarBrand>
                {/* <div className="row" style={{position: "fixed", bottom: "0px", width: '100%', zIndex: '100', height: '50px', boxShadow: "0 -5px 5px -5px #333"}}>
                  <div className="col" style={{backgroundColor: "#23343d", padding: '0px 0px 0px 0px', display: 'relative'}}>
                    <p style={{color: "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ภาพรวม</p>
                  </div>
                  <div className="col" style={{backgroundColor: "white", padding: '0px 0px 0px 0px', display: 'relative'}}>
                    <p style={{color: "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>จัดการงาน</p>
                  </div>
                </div> */}
              </div>
            </div>
              {this.props.pageName === "Dashboard" ? null : (
                <div className="mt-5">
                  <span
                    style={{
                      fontFamily: "IBM Plex Sans Thai",
                      color: "#ffffff",
                      fontSize: "16px",
                      fontWeight: "bold",
                      width: '100%'
                    }}
                  >
                    {this.props.pageName}
                  </span>
                </div>
              )}
            
            {(this.props.pageName === "Dashboard") ? (
              <div className={this.props.pageName === "Dashboard" ? "mt-4 text-white" : "mt-5 text-white position-absolute"} style={{zIndex: '100', marginLeft: (this.props.pageName === "Dashboard") ? null : "250px" }} id="notiPopOver">
                <i class="far fa-bell fa-2x" ></i>
                {(this.state.poundsNoti || this.state.farmNoti || hatchNoti || boxNoti) ? (
                  <div>
                  <Badge pill color="danger" className="ml-3" style={{marginTop: '-40px', position: 'absolute', display: notiCountShow ? "flex" : "none"}}>
                      {poundsNoti.length + farmNoti.length + hatchNoti.length + boxNoti.length}
                  </Badge>
                </div>
                ) : null}
             
                <Popover
                  placement="left"
                  isOpen={this.state.popoverNoti}
                  target="notiPopOver"
                  toggle={()=> this.setState({popoverNoti: !this.state.popoverNoti, notiCountShow: false})}
                >
                  <Notification 
                    pageName={this.props.pageName}
                    poundsNoti={this.state.poundsNoti}
                    farmNoti={this.state.farmNoti}
                    hatchNoti={hatchNoti}
                    boxNoti={boxNoti}
                  /> 
                </Popover>
              </div>
            ) : null}



              {this.props.pageName == "งานที่ต้องทำ" ? (

              <div className={"mt-5 text-white position-absolute"} style={{
                position: 'absolute',
                left: "85%",
                right: 0,
                top: 0,
                bottom: 0}}
                 id="calendarPopOver">
              <i class="fas fa-calendar-alt fa-2x" ></i>
                
              
                <Popover
                  placement="bottom"
                  isOpen={this.state.popoverOpen}
                  target="calendarPopOver"
                  toggle={this.togglePopOver}
                >
                  <Calendars locale={"th"} className={"border-0"} 
                   locale={'th'}
                   className={'border-0'}
                   onChange={async (e) =>{ await this.setState({date: e}); await this.sendtime(e)}}
                   value={date}/>
                </Popover>
              </div>
            ) : null}



                  {/*CalendarNoti*/}

        
                  {(this.props.pageName === "Calendar" || this.props.pageName === "ปฏิทินมอบหมายงาน") ? (
              <div>
                <div className={this.props.pageName === "Calendar" ? "mt-4 text-white" : "mt-5 text-white position-absolute"} style={{
                  position: 'absolute',
                  left: "85%",
                  right: 0,
                  top: 0,
                  bottom: 0
              }} id="notiPopOver">
                  <i class="far fa-bell fa-2x" ></i>
                  {(this.state.sevennoti || this.state.fouronenoti || this.state.fourfournoti) ? (
                    <div>
                      <Badge pill color="danger" className="ml-3" style={{marginTop: '-40px', position: 'absolute', display: notiCountShow ? "flex" : "none"}}>
                          {fouronenoti.length + sevennoti.length + fourfournoti.length}
                      </Badge>
                    </div>

                  ) : null}

                  <Popover
                    placement="bottom"
                    isOpen={this.state.popoverNoti}
                    target="notiPopOver"
                    toggle={()=> this.setState({popoverNoti: !this.state.popoverNoti, notiCountShow: false})}
                  >
                    <NotificaitonCalendar 
                      pageName={this.props.pageName}
                    
                      fourfournoti={this.state.fourfournoti}
                      fouronenoti={this.state.fouronenoti}
                      sevennoti={this.state.sevennoti}
                    /> 
                  </Popover>
                </div>
                <a href={`/admin/export`}> 
                  <div className={this.props.pageName === "Calendar" ? "mt-4 text-white" : "mt-5 text-white position-absolute"} style={{
                    position: 'absolute',
                    left: "10%",
                    right: 0,
                    top: 0,
                    bottom: 0
                }} id="fmcl">
                      <i className="far fa-file-alt fa-2x" ></i>
                  </div>
                  </a>
              </div>
            ) : null}




            {/* <button
              aria-controls="navigation-index"
              aria-expanded={this.state.collapseOpen}
              aria-label="Toggle navigation"
              className="navbar-toggler"
              // data-target="#navigation"
              data-toggle="collapse"
              type="button"
              onClick={this.toggleCollapse}
            >
              <span className="navbar-toggler-bar navbar-kebab" />
              <span className="navbar-toggler-bar navbar-kebab" />
              <span className="navbar-toggler-bar navbar-kebab" />
            </button> */}
            <Collapse
              className="justify-content-end"
              navbar
              isOpen={this.state.collapseOpen}
            >
              <Nav navbar>
                <UncontrolledDropdown className="btn-rotate" nav>
                  <DropdownToggle
                    aria-haspopup={true}
                    caret
                    color="default"
                    data-toggle="dropdown"
                    id="navbarDropdownMenuLink"
                    nav
                  >
                    <i className="nc-icon nc-single-02" />
                    <p>
                      <span className="d-lg-none d-md-block">Some Actions</span>
                    </p>
                  </DropdownToggle>
                  <DropdownMenu aria-labelledby="navbarDropdownMenuLink" right>
                    <NavLink to="/admin/user-profile">
                      <DropdownItem href="http://localhost:3000/admin/user-profile">
                        ข้อมูลผู้ใช้งาน
                      </DropdownItem>
                    </NavLink>
                    <NavLink to="/auth/login">
                      <DropdownItem>ออกจากระบบ</DropdownItem>
                    </NavLink>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </Container>
          <Modal isOpen={this.state.signOut}  style={{maxHeight: "200px"}} className="custom-modal-style">
            <ModalHeader className="">
                <div className="d-flex justify-content-center">
                  <h5 className="text-center" style={{color:"black", fontSize: "14px",fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}> ต้องการออกจากระบบ ? </h5>
                  <i style={{position: "absolute", right: '5%'}} className="far fa-times-circle fa-2x text-danger" onClick={() => this.setState({signOut: false})}></i>
                </div>
            </ModalHeader>
            <ModalFooter className="justify-content-center">
              <a href={`/auth/login`}>
                  <Button color="secondary">ยืนยัน</Button>
              </a>
              <Button className="mr-3" color="primary" onClick={() => this.setState({signOut: false})}>ยกเลิก</Button>
            </ModalFooter>
          </Modal>
        </Navbar>
      </>
    );
  }
}

export default AdminNavbar;
