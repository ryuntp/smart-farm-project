import React, { useState } from "react";
import Cookies from "js-cookie";
const AdminFooter = (props) => {
    const { active } = props
    if (active) {
        return (
            <div className="row" style={{position: "fixed", bottom: "0px", width: '104%', zIndex: '100', height: '50px', boxShadow: "0 -5px 5px -5px #333", left: "0px"}}>
                <div className="col" style={{backgroundColor: "white", padding: '0px 0px 0px 0px', display: 'relative'}}>
                    {/* <a href={`/admin/farm?farm_id=${Cookies.get('farmId')}`}> */}
                    <a href={`/admin/homedash`}>
                        <p style={{color: "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ภาพรวม</p>
                    </a>
                </div>
                <div className="col" style={{backgroundColor: "#23343d", padding: '0px 0px 0px 0px', display: 'relative'}}>
                    <a href="/admin/calendar">
                        <p style={{color: "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>จัดการงาน</p>
                    </a>
                </div>
        </div>
        )
    }
    return (
        <div className="row" style={{position: "fixed", bottom: "0px", width: '104%', zIndex: '100', height: '50px', boxShadow: "0 -5px 5px -5px #333", left: "0px"}}>
                <div className="col" style={{backgroundColor: "#23343d", padding: '0px 0px 0px 0px', display: 'relative'}}>
                    {/* <a href={`/admin/farm?farm_id=${Cookies.get('farmId')}`}> */}
                    <a href={`/admin/homedash`}>
                        <p style={{color: "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ภาพรวม</p>
                    </a>
                </div>
                <div className="col" style={{backgroundColor: "white", padding: '0px 0px 0px 0px', display: 'relative'}}>
                    <a href="/admin/calendar">
                        <p style={{color: "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>จัดการงาน</p>
                    </a>
                </div>
        </div>
    )
}

export default AdminFooter


