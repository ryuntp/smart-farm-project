import React, { useState } from "react";
import Cookies from "js-cookie";
const WorkerInquiryFooter = (props) => {
    const { insectariumId } = props
    return (
        <div className="row" style={{position: "fixed", bottom: "0px", width: '104%', zIndex: '100', height: '50px', boxShadow: "0 -5px 5px -5px #333", left: "0px", zIndex: '100'}}>
            <div className="col-md-12" style={{backgroundColor: "#23343d", padding: '0px 0px 0px 0px', display: 'relative'}}>
                <p style={{color: "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ยืนยันตอบแบบสอบถาม</p>
            </div>
        </div>
    )
}

export default WorkerInquiryFooter


